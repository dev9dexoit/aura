//
//  CheckInCompleteView.swift
//  MyDoktorUp
//
//  Created by Mac on 03/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI

struct CheckInCompleteView: View {
    
    var window: UIWindow?
    @Binding var rootIsActive : Bool
    @State var GoToAppointmentTab : Bool
    @Environment(\.presentationMode) var presentationMode
    @State var Selectedmodel = AppointmentModel()
    @EnvironmentObject var appState: AppState
    
    var body: some View {
        ZStack {
            ScrollView {
                VStack(alignment: .leading, spacing: 15) {
                    HStack {
                        Spacer()
                        
                        Text("\(self.Selectedmodel.appointment_date?.convertDateFormat(inputDate: self.Selectedmodel.appointment_date ?? "", inputFormat: "yyyy-MM-dd", format: "EEEE MMM d, YYYY") ?? "")" + " - " + "\(self.Selectedmodel.appointment_time?.convertDateFormat(inputDate: self.Selectedmodel.appointment_time ?? "", inputFormat: "HH:mm", format: "hh:mma") ?? "")")
                            .foregroundColor(kCustomOrangeColor)
                            .fontWeight(.semibold)
                            .font(.system(.headline))
                            .multilineTextAlignment(.center)
                        
                        Spacer()
                    }
                    
                    Spacer()
                    
                    HStack {
                        Spacer()
                        Text("Visit reason: \(self.Selectedmodel.visit_reason ?? "")")
                            .font(Font.system(size: 21))
                            .fontWeight(.regular)
                            .foregroundColor(kCustomTextBlueColor)
                            .multilineTextAlignment(.center)
                        Spacer()
                    }
                    
                    Spacer()
                    
                    HStack {
                        Spacer()
                        Text("Please follow the instructions sent to \n\(self.Selectedmodel.patient_email ?? "")\n to join the video call.")
                            .foregroundColor(kCustomTextBlueColor)
                            .font(.system(size: 21, weight: Font.Weight.regular, design: Font.Design.default))
                            .multilineTextAlignment(.center)
                        Spacer()
                    }
                    
                    Spacer()
                    Spacer()
                    Spacer()
                    Spacer()
                    
                    Button(action: {
                        print("Appointments Tapped!!")
                        self.appState.reloadDashboard(selectedTab:2, selectedProvider: Provider())
                    }) {
                        HStack {
                            Text("Appointments")
                                .foregroundColor(.white)
                                .padding(.vertical, 12)
                            
                        }
                        .frame(minWidth: 0, maxWidth: .infinity)
                        .background(kPrimaryRegularColor)
                        .cornerRadius(10)
                    }
                    
                }
                VStack(){
                    
                    NavigationLink(destination: ContentView(isNavigationBarHidden: .constant(true), selection:2), isActive: $GoToAppointmentTab) {
                        EmptyView()
                    }
                    
                }
            }.padding()
        }
        .navigationBarTitle("Check-in Complete", displayMode: .inline)
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(trailing:
            HStack {
                Button(action: {
                    
                    self.rootIsActive = true
                }) {
                    Text("Done")
                }
                .padding(.leading, -8)
        })
    }
}

struct CheckInCompleteView_Previews: PreviewProvider {
    static var previews: some View {
        CheckInCompleteView(rootIsActive: .constant(false), GoToAppointmentTab: false)
    }
}

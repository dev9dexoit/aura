import SwiftUI
struct PersonalInfoView: View {
    
    @ObservedObject var appointments = AppointmentListObserver()
    @Environment(\.presentationMode) var presentationMode
    
    @Binding var rootIsActive: Bool
    @ObservedObject var observedInsurance = InsuranceCarrierObserver()
    @ObservedObject var observerChekingAppointment = ChekingAppointment_Observer()
    @ObservedObject var observedInsuranceSelection = InsuranceSelectionObserver()
    @State var insuranceNames: [String] = []
    
    @State var isActiveInsurance = false
    
    @State var fullName: String = ""
    @State var birthDate:String = ""
    @State var selectedGender = 0
    @State var emailAddress: String = ""
    @State var Gender = ""
    @State var address: String = ""
    @State var cityStateZip: String = ""
    @State var Selectedmodel = AppointmentModel()
    @State var SelectedId = ""
    
    var arrMaritalStatus = ["Married", "Unmarried","Divorced"]
    var arrLanguage = [
        "English",
        "Latin",
        "French",
        "Spanish",
        "Hindi",
        "Mandarin Chinese",
        "Arabic"
    ]
    
    var arrRace = [
        "Bushmen",
        "Negroes",
        "Negritoes",
        "Melanochroi",
        "Australoids",
        "Xanthochroi",
        "Polynesians",
        "Mongoloids A",
        "Mongoloids B",
        "Mongoloids C",
        "Esquimaux"
    ]
    
    var arrEthnicity = [
        "African Americans",
        "American Indian group",
        "Jewish people",
        "Americans",
        "Asian people"
    ]
    
    @State var selectedMaritalStatus = 0
    @State var selectedLanguage = 0
    @State var selectedRace = 0
    @State var selectedEthnicity = 0
    @State var isShowing = true
    
    @State var isNext = false
    @State var isChekingInfo = false
//    @ObservedObject var phoneNo = TextBindingManager()
    @State var Phonenumber = ""

    @State var InsuranceImage1:UIImage = UIImage(named: "ic_camera")!
    @State var InsuranceImage2:UIImage = UIImage(named:"ic_camera")!
    @State var DrivingImage1:UIImage = UIImage(named:"ic_camera")!
    @State var DrivingImage2:UIImage = UIImage(named:"ic_camera")!
    
    
    @State var IsInsuranceImage1:Bool = false
    @State var IsInsuranceImage2:Bool = false
    @State var IsDrivingImage1:Bool = false
    @State var IsDrivingImage2:Bool = false
    @State var isDisappar = false

    var body: some View {
        LoadingView(isShowing: .constant(isShowing)) {
            ZStack {
                Form {
                    Section(header: VStack(alignment: .leading, spacing: 0){
                        HStack(alignment: .center, spacing: 0){
                            Spacer()
                            Group {
                                Image("ic_user_placeholder")
                                    .resizable()
                                    .scaledToFit()
                                    .frame(height: 50)
                                    .clipped()
                                    .listRowInsets(EdgeInsets())
                                    .background(kCustomLightBlueColor)
                                
                            }
                            .frame(width:120,height:120)
                                
                            .overlay(
                                RoundedRectangle(cornerRadius: 120/2)
                                    .strokeBorder(style: StrokeStyle(lineWidth: 1))
                                    .foregroundColor(kCustomGrayColor)
                            )
                            
                            Spacer()
                        }
                        
                        Text("Personal information").foregroundColor(kPrimaryRegularColor).fontWeight(.thin).font(.system(.title)).padding(.top, 10)
                        
                    }.padding(.top, 30)
                    ) {
                        
                        TextField("* First Name, Last Name", text: self.$fullName).disabled(true)
                        
                        TextField("* Phone", text: self.$Phonenumber).keyboardType(.numberPad).disabled(true)
                        
                        TextField("* Date of Birth", text: self.$birthDate).keyboardType(.numberPad).disabled(true).disabled(true)
                        
                        TextField("* Gender", text: self.$Gender).keyboardType(.numberPad).disabled(true).disabled(true)
                        
                        TextField("* Email", text: self.$emailAddress).keyboardType(.emailAddress).disabled(true)
                    }.padding(.vertical, 10)
                        .foregroundColor(kCustomGrayColor)
                    
                    Section(header: Text("Address").foregroundColor(kPrimaryRegularColor).fontWeight(.thin).font(.system(.title))) {
                        
                        TextField("Address 1, Address 2", text: self.$address).disabled(true)
                        TextField("City, State, Zip", text: self.$cityStateZip).disabled(true)
                        
                    }.padding(.vertical, 10)
                        .foregroundColor(kCustomGrayColor)
                    
                    Section(header: Text("Insurance").foregroundColor(kPrimaryRegularColor).fontWeight(.thin).font(.system(.title))) {
                        HStack(alignment: .center, spacing: 10){
                            NavigationLink(destination: InsuranceListView(insuranceNames: self.insuranceNames, insuranceDelegate: self), isActive: self.$isActiveInsurance) {
                                Button("Insurance") {
                                    self.isActiveInsurance.toggle()
                                }
                                Spacer()
                                Text(self.observedInsuranceSelection.insuranceName)
                            }
                        }
                    }
                    .padding(.vertical, 10)
                    Section(header: Text("Insurance card").foregroundColor(kPrimaryRegularColor).fontWeight(.thin).font(.system(.title))) {
                        
                        HStack(alignment: .top, spacing: 15, content: {
                            VStack (alignment: .leading, spacing: 10) {
                                Text("Front side of card")
                                
                                GeometryReader { geometry in
                                    
                                    Image(uiImage: self.InsuranceImage1)
                                        .renderingMode(.original)
                                        .scaledToFill()
                                        .frame(height:100)
                                        .clipped()
                                        .listRowInsets(EdgeInsets())
                                        .frame(width:geometry.size.width,height:geometry.size.height)
                                        .overlay(
                                            RoundedRectangle(cornerRadius: 10)
                                                .strokeBorder(style: StrokeStyle(lineWidth: 1))
                                                .foregroundColor(kCustomGrayColor)
                                                .cornerRadius(10)
                                    )
                                        .onTapGesture {
                                            if !Platform.isSimulator {
                                                self.IsInsuranceImage1 = true
                                            }
                                    }
                                    .sheet(isPresented: self.$IsInsuranceImage1) {
                                        
                                        ScannerView(Title: "Insurance Front side of card", completion: { image in
                                            self.InsuranceImage1 = image
                                        })
                                    }
                                    
                                }.frame(height:100)
                                
                            }
                            
                            VStack (alignment: .leading, spacing: 10) {
                                Text("Back side of card")
                                
                                GeometryReader { geometry in
                                    Button(action: {
                                    }) {
                                        
                                        Image(uiImage: self.InsuranceImage2)
                                            .renderingMode(.original)
                                            .scaledToFill()
                                            .frame(height:100)
                                            .clipped()
                                            .listRowInsets(EdgeInsets())
                                            .frame(width:geometry.size.width,height:geometry.size.height)
                                            .overlay(
                                                RoundedRectangle(cornerRadius: 10)
                                                    .strokeBorder(style: StrokeStyle(lineWidth: 1))
                                                    .foregroundColor(kCustomGrayColor)
                                                    .cornerRadius(10)
                                        )
                                        
                                    }
                                    .onTapGesture {
                                        if !Platform.isSimulator {
                                            self.IsInsuranceImage2 = true
                                        }
                                    }
                                    .sheet(isPresented: self.$IsInsuranceImage2) {
                                        ScannerView(Title: "Insurance Back side of card", completion: { image in
                                            self.InsuranceImage2 = image
                                        })
                                    }
                                }.frame(height:100)
                                
                            }
                        })
                        
                    }.padding(.vertical, 0)
                    
                    
                    Section(header: Text("Drivers license").foregroundColor(kPrimaryRegularColor).fontWeight(.thin).font(.system(.title))) {
                        
                        HStack(alignment: .top, spacing: 15, content: {
                            VStack (alignment: .leading, spacing: 10) {
                                Text("Front side of card")
                                
                                GeometryReader { geometry in
                                    Button(action: {
                                    }) {
                                        
                                        Image(uiImage: self.DrivingImage1)
                                            .renderingMode(.original)
                                            .scaledToFill()
                                            .frame(height:100)
                                            .clipped()
                                            .listRowInsets(EdgeInsets())
                                            .frame(width:geometry.size.width,height:geometry.size.height)
                                            .overlay(
                                                RoundedRectangle(cornerRadius: 10)
                                                    .strokeBorder(style: StrokeStyle(lineWidth: 1))
                                                    .foregroundColor(kCustomGrayColor)
                                                    .cornerRadius(10)
                                        )
                                    }
                                    .onTapGesture {
                                        if !Platform.isSimulator {
                                            self.IsDrivingImage1 = true
                                        }
                                    }
                                        
                                    .sheet(isPresented: self.$IsDrivingImage1) {
                                        ScannerView(Title: "Drivers license Front side of card", completion: { image in
                                            self.DrivingImage1 = image
                                        })
                                    }
                                }.frame(height:100)
                            }
                            
                            VStack (alignment: .leading, spacing: 10) {
                                Text("Back side of card")
                                
                                GeometryReader { geometry in
                                    Button(action: {
                                        
                                    }) {
                                        
                                        Image(uiImage: self.DrivingImage2)
                                            .renderingMode(.original)
                                            .scaledToFill()
                                            .frame(height:100)
                                            .clipped()
                                            .listRowInsets(EdgeInsets())
                                            .frame(width:geometry.size.width,height:geometry.size.height)
                                            .overlay(
                                                RoundedRectangle(cornerRadius: 10)
                                                    .strokeBorder(style: StrokeStyle(lineWidth: 1))
                                                    .foregroundColor(kCustomGrayColor)
                                                    .cornerRadius(10)
                                        )
                                        
                                    }
                                    .onTapGesture {
                                        if !Platform.isSimulator {
                                            self.IsDrivingImage2 = true
                                        }
                                    }
                                    .sheet(isPresented: self.$IsDrivingImage2) {
                                        ScannerView(Title: "Drivers license Back side of card ", completion: { image in
                                            self.DrivingImage2 = image
                                        })
                                    }
                                }.frame(height:100)
                            }
                            
                        })
                        
                        Picker(selection: self.$selectedMaritalStatus, label: Text("Marital Status")) {
                            ForEach(0 ..< self.arrMaritalStatus.count) {
                                Text(self.arrMaritalStatus[$0]).padding(.vertical, 10)
                            }
                        }
                        
                        Picker(selection: self.$selectedLanguage, label: Text("Language")) {
                            ForEach(0 ..< self.arrLanguage.count) {
                                Text(self.arrLanguage[$0]).padding(.vertical, 10)
                            }
                        }
                        
                        Picker(selection: self.$selectedRace, label: Text("Race")) {
                            ForEach(0 ..< self.arrRace.count) {
                                Text(self.arrRace[$0]).padding(.vertical, 10)
                            }
                        }
                        
                        Picker(selection: self.$selectedEthnicity, label: Text("Ethnicity")) {
                            ForEach(0 ..< self.arrEthnicity.count) {
                                Text(self.arrEthnicity[$0]).padding(.vertical, 10)
                            }
                        }
                        
                    }.padding(.vertical, 0)
                        .foregroundColor(kCustomGrayColor)
                    
                }.padding(.vertical, -34)
                    .foregroundColor(kCustomGrayColor)
                    .navigationBarTitle("Step 1 of 2", displayMode: .inline)
                    .navigationBarItems(leading: HStack {
                        Button(action: {
                            self.presentationMode.wrappedValue.dismiss()
                        }, label: {Image("ic_back")})
                        },trailing:
                        
                        NavigationLink(destination: MedicalFormsView(rootIsActive: self.$rootIsActive,Selectedmodel: self.Selectedmodel), isActive: self.$isNext){
                            HStack {
                                Button(action: {
                                    
                                    self.observerChekingAppointment.postInsurance(Frontimage: self.InsuranceImage1, Backimage: self.InsuranceImage2) { (postSucess) in
                                        print("postInsurance sucess")
                                    }
                                    self.observerChekingAppointment.postDrivingLicense(Frontimage: self.DrivingImage1, Backimage: self.DrivingImage2){ (postSucess) in
                                        print("postDrivingLicense sucess")
                                    }
                                    
                                    var paramters = [String:String]()
                                    paramters["marital_status"] =  self.arrMaritalStatus[self.selectedMaritalStatus]
                                    paramters["language"] = self.arrLanguage[self.selectedLanguage]
                                    paramters["race"] = self.arrRace[self.selectedRace]
                                    paramters["ethnicity"] = self.arrEthnicity[self.selectedEthnicity]
                                    
                                    self.observerChekingAppointment.setChekingAppoitmentStep(Checkingparamters:[""],Infoparamters:paramters,id: self.Selectedmodel.appointment_id ?? "", type: "info") { (cheking) in
                                        self.isShowing = true
                                        if cheking == true
                                        {
                                            self.isShowing = false
                                            self.isNext = true
                                        }
                                    }
                                    
                                }) {
                                    Text("Next").foregroundColor(.white)
                                }
                            }
                        }
                )
                    .background(kCustomLightBlueColor)
                
            }.background(kCustomLightBlueColor)
                .navigationBarBackButtonHidden(true)
                .onAppear {
                    UITableView.appearance().backgroundColor = .clear
                    UITableView.appearance().separatorStyle = .singleLine
                    UITableViewCell.appearance().selectionStyle = .none
                    self.appointments.GetAppointmentInfo(
                        AppointmentId: self.SelectedId,
                        completionHander: { (isSuccess) in
                            self.isShowing = true
                            if isSuccess {
                                if !self.isDisappar
                                {

                                    self.Selectedmodel = self.appointments.appointmentInfoModel
                                    self.fullName = self.Selectedmodel.patient_name ?? ""
                                    self.birthDate = self.Selectedmodel.patient_dob ?? ""
                                    self.selectedGender = 0
                                    self.emailAddress = self.Selectedmodel.patient_email ?? ""
                                    self.Gender = self.Selectedmodel.patient_gender ?? ""
                                    self.observedInsuranceSelection.insuranceName = self.Selectedmodel.insurance ?? ""
                                    self.Phonenumber = self.Selectedmodel.patient_phone?.formatePhoneNumber() ?? ""
                                    self.address = self.Selectedmodel.patient_address1 ?? ""
                                    self.cityStateZip =  self.Selectedmodel.patient_city_state_zip ?? ""
                                }
                                self.isShowing = false
                            } else {
                                self.isShowing = false
                            }
                    })
                    
                    
                    self.observedInsurance.getInsuranceList(searchText: "", isCommonAPI: false, complitionHandler: { (isSuccess) in
                        if isSuccess {
                            self.isShowing = false
                            self.insuranceNames.removeAll()
                            for case let insurance in self.observedInsurance.arrInsuranceList {
                                self.insuranceNames.append(insurance.name ?? "")
                            }
                        }
                    })
            }
        }
    }
}

struct PersonalInfoView_Previews: PreviewProvider {
    static var previews: some View {
        PersonalInfoView(rootIsActive: .constant(false))
    }
}


extension PersonalInfoView: InsuranceDelegate {
    func didSelectInsurance(index: Int) {
        print("Insurance ID = ",self.observedInsurance.arrInsuranceList[index].id!)
        self.observedInsuranceSelection.insuranceName = self.observedInsurance.arrInsuranceList[index].name!
    }
}

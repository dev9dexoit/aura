//
//  CheckInView.swift
//  MyDoktorUp
//
//  Created by Mac on 05/02/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI

struct CheckInView: View {
    
    @ObservedObject var appointments = AppointmentListObserver()
    @State private var isShowing = true
    
    init() {
        UITableView.appearance().separatorStyle = .none
        UITableViewCell.appearance().accessoryType = .none
        UITableViewCell.appearance().selectionStyle = .none
    }
    var body: some View {
        LoadingView(isShowing: .constant(isShowing)) {
            NavigationView {
                
                if self.appointments.appointmentData.count == 0 {
                    VStack {
                        // No Appointments are available
                        Text("Nothing to show here.")
                            .font(.headline)
                            .fontWeight(.bold)
                            .foregroundColor(kCustomPrimaryDarkColor)
                            .padding(.bottom, 5)
                        Text("Your Digital Check-in eligible appointments will show here.")
                            .font(.headline)
                            .fontWeight(.regular)
                            .multilineTextAlignment(.center)
                            .foregroundColor(kCustomPrimaryDarkColor)
                            .padding(.leading,10)
                            .padding(.trailing,10)
                            
                    }
                    .navigationBarTitle("Digital Check-in", displayMode: .inline)
                    
                } else {
                    List {
                        ForEach(self.appointments.appointmentData) { section in
                            
                            Section(header:
                                HStack {
                                    Text(section.title)
                                        .font(.title)
                                        .fontWeight(.light)
                                        .foregroundColor(kPrimaryRegularColor)
                                        .foregroundColor(kPrimaryRegularColor)
                                        .padding()
                                    Spacer()
                                    
                                    
                                }
                                .background(kCustomLightBlueColor)
                                .listRowInsets(EdgeInsets(top: 0,leading: 0,bottom: 0,trailing: 0)
                                )
                                
                            ) {
                                ForEach(section.items, id: \.self) { model in
                                    VStack(alignment: .leading, spacing: 5) {
                                        if section.title == AppointmentType.scheduled {
                                            // Schedule Appointment list
                                            ScheduledAppointmentForCheckInRowView(section: section.title, model: model)
                                                .padding()
                                                .background(Color.white)
                                                .clipped()
                                                .shadow(color: kCustomGrayColor, radius: 2, x: 0, y: 2)
                                                .border(kCustomLightGrayDividerColor, width: 0)
                                            // Digital Check-in Button
                                            if getHideAccordingHour(date: model.appointment_date ?? "\(Date())", formate: "MM-dd-yyyy")
                                            {
                                                Button(action: {
                                                    print("Digital check in tapped!!")
                                                }) {
                                                    Text("Digital Check-in")
                                                        .fontWeight(.medium)
                                                        .frame(minWidth: 0, maxWidth: .infinity)
                                                        .padding(.vertical, 10)
                                                        .foregroundColor(Color.white)
                                                }
                                                .background(kPrimaryRegularColor)
                                                .clipped()
                                                .shadow(color: kCustomGrayColor, radius: 2, x:  0, y: 2)
                                                
                                            }
                                        }
                                    }
                                }
                                .listRowBackground(kCustomLightBlueColor)
                            }
                        }
                    }
                    .navigationBarTitle("Digital Check-in", displayMode: .inline)
                    
                }
                
                
            }.onAppear() {
                
                self.isShowing = true
                
                self.appointments.getAppointmentList(ForDigitalCheking:true,
                                                     startDate: Utils.getCurrentDate(isRequest: true),
                                                     endDate: "", type: "grouped",
                                                     completionHander: { (isSuccess) in
                                                        if isSuccess {
                                                            print("Success true")
                                                            self.isShowing = false
                                                        } else {
                                                            print("Failure")
                                                            self.isShowing = false
                                                        }
                })
            }
            .navigationBarBackButtonHidden(true)
            .hideNavigationBar()
                .navigationViewStyle(StackNavigationViewStyle())// This line is supporting iPad, we should not comment out this line
        }
    }
}

struct CheckInView_Previews: PreviewProvider {
    static var previews: some View {
        CheckInView()
    }
}

struct ScheduledAppointmentForCheckInRowView: View {
    var section: String
    var model = AppointmentModel()
    @State var rootIsActive: Bool = false
    
    var body: some View {
        
        NavigationLink(destination:PersonalInfoView(rootIsActive: self.$rootIsActive,SelectedId:model.appointment_id!))
        {
            VStack(alignment: .leading, spacing: 0) {
                Text("\(model.provider_first_name ?? "") \(model.provider_last_name ?? "")").font(.headline).fontWeight(.bold).foregroundColor(kPrimaryRegularColor).lineLimit(nil)
                
                HStack{
                    VStack(alignment: .leading, spacing: 2) {
                        
                        Text("\(model.taxonomy_name ?? "")").foregroundColor(Color.black).font(.footnote).fontWeight(.medium).lineLimit(nil)
                            .padding(.top, 5)
                        Text("\(model.provider_address1 ?? "") \(model.provider_address2 ?? "" )").foregroundColor(kCustomGrayColor).font(.footnote).fontWeight(.regular)
                            .padding(.top, 3).lineLimit(nil)
                        Text("\(model.distance ?? "2.00") Miles").foregroundColor(kCustomDarkGrayColor).font(.footnote).fontWeight(.medium).lineLimit(nil)
                            .padding(.top, 3)
                    }
                    Spacer()
                    VStack(alignment: .leading, spacing: 2) {
                        HStack
                            {
                                Spacer()
                                
                                Text("\(model.appointment_date?.convertDateFormat(inputDate: model.appointment_date ?? "", inputFormat: "MM-dd-yyyy", format: "MMM d, yyyy") ?? "")" + " - " + "\(model.appointment_time?.convertDateFormat(inputDate: model.appointment_time ?? "", inputFormat: "HH:mm", format: "hh:mma") ?? "")")
                                    .foregroundColor(kCustomOrangeColor).font(.callout).fontWeight(.light).lineLimit(nil).multilineTextAlignment(.trailing)
                                
                                
                        }
                        HStack
                            {
                                Spacer()
                                
                                Text("\(model.provider_phone?.formatePhoneNumber() ?? "")").foregroundColor(kPrimaryRegularColor).font(.callout).fontWeight(.regular).padding(.top, 2).lineLimit(nil)
                                
                        }
                    }
                }
            }
        }
    }
}

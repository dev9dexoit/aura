//
//  MedicalFormsView.swift
//  MyDoktorUp
//
//  Created by Mac on 07/03/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI
import Braintree
import BraintreeDropIn

struct MedicalFormsView: View {
    
    @ObservedObject var observerChekingAppointment = ChekingAppointment_Observer()
    @Environment(\.presentationMode) var presentationMode
    @Binding var rootIsActive : Bool
    @State var showCheckInCompleteView = false
    @State var isPaymentEnabled = false
    @State var isSignAllForm = false
    @State var Selectedmodel = AppointmentModel()
    @State var isPaymentTap = false
    @State var isSigUpAllFormTap = false
    let dateformate = DateFormatter()
    @State var isShowing = false
    
    var body: some View {
        LoadingView(isShowing: .constant(isShowing)) {
            VStack {
                VStack {
                    Form{
                        Section(header: Text("Medical forms").foregroundColor(kPrimaryRegularColor).fontWeight(.thin).font(.system(.title)).padding(.top,15)) {
                            
                            NavigationLink(destination: FinancialPolicyView(title: "Financial Policy", formNumber: 1,PhoneNumber:self.Selectedmodel.patient_phone ?? "",id:self.Selectedmodel.appointment_id ?? "")) {
                                
                                OptionRow(title: "Financial Policy")
                             
                        }
                        
                        NavigationLink(destination: HIPAAAcknowledgementView(title: "HIPAA Acknowledgement", formNumber: 2,PhoneNumber:self.Selectedmodel.patient_phone ?? "",id:self.Selectedmodel.appointment_id ?? "")) {
                            OptionRow(title: "HIPAA Acknowledgement")
                            
                        }
                        
                        
                        NavigationLink(destination: HIPPAAuthorizationView()) {
                            OptionRow(title: "HIPPA Authorization")
                            
                        }
                       
                        
                        NavigationLink(destination: AssignmentBenefitView(title: "Assignment of Benefits", formNumber: 3,PhoneNumber:self.Selectedmodel.patient_phone ?? "",id:self.Selectedmodel.appointment_id ?? "")) {
                            OptionRow(title: "Assignment of Benefits")
                            
                        }
                       
                        
                        NavigationLink(destination: NoticeOfPrivacyFormView(title: "Notice of Privacy Form", formNumber: 4,PhoneNumber:self.Selectedmodel.patient_phone ?? "",id:self.Selectedmodel.appointment_id ?? "")) {
                            OptionRow(title: "Notice of Privacy Form")
                            
                        }
                       
                        NavigationLink(destination: NoticeOfPrivacySignatureFormView(title: "Notice of Privacy signature Form", formNumber: 5,PhoneNumber:self.Selectedmodel.patient_phone ?? "",id:self.Selectedmodel.appointment_id ?? "")) {
                            OptionRow(title: "Notice of Privacy signature Form")
                            
                        }
                        
                        
                        
                    }.foregroundColor(kCustomGrayColor).padding(.bottom, 5).listRowBackground(kCustomLightBlueColor)
                    
                }
                    
                        Spacer()
                        if self.isSignAllForm {
                            Text("You signed on \(self.getCheingkingAllSign(date: self.Selectedmodel.checkin_consent ?? ""))")
                                .foregroundColor(kCustomDarkGrayColor)
                                .font(Font.system(size: 22))
                                .fontWeight(.regular)
                                .padding(.top, 5)
                                .padding(.bottom, 15)
                        }
                        
                        if !self.isSigUpAllFormTap {
                            Button(action: {
                                let selectePhonenumber = self.Selectedmodel.patient_phone
                                MedicalFormJsonObject = NSArray()
                                MedicalFormJsonObject.adding(SetModelObject(Number: selectePhonenumber ?? "", FormName: "Financial Policy", formNumber: 1))
                                MedicalFormJsonObject.adding(SetModelObject(Number: selectePhonenumber ?? "", FormName: "HIPAA Acknowledgement", formNumber: 2))
                                MedicalFormJsonObject.adding(SetModelObject(Number: selectePhonenumber ?? "", FormName: "Assignment of Benefits", formNumber: 3))
                                MedicalFormJsonObject.adding(SetModelObject(Number: selectePhonenumber ?? "", FormName: "Notice of Privacy Form", formNumber: 4))
                                MedicalFormJsonObject.adding(SetModelObject(Number: selectePhonenumber ?? "", FormName: "Notice of Privacy signature Form", formNumber: 5))
                                self.isShowing = true

                                self.observerChekingAppointment.setChekingAppoitmentStep(Checkingparamters:MedicalFormJsonObject as! [Any],Infoparamters:["":""],id: self.Selectedmodel.appointment_id ?? "", type: "consent") { (cheking) in
                                    if cheking == true
                                    {
                                        self.isShowing = false
                                        self.isSignAllForm = true
                                        self.isSigUpAllFormTap = true
                                        self.isPaymentEnabled = true
                                    }
                                }
                            }) {
                                HStack {
                                    Text("Sign all forms")
                                        .font(.system(.body))
                                        .fontWeight(.medium)
                                        .foregroundColor(Color.white)
                                        .padding(.vertical, 15)
                                }
                                .frame(minWidth: 0, maxWidth: .infinity)
                                .background(kPrimaryRegularColor)
                                .cornerRadius(10)
                            }.padding(.bottom, 15)
                        } else {
                            Button(action: {
                                print("Sign all forms")
                                
                            }) {
                                HStack {
                                    Text("Sign all forms")
                                        .font(.system(.body))
                                        .fontWeight(.medium)
                                        .foregroundColor(Color.white)
                                        .padding(.vertical, 15)
                                }
                                .frame(minWidth: 0, maxWidth: .infinity)
                                .background(kCustomLightGrayDisable)
                                .cornerRadius(10)
                            }.padding(.bottom, 15)
                        }
                        
                        if self.isPaymentEnabled {
                            
                            NavigationLink(destination: CheckInCompleteView(rootIsActive: self.$rootIsActive,GoToAppointmentTab:false,Selectedmodel: self.Selectedmodel), isActive: self.$showCheckInCompleteView){
                                Button(action: {
                                    print("Payment tapped!")
                                    self.isPaymentTap = true
                                }) {
                                    HStack {
                                        Text("Payment")
                                            .font(.system(.body))
                                            .fontWeight(.medium)
                                            .foregroundColor(Color.white)
                                            .padding(.vertical, 15)
                                    }
                                    .frame(minWidth: 0, maxWidth: .infinity)
                                    .background(kPrimaryRegularColor)
                                    .cornerRadius(10)
                                }
                            }
                            
                        } else {
                            NavigationLink(destination: CheckInCompleteView(rootIsActive: self.$rootIsActive,GoToAppointmentTab:false,Selectedmodel: self.Selectedmodel), isActive: self.$showCheckInCompleteView){
                                Button(action: {
                                    
                                    self.showCheckInCompleteView = true
                                    
                                }) {
                                    HStack {
                                        Text("Payment")
                                            .font(.system(.body))
                                            .fontWeight(.medium)
                                            .foregroundColor(Color.white)
                                            .padding(.vertical, 15)
                                    }
                                    .frame(minWidth: 0, maxWidth: .infinity)
                                    .background(kCustomLightGrayDisable)
                                    .cornerRadius(10)
                                }
                            }
                            
                        }
                        
                    
                    
                }.padding(.horizontal, 20).padding(.bottom, 20).background(kCustomLightBlueColor)
                    .navigationBarTitle("Step 2 of 2", displayMode: .inline)
                    .navigationBarItems(leading: HStack {
                        Button(action: {
                            _ = CheckInView()
                            
                            self.presentationMode.wrappedValue.dismiss()
                        }){Image("ic_back")}
                    })
                    .onAppear() {
                        if self.Selectedmodel.checkin_consent != nil && self.Selectedmodel.checkin_consent !=  "" {
                            self.isSignAllForm = true
                            self.isPaymentEnabled = true
                        } else {
                            self.isPaymentEnabled = false
                            self.isSignAllForm = false
                        }
                }
                if self.isPaymentTap {
                    PaymentWithPaypal(authorization: PayPayInfo.clientTokenOrTokenizationKey) { (controller, result, error) in
                        if (error != nil) {
                            print("ERROR")
                        } else if (result?.isCancelled == true) {
                            print("CANCELLED")
                        } else if let result = result {
                            print("result \(result)")
                            self.observerChekingAppointment.setChekingAppoitmentStep(Checkingparamters:[""],Infoparamters:["":""],
                                                                                     id: self.Selectedmodel.appointment_id ?? "",
                                                                                     type: "complete") { (cheking) in
                                                                                        self.isShowing = true
                                                                                        if cheking == true {
                                                                                            self.isShowing = false
                                                                                            self.showCheckInCompleteView = true
                                                                                        }
                            }
                        }
                        self.isPaymentTap = false
                    }
                }
            }
        }
    }
    
    func getCheingkingAllSign(date:String) -> String {
        self.dateformate.dateFormat = "MM/dd/YYYY HH:mm:SS"
        
        if date == "" {
            return dateformate.string(from: Date())
        } else {
            let chekingDate = dateformate.date(from: date)
            return dateformate.string(from: chekingDate ?? Date())
        }
    }
}

struct MedicalFormsView_Previews: PreviewProvider {
    static var previews: some View {
        MedicalFormsView(rootIsActive: .constant(false))
    }
}

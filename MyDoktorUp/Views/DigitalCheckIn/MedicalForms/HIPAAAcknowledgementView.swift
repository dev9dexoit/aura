//
//  HIPAAAcknowledgementView.swift
//  MyDoktorUp
//
//  Created by Mac on 07/03/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI

struct HIPAAAcknowledgementView: View {

    @ObservedObject var observerChekingAppointment = ChekingAppointment_Observer()
    var title: String
    var formNumber:Int
    var PhoneNumber:String
    var id:String
    @State var isShowing = false
    
    var body: some View {
        
        LoadingView(isShowing: .constant(isShowing)) {
            ZStack() {
                VStack {
                    PDFKitRepresentedView(URL(string: "http://www.africau.edu/images/default/sample.pdf")!)
                    
                    Button(action: {
                                                   
                        MedicalFormJsonObject = NSArray()
                        MedicalFormJsonObject.adding(SetModelObject(Number: self.PhoneNumber, FormName: self.title, formNumber: 1))
                                                 
                        self.observerChekingAppointment.setChekingAppoitmentStep(Checkingparamters:MedicalFormJsonObject as! [Any],Infoparamters:["":""],id: self.id, type: "consent") { (cheking) in
                                                                               self.isShowing = true
                                                                               if cheking == true
                                                                               {
                                                                                   self.isShowing = false
                                                                                }
                                                                           }
                                                                        }) {
                                                                           HStack {
                                                                               Text("Sign forms")
                                                                                   .font(.system(.body))
                                                                                   .fontWeight(.medium)
                                                                                   .foregroundColor(Color.white)
                                                                                   .padding(.vertical, 15)
                                                                           }
                                                                           .frame(minWidth: 0, maxWidth: .infinity)
                                                                           .background(kPrimaryRegularColor)
                                                                           .cornerRadius(10)
                        }.padding(.bottom, 15).padding(.horizontal, 20).padding(.top, 10)
                    
                }
                
                
                
            }
        }
    }
}

struct HIPAAAcknowledgementView_Previews: PreviewProvider {
    static var previews: some View {
        HIPAAAcknowledgementView(title: "", formNumber: 0,PhoneNumber:"",id:"")
    }
}

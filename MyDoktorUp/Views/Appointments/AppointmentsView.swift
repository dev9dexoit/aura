//
//  AppointmentsView.swift
//  MyDoktorUp
//
//  Created by Mac on 05/02/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI
import CoreLocation
struct AppointmentsView: View {
    
    @EnvironmentObject var appState: AppState
    @ObservedObject var appointments = AppointmentListObserver()
    @State  var isShowing = true
    @State var isReaload = false
    
    @State var rootIsActive: Bool = false
    
    var body: some View {
        
        LoadingView(isShowing: .constant(isShowing)) {
            
            
            
            
            NavigationView {
                //                List {
                if self.appointments.appointmentData.count == 0 {
                    VStack {
                        // No Appointments are available
                        Text("Nothing to show here.")
                            .font(.headline)
                            .fontWeight(.bold)
                            .foregroundColor(kCustomPrimaryDarkColor)
                            .padding(.bottom, 5)
                        Text("Your past and future appointments will show here.")
                            .font(.headline)
                            .fontWeight(.regular)
                            .multilineTextAlignment(.center)
                            .foregroundColor(kCustomPrimaryDarkColor)
                        
                    }
                    .navigationBarTitle("Appointments", displayMode: .inline)
                    
                } else  {
                    List {
                        ForEach(self.appointments.appointmentData) { section in
                            Section(header:
                                HStack {
                                    Text(section.title)
                                        .font(.title)
                                        .fontWeight(.light)
                                        .foregroundColor(kPrimaryRegularColor)
                                        .foregroundColor(kPrimaryRegularColor)
                                        .padding(.top, 8)
                                        .padding(.bottom, 8)
                                        .padding(.leading, 18)
                                    Spacer()
                                }
                                .background(kCustomLightBlueColor)
                                    
                                .listRowInsets(
                                    EdgeInsets(top: 0,leading: 0,bottom: 0,trailing: 0)
                            )) {
                                
                                
                                
                                ForEach(section.items, id: \.self) { model in
                                    VStack() {
                                        
                                        if section.title == AppointmentType.checkedin_appointments {
                                            
                                            CheckedInAppointmentRowView(section: section.title, model: model)
                                                .padding()
                                                .background(Color.white)
                                                
                                                .clipped()
                                                .shadow(color: kCustomGrayColor, radius: 2, x: 0, y: 2)
                                                .border(kCustomLightGrayDividerColor, width: 0)
                                            VStack() {
                                                
                                                if getHideAccordingMinute(Model: model)
                                                {
                                                    JoinVideoCall(model:model, isLoading: self.$isShowing)
                                                    
                                                }
                                            }
                                        } else if section.title == AppointmentType.scheduled {
                                           
                                            ScheduledAppointmentRowView(section: section.title, model: model)
                                                .padding()
                                                .background(Color.white)
                                                .clipped()
                                                .shadow(color: kCustomGrayColor, radius: 2, x: 0, y: 2)
                                                .border(kCustomLightGrayDividerColor, width: 0)
                                            
                                            VStack() {
                                                if getHideAccordingHour(date: model.appointment_date ?? "\(Date())", formate: "MM-dd-yyyy")
                                                {
                                                    DigitalChekingBtn(model: model)
                                                
                                                }
                                            }
                                            
                                        } else if section.title == AppointmentType.past  {
                                            // Past Appointment list
                                            PastAppointmentRowView(section: section.title, model: model)
                                                .padding()
                                                .background(Color.white)
                                                .clipped()
                                                .shadow(color: kCustomGrayColor, radius: 2, x: 0, y: 2)
                                        }
                                    }.padding(.horizontal, -4)
                                }
                                .buttonStyle(BorderlessButtonStyle())
                                    
                                .listRowBackground(kCustomLightBlueColor)
                                
                            }
                        }
                        
                        
                    }
                    .navigationBarTitle("Appointments", displayMode: .inline)
                }
                
                
            }
            .navigationBarBackButtonHidden(true)
            .hideNavigationBar()
                
            .onAppear() {
                UITableViewCell.appearance().selectionStyle = .none
                UITableView.appearance().separatorStyle = .none
                UITableViewCell.appearance().accessoryType = .none
                self.isShowing = true
                
                self.appointments.getAppointmentList(ForDigitalCheking:false,
                                                     startDate: Utils.getLast30DaysFromToday(isRequest: true),
                                                     endDate: Utils.getTodayWith20DaysFromToday(isRequest: true),
                                                     type: "grouped",
                                                     completionHander: { (isSuccess) in
                                                        if isSuccess {
                                                            
                                                            self.isShowing = false
                                                            self.isReaload = true
                                                            
                                                        } else {
                                                            
                                                            self.isShowing = false
                                                        }
                })
            }
            .onDisappear(){
                self.isShowing = true
            }
        }.navigationViewStyle(StackNavigationViewStyle()) // This line is supporting iPad, we should not comment out this line
        
        
    }
}


struct AppointmentsView_Previews: PreviewProvider {
    static var previews: some View {
        AppointmentsView(appointments: AppointmentListObserver())
    }
}


struct DigitalChekingBtn: View {
    @State var rootIsActive: Bool = false
    var model = AppointmentModel()
    @State var isDetails = false
    
    
    var body: some View {
        ZStack{
            
            NavigationLink(
                destination:PersonalInfoView(rootIsActive: self.$rootIsActive,SelectedId:model.appointment_id!),
                isActive: self.$isDetails) {
                    EmptyView()
            }
            
            
            Button(action: {
                self.isDetails = true
            }) {
                Text("Digital Check-in")
                    .fontWeight(.medium)
                    .frame(minWidth: 0, maxWidth: .infinity)
                    .padding(.vertical, 10)
                    .foregroundColor(Color.white)
            }
            .background(kPrimaryRegularColor)
            .clipped()
            .shadow(color: kCustomGrayColor, radius: 2, x:  0, y: 2)
        }
    }
    
}


struct JoinVideoCall: View {
    var model = AppointmentModel()
    @Binding var isLoading:Bool
    @ObservedObject var providerDetails = ProviderDetailObserver()
    var SelectedPorvider = Provider()
    @State  var isShowingAlert = false
    @State  var isShowingDistanceAlert = false
    @ObservedObject var observerChekingAppointment = ChekingAppointment_Observer()
    
    var body: some View {
        Button(action: {
            if self.model.appointment_join == "" || self.model.appointment_join == "null" ||  self.model.appointment_join == nil {
                self.isShowingAlert = true
            } else {
                self.providerDetails.getProviderDetail(provideId: UserDefaultsManager().providerID ?? "") { (isSuccess) in
                    self.isLoading = true
                    if isSuccess {
                        
                        self.isLoading = false
                        
                        let userLat = CLLocationManager().location?.coordinate.latitude
                        let userLong = CLLocationManager().location?.coordinate.longitude
                        
                        let Usercoordinate =  CLLocation(latitude: userLat! , longitude: userLong!)
                        
                        let lat = (self.providerDetails.arrProviders.first!.latitude! as NSString).doubleValue
                        let lon = (self.providerDetails.arrProviders.first!.longitude! as NSString).doubleValue
                        
                        let Providercoordinate₀ = CLLocation(latitude: lat , longitude: lon)
                        
                        let distace = Usercoordinate.distance(from: Providercoordinate₀)
                        if distace <= 500 {
                            
                            self.observerChekingAppointment.setChekingAppoitmentStep(Checkingparamters:[""],Infoparamters:["":""],id: self.model.appointment_id ?? "", type: "arrived") { (cheking) in
                                self.isLoading = true
                                if cheking == true {
                                    self.isLoading = false
                                    print("sucess")
                                }
                            }
                        } else {
                            self.isShowingDistanceAlert = true
                        }
                        self.isLoading = false
                    } else {
                        print("Failure")
                        self.isLoading = false
                    }
                }
            }
        }) {
            if model.appointment_join  == "true" || model.appointment_join == "" || model.appointment_join == "null" {
                Text("Digital Check-in")
                    .fontWeight(.medium)
                    .frame(minWidth: 0, maxWidth: .infinity)
                    .padding(.vertical, 10)
                    .foregroundColor(Color.white)
                
            } else {
                Text("Arrived")
                    .fontWeight(.medium)
                    .frame(minWidth: 0, maxWidth: .infinity)
                    .padding(.vertical, 10)
                    .foregroundColor(Color.white)
            }
        }
        .background(kCustomOrangeColor)
        .clipped()
        .shadow(color: kCustomGrayColor, radius: 2, x:  0, y: 2)
        .alert(isPresented: self.$isShowingAlert) {
            return Alert(title: Text("Video call link is not available. Please contact Provider"))
        }
        .alert(isPresented:self.$isShowingDistanceAlert) {
            return Alert(title: Text("Looks like you are away from the Doctor’s office. Please reach to the office and try again."))
        }
    }
}

struct CheckedInAppointmentRowView: View {
    var section: String
    var model = AppointmentModel()
    @State var isActive = false
    
    var body: some View {
        NavigationLink(destination: CheckedIn_AppointmentDetailsView(SelectedId:model.appointment_id!)) {
            VStack(alignment: .leading, spacing: 0) {
                Text("\(model.provider_first_name ?? "") \(model.provider_last_name ?? "")").font(.headline).fontWeight(.bold).foregroundColor(kPrimaryRegularColor).lineLimit(nil)
                
                HStack{
                    VStack(alignment: .leading, spacing: 2) {
                        
                        Text("\(model.taxonomy_name ?? "")").foregroundColor(Color.black).font(.footnote).fontWeight(.medium).lineLimit(nil)
                            .padding(.top, 5)
                        Text("\(model.provider_address1 ?? "") \(model.provider_address2 ?? "") \(model.provider_city ?? "" ) \(model.provider_state ?? "")" ).foregroundColor(kCustomGrayColor).font(.footnote).fontWeight(.regular)
                            .padding(.top, 3).lineLimit(nil)
                        Text("\(model.distance ?? "2.00") Miles").foregroundColor(kCustomDarkGrayColor).font(.footnote).fontWeight(.medium).lineLimit(nil)
                            .padding(.top, 3)
                    }
                    Spacer()
                    VStack(alignment: .leading, spacing: 2) {
                        HStack {
                            Spacer()
                            
                            Text("\(model.appointment_date?.convertDateFormat(inputDate: model.appointment_date ?? "", inputFormat: "MM-dd-yyyy", format: "MMM d, yyyy") ?? "")" + " - " + "\(model.appointment_time?.convertDateFormat(inputDate: model.appointment_time ?? "", inputFormat: "HH:mm", format: "hh:mma") ?? "")")
                                .foregroundColor(kCustomOrangeColor).font(.callout).fontWeight(.light).lineLimit(nil).multilineTextAlignment(.trailing)
                        }
                        HStack {
                            Spacer()
                            
                            Text("\(model.provider_phone?.formatePhoneNumber() ?? "")").foregroundColor(kPrimaryRegularColor).font(.callout).fontWeight(.regular).padding(.top, 2).lineLimit(nil)
                        }
                    }
                }
                
                
            }
        }.isDetailLink(false)
    }
}


struct ScheduledAppointmentRowView: View {
    
    var section: String
    var model = AppointmentModel()
    @State var goDetails = false
    var body: some View {
        NavigationLink(destination: ScheduledAppointmentDetailView(SelectedId:model.appointment_id!,isBackView: self.$goDetails),isActive: self.$goDetails) {
            VStack(alignment: .leading, spacing: 0) {
                Text("\(model.provider_first_name ?? "") \(model.provider_last_name ?? "")").font(.headline).fontWeight(.bold).foregroundColor(kPrimaryRegularColor).lineLimit(nil)
                
                HStack{
                    VStack(alignment: .leading, spacing: 2) {
                        
                        Text("\(model.taxonomy_name ?? "")").foregroundColor(Color.black).font(.footnote).fontWeight(.medium)
                            .padding(.top, 5).lineLimit(nil)
                        Text("\(model.provider_address1 ?? "") \(model.provider_address2 ?? "") \(model.provider_city ?? "" ) \(model.provider_state ?? "")" ).foregroundColor(kCustomGrayColor).font(.footnote).fontWeight(.regular)
                            .padding(.top, 3).lineLimit(nil)
                        Text("\(model.distance ?? "2.00") Miles").foregroundColor(kCustomDarkGrayColor).font(.footnote).fontWeight(.medium).lineLimit(nil)
                            .padding(.top, 3)
                    }
                    Spacer()
                    VStack(alignment: .leading, spacing: 2) {
                        HStack {
                            Spacer()
                            
                            Text("\(model.appointment_date?.convertDateFormat(inputDate: model.appointment_date ?? "", inputFormat: "MM-dd-yyyy", format: "MMM d, yyyy") ?? "")" + " - " + "\(model.appointment_time?.convertDateFormat(inputDate: model.appointment_time ?? "", inputFormat: "HH:mm", format: "hh:mma") ?? "")")
                                .foregroundColor(kCustomOrangeColor).font(.callout).fontWeight(.light).lineLimit(nil).multilineTextAlignment(.trailing)
                        }
                        HStack {
                            Spacer()
                            
                            Text("\(model.provider_phone?.formatePhoneNumber() ?? "")").foregroundColor(kPrimaryRegularColor).font(.callout).fontWeight(.regular).padding(.top, 2).lineLimit(nil)
                        }
                    }
                }
                
            }.onTapGesture {
                self.goDetails = true
            }
            
            
            
        }
        .isDetailLink(false)
    }
}

struct PastAppointmentRowView: View {
    
    var section: String
    var model = AppointmentModel()
    
    var body: some View {
        NavigationLink(destination: PastAppointmentDetailView(SelectedId:model.appointment_id!)) {
            VStack(alignment: .leading, spacing: 0) {
                Text("\(model.provider_first_name ?? "") \(model.provider_last_name ?? "")").font(.headline).fontWeight(.bold).foregroundColor(kPrimaryRegularColor).lineLimit(nil)
                
                HStack {
                    VStack(alignment: .leading, spacing: 2) {
                        
                        Text("\(model.taxonomy_name ?? "")").foregroundColor(Color.black).font(.footnote).fontWeight(.medium)
                            .padding(.top, 5).lineLimit(nil)
                        Text("\(model.provider_address1 ?? "") \(model.provider_address2 ?? "") \(model.provider_city ?? "" ) \(model.provider_state ?? "")" ).foregroundColor(kCustomGrayColor).font(.footnote).fontWeight(.regular)
                            .padding(.top, 3).lineLimit(nil)
                        Text("\(model.distance ?? "2.00") Miles").foregroundColor(kCustomDarkGrayColor).font(.footnote).fontWeight(.medium).lineLimit(nil)
                            .padding(.top, 3)
                    }
                    Spacer()
                    VStack(alignment: .leading, spacing: 2) {
                        HStack
                            {
                                Spacer()
                                
                                Text("\(model.appointment_date?.convertDateFormat(inputDate: model.appointment_date ?? "", inputFormat: "MM-dd-yyyy", format: "MMM d, yyyy") ?? "")" + " - " + "\(model.appointment_time?.convertDateFormat(inputDate: model.appointment_time ?? "", inputFormat: "HH:mm", format: "hh:mma") ?? "")")
                                    .foregroundColor(kCustomOrangeColor).font(.callout).fontWeight(.light).lineLimit(nil).multilineTextAlignment(.trailing)
                                
                                
                        }
                        HStack {
                            Spacer()
                            
                            Text("\(model.provider_phone?.formatePhoneNumber() ?? "")").foregroundColor(kPrimaryRegularColor).font(.callout).fontWeight(.regular).lineLimit(nil).padding(.top, 2)
                        }
                    }
                }
                
            }
        }
        .isDetailLink(false)
        .border(kCustomLightGrayDividerColor, width: 0)
    }
}

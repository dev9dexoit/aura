//
//  PastAppointmentDetailView.swift
//  MyDoktorUp
//
//  Created by Mac on 29/02/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI

struct PastAppointmentDetailView: View {
    @EnvironmentObject var appState: AppState

    @ObservedObject var appointments = AppointmentListObserver()
    @State var Selectedmodel = AppointmentModel()
    @State var drNote = ""
    @State var SelectedId = ""
    @Environment(\.presentationMode) var presentationMode
    @State var textHeight: CGFloat = 150
    @State var isReBooking = false
    @State var isShowing = true
    
    var body: some View {
        ZStack {
            VStack {
                VStack {
                    Text("Past appointment")
                        .font(Font.system(size: 28))
                        .fontWeight(.light)
                        .foregroundColor(kPrimaryRegularColor)
                        .padding(.bottom, 10)
                }
                
                VStack(alignment: .leading, spacing: 15) {
                    
                    HStack {
                        Spacer()
                        Text("\(self.Selectedmodel.appointment_date?.convertDateFormat(inputDate: self.Selectedmodel.appointment_date ?? "", inputFormat: "MM/dd/yyyy", format: "MMM d, yyyy") ?? "")" + " - " + "\(self.Selectedmodel.appointment_time?.convertDateFormat(inputDate: self.Selectedmodel.appointment_time ?? "", inputFormat: "HH:mm", format: "hh:mma") ?? "")")
                            
                            .font(Font.system(size: 19)).fontWeight(.regular).foregroundColor(kCustomOrangeColor)
                        Spacer()
                    }
                    HStack {
                        Spacer()
                        if  self.Selectedmodel.booking_forself?.uppercased() == "YES" {
                            Text("Appointment for: Myself").font(Font.system(size: 22)).fontWeight(.regular).foregroundColor(kCustomTextBlueColor)
                        } else {
                            Text("Appointment for: \(self.Selectedmodel.booking_forself ?? "")").font(Font.system(size: 22)).fontWeight(.regular).foregroundColor(kCustomTextBlueColor)
                        }
                        
                        Spacer()
                    }
                    HStack {
                        Spacer()
                        Text("Visit reason: \(self.Selectedmodel.visit_reason ?? "")").font(Font.system(size: 20)).fontWeight(.regular).foregroundColor(kCustomTextBlueColor)
                        Spacer()
                        
                    }
                    
                }.padding(.horizontal, 40)
                    .padding(.top, 10)
                
                VStack(alignment: .leading, spacing: 5) {
                    Text("Notes to doctor:").font(.system(.body)).fontWeight(.regular).foregroundColor(kCustomGrayColor).padding(.top, 10)
                    
                    TextView(placeholder: "", text: self.$drNote, isSelection: false, isEditable: false, isUserInteractionEnabled: false, fontSize: 16, minHeight: self.textHeight, calculatedHeight: self.$textHeight)
                        .frame(minHeight: self.textHeight, maxHeight: self.textHeight)
                        
                        .disabled(true)
                    
                }.padding(.top, 15)
                Spacer()
                
                NavigationLink(destination: DoctorProfileView(isNavigationBarHidden: .constant(true)), isActive: self.$isReBooking) {
                    EmptyView()
                    Button(action: {
                            self.appState.reloadDashboard(selectedTab: 0, selectedProvider: Provider())

                    }) {
                        HStack {
                            
                            Text("Re-book")
                                .foregroundColor(.white)
                                .padding(.vertical, 12)
                        }
                        .frame(minWidth: 0, maxWidth: .infinity)
                        .background(kPrimaryRegularColor)
                        .cornerRadius(10)
                    }
                }
            }.padding()                
                .navigationBarTitle("Details", displayMode: .inline)
                .navigationBarBackButtonHidden(true)
                .navigationBarItems(leading:
                    HStack {
                        Button(action: {
                            self.presentationMode.wrappedValue.dismiss()
                        }) {
                            Image("ic_back")
                        }
                        .padding(.leading, -8)
                })
        }
        .background(kCustomLightBlueColor)
        .onAppear {
            self.appointments.GetAppointmentInfo(
                AppointmentId: self.SelectedId,
                completionHander: { (isSuccess) in
                    self.isShowing = true
                    if isSuccess {
                        
                        self.Selectedmodel = self.appointments.appointmentInfoModel
                        self.drNote = self.Selectedmodel.visit_note ??  ""
                        print("Success")
                        self.isShowing = false
                        
                    } else {
                        self.isShowing = false
                    }
            })
        }
        
    }
}

struct PastAppointmentDetailView_Previews: PreviewProvider {
    static var previews: some View {
        PastAppointmentDetailView()
    }
}

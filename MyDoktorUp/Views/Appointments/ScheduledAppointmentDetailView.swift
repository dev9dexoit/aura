//
//  AppointmentDetailView.swift
//  MyDoktorUp
//
//  Created by Mac on 28/02/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI

enum CancelAoointmentAlert {
    case AlertPopUp,SucessPopup
}

struct ScheduledAppointmentDetailView: View {
    
    @EnvironmentObject var appState: AppState
    @ObservedObject var appointments = AppointmentListObserver()
    @Environment(\.presentationMode) var presentationMode
    @State private var activeAlert: CancelAoointmentAlert = .AlertPopUp
    @State var Selectedmodel = AppointmentModel()
    @State var drNote = ""
    @State var SelectedId = ""
    @State var textHeight: CGFloat = 150
    @State var isCancelAletApointment = false
    @State var isShowing = true
    @State var isRescheduleApointment = false

    @State var isSucessCancelApointment = false
    @Binding var isBackView:Bool
    var body: some View {
        LoadingView(isShowing: .constant(isShowing)) {
            ZStack {
                VStack {
                    VStack {
                        Text("Scheduled appointment")
                            .font(Font.system(size: 28))
                            .fontWeight(.light)
                            .foregroundColor(kPrimaryRegularColor)
                            .padding(.bottom, 10)
                        VStack(alignment: .leading, spacing: 15) {
                            
                            HStack {
                                Spacer()
                                Text("\(self.Selectedmodel.appointment_date?.convertDateFormat(inputDate: self.Selectedmodel.appointment_date ?? "", inputFormat: "MM/dd/yyyy", format: "MMM d, yyyy") ?? "")" + " - " + "\(self.Selectedmodel.appointment_time?.convertDateFormat(inputDate: self.Selectedmodel.appointment_time ?? "", inputFormat: "HH:mm", format: "hh:mma") ?? "")")
                                    
                                    .font(Font.system(size: 19)).fontWeight(.regular).foregroundColor(kCustomOrangeColor)
                                Spacer()
                            }
                            
                            HStack {
                                Spacer()
                                if  self.Selectedmodel.booking_forself?.uppercased() == "YES" {
                                    
                                    Text("Appointment for: Myself").font(Font.system(size: 22)).fontWeight(.regular).foregroundColor(kCustomTextBlueColor)
                                } else {
                                    Text("Appointment for: \(self.Selectedmodel.booking_forself ?? "")").font(Font.system(size: 22)).fontWeight(.regular).foregroundColor(kCustomTextBlueColor)
                                }
                                
                                Spacer()
                            }
                            
                            HStack {
                                Spacer()
                                Text("Visit reason: \(self.Selectedmodel.visit_reason ?? "")").font(Font.system(size: 20)).fontWeight(.regular).foregroundColor(kCustomTextBlueColor)
                                Spacer()
                            }
                            
                        }.padding(.horizontal, 40)
                            .padding(.top, 10)
                        
                        VStack(alignment: .leading, spacing: 5){
                            Text("Notes to doctor:").font(.system(.body)).fontWeight(.regular).foregroundColor(kCustomGrayColor).padding(.top, 10)
                            
                            TextView(placeholder: "", text: self.$drNote, isSelection: false, isEditable: false, isUserInteractionEnabled: false, fontSize: 16, minHeight: self.textHeight, calculatedHeight: self.$textHeight)
                                .frame(minHeight: self.textHeight, maxHeight: self.textHeight)
                                
                                .disabled(true)
                        }
                        Spacer()
                        VStack(alignment: .leading, spacing: 5) {
                            
                            Button(action: {
                                print("Digital Check-in")
                            }) {
                                HStack {
                                    Text("Digital Check-in")
                                        .foregroundColor(.white)
                                        .padding(.vertical, 12)
                                    
                                }
                                .frame(minWidth: 0, maxWidth: .infinity)
                                .background(kPrimaryRegularColor)
                                .cornerRadius(10)
                            }.padding(.top, 10)
                            
                            Button(action: {
                                //self.appState.reloadDashboard(selectedTab: 0, selectedProvider: Provider())
                                self.activeAlert = .AlertPopUp
                                self.isRescheduleApointment = true
                            }) {
                                Text("Reschedule")
                                    .font(.system(.body))
                                    .fontWeight(.medium)
                                    .foregroundColor(kCustomGrayColor)
                                    .padding(.vertical, 12)
                                    .frame(minWidth: 0, maxWidth: .infinity)
                                    .overlay(
                                        RoundedRectangle(cornerRadius: 10)
                                            .strokeBorder(style: StrokeStyle(lineWidth: 1))
                                            .foregroundColor(kCustomGrayColor)
                                            .cornerRadius(10)
                                )
                            }
                            .padding(.top, 15)
                            .background(Color.clear)
                            .alert(isPresented: self.$isRescheduleApointment){
                                
                                switch self.activeAlert {
                                case .AlertPopUp :
                                    
                                    return Alert(title: Text("Reschedule will cancel this appointment. Are you shure you want to cancel and reschedule?"), message: Text(""),primaryButton: .default(Text("Ok"), action: {
                                        
                                        self.isShowing = true
                                        self.appointments.CancelAppointmentList(AppointmentId: self.Selectedmodel.appointment_id ?? "") { (isSucces) in
                                            if isSucces {
                                                self.isShowing = false
                                                self.activeAlert = .SucessPopup
                                                self.isCancelAletApointment = true
                                            } else {
                                                self.isShowing = false
                                            }
                                        }
                                        
                                    }),secondaryButton: .cancel())
                                    
                                case .SucessPopup:
                                    return  Alert(title: Text("Succesfully cancel this appointment."), message: Text(""),dismissButton: .default(Text("Ok"), action: {
                                        self.appState.reloadDashboard(selectedTab: 0, selectedProvider: Provider())
                                        
                                    }))
                                    
                                }
                                
                            }
                            
                            
                            
                            if self.Selectedmodel.status != "CANCELLED"{
                                Button(action: {
                                    self.activeAlert = .AlertPopUp
                                    
                                    self.isCancelAletApointment = true
                                    

                                }) {
                                    Text("Cancel appointment")
                                        .font(.system(.body))
                                        .fontWeight(.medium)
                                        .foregroundColor(kCustomGrayColor)
                                        .padding(.vertical, 12)
                                        .frame(minWidth: 0, maxWidth: .infinity)
                                        .overlay(
                                            RoundedRectangle(cornerRadius: 10)
                                                .strokeBorder(style: StrokeStyle(lineWidth: 1))
                                                .foregroundColor(kCustomGrayColor)
                                                .cornerRadius(10)
                                            
                                    )
                                }
                                .padding(.top, 15)
                                .background(Color.clear)
                                .alert(isPresented: self.$isCancelAletApointment){
                                    
                                    switch self.activeAlert {
                                    case .AlertPopUp :
                                        
                                        return Alert(title: Text("Are you sure you want to cancel this appointment?"), message: Text(""),primaryButton: .default(Text("Ok"), action: {
                                            
                                            self.isShowing = true
                                            self.appointments.CancelAppointmentList(AppointmentId: self.Selectedmodel.appointment_id ?? "") { (isSucces) in
                                                if isSucces {
                                                    self.isShowing = false
                                                    self.activeAlert = .SucessPopup
                                                    self.isCancelAletApointment = true
                                                } else {
                                                    self.isShowing = false
                                                }
                                            }
                                            
                                        }),secondaryButton: .cancel())
                                        
                                    case .SucessPopup:
                                        return  Alert(title: Text("Succesfully cancel this appointment."), message: Text(""),dismissButton: .default(Text("Ok"), action: {
                                                                            
                                            self.appState.reloadDashboard(selectedTab: 0, selectedProvider: Provider())

                                            
                                            
                                        }))
                                        
                                    }
                                }
                            }
                            
                            
                            
                            
                        }.padding(.top, 10)
                    }
                    
                }.padding()
                    .navigationBarTitle("Details", displayMode: .inline)
                    .navigationBarBackButtonHidden(true)
                    .navigationBarItems(leading:
                        HStack {
                            Button(action: {
                                self.isBackView = false
                            }) {
                                Image("ic_back")
                            }
                            .padding(.leading, -8)
                    })
            }
            .background(kCustomLightBlueColor)
            .onAppear() {
                
                self.appointments.GetAppointmentInfo(
                    AppointmentId: self.SelectedId,
                    completionHander: { (isSuccess) in
                        self.isShowing = true
                        if isSuccess {
                            self.Selectedmodel = self.appointments.appointmentInfoModel
                            print("self.Selectedmodel \(self.Selectedmodel)")
                            
                            self.drNote = self.Selectedmodel.visit_note ?? ""
                            print("Success")
                            self.isShowing = false
                            
                        } else {
                            self.isShowing = false
                        }
                })
            }
        }
    }
}

struct AppointmentDetailView_Previews: PreviewProvider {
    static var previews: some View {
        ScheduledAppointmentDetailView( isBackView: .constant(false))
    }
}

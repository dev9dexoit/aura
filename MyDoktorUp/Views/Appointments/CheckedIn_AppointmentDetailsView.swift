//
//  Checked_n_AppointmentDetailsView.swift
//  MyDoktorUp
//
//  Created by MACBOOK PRO on 25/04/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI
import CoreLocation

struct CheckedIn_AppointmentDetailsView: View {
    
    @ObservedObject var providerDetails = ProviderDetailObserver()
    @ObservedObject var appointments = AppointmentListObserver()
    @Environment(\.presentationMode) var presentationMode
    @State private var activeAlert: CancelAoointmentAlert = .AlertPopUp
    @EnvironmentObject var appState: AppState
    @State var Selectedmodel = AppointmentModel()
    @State var drNote = ""
    @State var SelectedId = ""
    @State var textHeight: CGFloat = 150
    @State var isCancelAletApointment = false
    @State var isCancelApointment = false
    @State var isRescheduleApointment = false
    @State private var isShowing = true
    @State  var isShowingAlert = false
    @State  var isShowingDistanceAlert = false
    @ObservedObject var observerChekingAppointment = ChekingAppointment_Observer()
    @State var isSucessCancelApointment = false
    
    var body: some View {
        LoadingView(isShowing: .constant(isShowing)) {
            ZStack {
                VStack {
                    VStack {
                        Text("Checked-In appointment")
                            .font(Font.system(size: 28))
                            .fontWeight(.light)
                            .foregroundColor(kPrimaryRegularColor)
                            .padding(.bottom, 10)
                        VStack(alignment: .leading, spacing: 15) {
                            
                            HStack {
                                Spacer()
                                Text("\(self.Selectedmodel.appointment_date?.convertDateFormat(inputDate: self.Selectedmodel.appointment_date ?? "", inputFormat: "MM/dd/yyyy", format: "MMM d, yyyy") ?? "")" + " - " + "\(self.Selectedmodel.appointment_time?.convertDateFormat(inputDate: self.Selectedmodel.appointment_time ?? "", inputFormat: "HH:mm", format: "hh:mma") ?? "")")
                                    
                                    .font(Font.system(size: 19)).fontWeight(.regular).foregroundColor(kCustomOrangeColor)
                                Spacer()
                            }
                            HStack {
                                Spacer()
                                if  self.Selectedmodel.booking_forself?.uppercased() == "YES" {
                                    Text("Appointment for: Myself").font(Font.system(size: 22)).fontWeight(.regular).foregroundColor(kCustomTextBlueColor)
                                } else {
                                    Text("Appointment for: \(self.Selectedmodel.booking_forself ?? "")").font(Font.system(size: 22)).fontWeight(.regular).foregroundColor(kCustomTextBlueColor)
                                }
                                
                                Spacer()
                            }
                            HStack {
                                Spacer()
                                Text("Visit reason: \(self.Selectedmodel.visit_reason ?? "")").font(Font.system(size: 20)).fontWeight(.regular).foregroundColor(kCustomTextBlueColor)
                                Spacer()
                                
                            }
                            
                        }.padding(.horizontal, 40)
                            .padding(.top, 10)
                        
                        
                        
                        VStack(alignment: .leading, spacing: 5){
                            Text("Notes to doctor:").font(.system(.body)).fontWeight(.regular).foregroundColor(kCustomGrayColor).padding(.top, 10)
                            
                            TextView(placeholder: "", text: self.$drNote, isSelection: false, isEditable: false, isUserInteractionEnabled: false, fontSize: 16, minHeight: self.textHeight, calculatedHeight: self.$textHeight)
                                .frame(minHeight: self.textHeight, maxHeight: self.textHeight)
                                
                                .disabled(true)
                        }
                        
                        Spacer()
                        VStack(alignment: .leading, spacing: 5) {
                            
                            Button(action: {
                                if self.Selectedmodel.appointment_join == "" || self.Selectedmodel.appointment_join == "null" ||  self.Selectedmodel.appointment_join == nil {
                                    self.isShowingAlert = true
                                } else {
                                    self.providerDetails.getProviderDetail(provideId: UserDefaultsManager().providerID ?? "") { (isSuccess) in
                                        self.isShowing = true
                                        if isSuccess {
                                            let userLat = CLLocationManager().location?.coordinate.latitude
                                            let userLong = CLLocationManager().location?.coordinate.longitude
                                            let Usercoordinate =  CLLocation(latitude: userLat! , longitude: userLong!)
                                            let lat = (self.providerDetails.arrProviders.first!.latitude! as NSString).doubleValue
                                            let lon = (self.providerDetails.arrProviders.first!.longitude! as NSString).doubleValue
                                            let Providercoordinate₀ = CLLocation(latitude: lat , longitude: lon)
                                            let distace = Usercoordinate.distance(from: Providercoordinate₀)
                                            
                                            if distace <= 500 {
                                                self.observerChekingAppointment.setChekingAppoitmentStep(Checkingparamters:[""],Infoparamters:["":""],id: self.Selectedmodel.appointment_id ?? "", type: "arrived") { (cheking) in
                                                    self.isShowing = true
                                                    if cheking == true {
                                                        self.isShowing = false
                                                        print("sucess")
                                                    }
                                                }
                                            } else {
                                                self.isShowingDistanceAlert = true
                                            }
                                            self.isShowing = false
                                        } else {
                                            print("Failure")
                                            self.isShowing = false
                                        }
                                    }
                                }
                            }) {
                                
                                
                                
                                if self.Selectedmodel.appointment_join  == "true" || self.Selectedmodel.appointment_join == "" || self.Selectedmodel.appointment_join == "null" {
                                    HStack {
                                        Text("Join Video Call")
                                            .foregroundColor(.white)
                                            .padding(.vertical, 12)
                                    }
                                    .frame(minWidth: 0, maxWidth: .infinity)
                                    .background(kCustomOrangeColor)
                                    .cornerRadius(10)
                                    
                                    
                                    
                                } else {
                                    
                                    if getHideAccordingMinute(Model: self.Selectedmodel)
                                    {
                                        HStack {
                                            Text("Arrived")
                                                .foregroundColor(.white)
                                                .padding(.vertical, 12)
                                            
                                        }
                                        .frame(minWidth: 0, maxWidth: .infinity)
                                        .background(kCustomOrangeColor)
                                        .cornerRadius(10)
                                        
                                    }else
                                    {
                                        HStack {
                                            Text("Arrived")
                                                .foregroundColor(.white)
                                                .padding(.vertical, 12)
                                        }
                                        .frame(minWidth: 0, maxWidth: .infinity)
                                        .background(kCustomLightGrayDisable)
                                        .cornerRadius(10)
                                    }
                                    
                                    
                                    
                                }
                                
                                
                            }.padding(.top, 10)
                                .alert(isPresented: self.$isShowingAlert) {
                                    return Alert(title: Text("Video call link is not available. Please contact Provider"))
                            }
                            .alert(isPresented:self.$isShowingDistanceAlert) {
                                return Alert(title: Text("Looks like you are away from the Doctor’s office. Please reach to the office and try again."))
                            }
                            
                            Button(action: {
                                self.activeAlert = .AlertPopUp
                                self.isRescheduleApointment = true
                                // self.appState.reloadDashboard(selectedTab:0,selectedProvider: Provider())
                            }) {
                                Text("Reschedule")
                                    .font(.system(.body))
                                    .fontWeight(.medium)
                                    .foregroundColor(kCustomGrayColor)
                                    .padding(.vertical, 12)
                                    .frame(minWidth: 0, maxWidth: .infinity)
                                    .overlay(
                                        RoundedRectangle(cornerRadius: 10)
                                            .strokeBorder(style: StrokeStyle(lineWidth: 1))
                                            .foregroundColor(kCustomGrayColor)
                                            .cornerRadius(10)
                                        
                                )
                            }
                            .padding(.top, 15)
                            .background(Color.clear)
                            .alert(isPresented: self.$isRescheduleApointment){
                                
                                switch self.activeAlert {
                                case .AlertPopUp :
                                    
                                    return Alert(title: Text("Reschedule will cancel this appointment. Are you shure you want to cancel and reschedule?"), message: Text(""),primaryButton: .default(Text("Ok"), action: {
                                        
                                        self.isShowing = true
                                        self.appointments.CancelAppointmentList(AppointmentId: self.Selectedmodel.appointment_id ?? "") { (isSucces) in
                                            if isSucces {
                                                self.isShowing = false
                                                self.activeAlert = .SucessPopup
                                                self.isCancelAletApointment = true
                                            } else {
                                                self.isShowing = false
                                            }
                                        }
                                        
                                    }),secondaryButton: .cancel())
                                    
                                case .SucessPopup:
                                    return  Alert(title: Text("Succesfully cancel this appointment."), message: Text(""),dismissButton: .default(Text("Ok"), action: {
                                        self.appState.reloadDashboard(selectedTab: 0, selectedProvider: Provider())
                                        
                                    }))
                                    
                                }
                                
                            }
                            
                            if self.Selectedmodel.status != "CANCELLED"{
                                Button(action: {
                                    self.activeAlert = .AlertPopUp
                                    self.isCancelAletApointment = true
                                }) {
                                    Text("Cancel appointment")
                                        .font(.system(.body))
                                        .fontWeight(.medium)
                                        .foregroundColor(kCustomGrayColor)
                                        .padding(.vertical, 12)
                                        .frame(minWidth: 0, maxWidth: .infinity)
                                        .overlay(
                                            RoundedRectangle(cornerRadius: 10)
                                                .strokeBorder(style: StrokeStyle(lineWidth: 1))
                                                .foregroundColor(kCustomGrayColor)
                                                .cornerRadius(10)
                                            
                                    )
                                }
                                .padding(.top, 15)
                                .background(Color.clear)
                                
                                .alert(isPresented: self.$isCancelAletApointment){
                                    
                                    switch self.activeAlert {
                                    case .AlertPopUp :
                                        
                                        return Alert(title: Text("Are you sure you want to cancel this appointment?"), message: Text(""),primaryButton: .default(Text("Ok"), action: {
                                            
                                            self.isShowing = true
                                            self.appointments.CancelAppointmentList(AppointmentId: self.Selectedmodel.appointment_id ?? "") { (isSucces) in
                                                if isSucces {
                                                    self.isShowing = false
                                                    self.activeAlert = .SucessPopup
                                                    self.isCancelAletApointment = true
                                                } else {
                                                    self.isShowing = false
                                                }
                                            }
                                            
                                        }),secondaryButton: .cancel())
                                        
                                    case .SucessPopup:
                                        return  Alert(title: Text("Succesfully cancel this appointment."), message: Text(""),dismissButton: .default(Text("Ok"), action: {
                                            //                                            self.presentationMode.wrappedValue.dismiss()
                                            self.appState.reloadDashboard(selectedTab: 0, selectedProvider: Provider())
                                            
                                        }))
                                        
                                    }
                                }
                            }
                        }.padding(.top, 20)
                    }
                    
                }.padding()
                    .navigationBarTitle("Details", displayMode: .inline)
                    .navigationBarBackButtonHidden(true)
                    .navigationBarItems(leading:
                        HStack {
                            Button(action: {
                                self.presentationMode.wrappedValue.dismiss()
                            }) {
                                Image("ic_back")
                            }
                            .padding(.leading, -8)
                    })
            }
            .background(kCustomLightBlueColor)
                
            .onAppear() {
                self.appointments.GetAppointmentInfo(
                    AppointmentId: self.SelectedId,
                    completionHander: { (isSuccess) in
                        self.isShowing = true
                        if isSuccess {
                            
                            self.Selectedmodel = self.appointments.appointmentInfoModel
                            
                            self.drNote = self.Selectedmodel.visit_note ?? ""
                            print("Success")
                            self.isShowing = false
                            
                        } else {
                            self.isShowing = false
                        }
                })
                
            }
        }
    }
}

struct CheckedIn_AppointmentDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        CheckedIn_AppointmentDetailsView()
    }
}

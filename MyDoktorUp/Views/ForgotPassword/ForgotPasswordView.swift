//
//  ForgotPasswordView.swift
//  MyDoktorUp
//
//  Created by MACBOOK PRO on 20/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI


enum ForgotPassworsdAlert {
    case InvalidEmail,Success
}

struct ForgotPasswordView: View {
    @Environment(\.presentationMode) var presentationMode
    @State private var activeAlert: ForgotPassworsdAlert = .InvalidEmail
    

        @State private var isShowing = false

        @State var email = ""
        @State var isForgotTap = false
        
            var body: some View {
                LoadingView(isShowing: .constant(isShowing)) {
                    ZStack {
                        kPrimaryRegularColor
                            .edgesIgnoringSafeArea(.all)

                        // Custom Back Button
                        CustomBackButton()
                    

                        VStack(alignment: .center, spacing: 5) {
                            // SignUp title view

                            VStack(alignment: .center, spacing: 20){
                                Image("doktoup_logo").padding(.top, 10)
                                
                                Text("Forgot Password")
                                    .font(.system(.title))
                                    .fontWeight(.bold)
                                    .foregroundColor(Color.white)
                                    .multilineTextAlignment(.center)
                                    
                            }
                            
                            VStack(alignment: .leading, spacing: 10) {
                                VStack(alignment: .leading, spacing: 5) {
                                     ZStack(alignment: .leading) {
                                      if self.email.isEmpty { Text("Email").foregroundColor(.white).padding(.all, 6) }
                                      TextField("", text: self.$email)
                                          .foregroundColor(Color.white)
                                          .padding(.all, 6)
                                          .accentColor(Color.white)
                                    }
                                    DividerView()
                                }
                                
                                Button(action: {
                                    if self.isValidEmail(Email:self.email)
                                    {
                                        self.isShowing = true
                                        SignInObserver().Forgotpassword(username: self.email) { (Result) in
                                            if Result == true
                                            {
                                                self.activeAlert = .Success

                                                self.isForgotTap = true
                                            }
                                        }
                                    }
                                    else
                                    {
                                        self.activeAlert = .InvalidEmail
                                        self.isForgotTap = true
                                    }
                                }) {
                                    HStack {
                                        Text("Reset Password")
                                            .font(.system(.body))
                                            .fontWeight(.medium)
                                            .foregroundColor(Color.white)
                                            .padding(.vertical, 12)
                                    }
                                    .frame(minWidth: 0, maxWidth: .infinity)
                                    .background(kCustomGreenColor)
                                    .cornerRadius(10)
                                }
                                .padding(.top, 10)
                                .alert(isPresented: self.$isForgotTap) {
                                    switch self.activeAlert {
                                    case .Success:
                                        
                                        return Alert(title: Text("The forgot password email has been sent \(self.email)."),dismissButton: .default(Text("Ok"), action: {
                                            self.presentationMode.wrappedValue.dismiss()
                                        }))
                                    
                                    case .InvalidEmail:
                                        return Alert(title: Text("Please enter valid email address"))
                                        }
                                    }
                                
                            }.padding()
                            .fixedSize(horizontal: false, vertical: true)
                        }.padding(.horizontal, 20)
                        .navigationBarTitle("")
                        .navigationBarHidden(true)
                        .navigationBarBackButtonHidden(true)
                    }  .onAppear(){
                        if UserDefaultsManager().loginType == LoginTypes.Email
                        {
                              self.email = UserDefaultsManager().userEmail ?? ""
                        }
                              
                    }
                }

        }
            
        func isValidEmail(Email:String) -> Bool {
            //print("validate emilId: \(testStr)")
            let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
            let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
            let result = emailTest.evaluate(with: Email)
            return result
        }

    }


struct ForgotPasswordView_Previews: PreviewProvider {
    static var previews: some View {
        ForgotPasswordView()
    }
}

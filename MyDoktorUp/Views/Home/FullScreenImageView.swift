//
//  FullScreenImageView.swift
//  MyDoktorUp
//
//  Created by Mac on 19/04/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI

struct FullScreenImageView: View {
    
    @Environment(\.presentationMode) var presentationMode
    @State var scale: CGFloat = 1.0
    var imageName: String = ""
    let pages: [PageViewData] = [
        PageViewData(imageNamed: "doctor_profile"),
        PageViewData(imageNamed:"doctor_profile" ),
        PageViewData(imageNamed: "doctor_profile")
    ]
    @State private var index: Int = 0

    var body: some View {
        
        ZStack {
           kCustomLightBlueColor
            VStack(alignment: .leading, spacing: 10) {
                Button(action: {
                    self.presentationMode.wrappedValue.dismiss()
                }) {
                    Text("Cancel")
                        .font(.system(.headline))
                        .foregroundColor(kPrimaryRegularColor)
                }.padding()
                
                Spacer()
            
                
                ZStack(alignment: .bottom) {
                           SwiperView(pages: self.pages, index: self.$index)
                           HStack(spacing: 8) {
                               ForEach(0..<self.pages.count) { index in
                                   CircleButton(isSelected: Binding<Bool>(get: { self.index == index }, set: { _ in })) {
                                       withAnimation {
                                           self.index = index
                                       }
                                   }
                               }
                           }
                           .padding(.bottom, 12)
                       }
                
                
                Spacer()
                   
            }
        }
    }
}

struct FullScreenImageView_Previews: PreviewProvider {
    static var previews: some View {
        FullScreenImageView()
    }
}

//
//  ReviewAndBookView.swift
//  MyDoktorUp
//
//  Created by Mac on 08/02/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI

struct ReviewAndBookView: View {
    var appointmentInfo = AppointmentInfo()
    var provider = Provider()
    @Environment(\.presentationMode) var presentationMode
    
    @ObservedObject var observedVisitReasons = VisitReasonsObserver()
    @ObservedObject var observedInsurance = InsuranceCarrierObserver()
    @ObservedObject var observedForm = ReviewBookFormObserver()
    @ObservedObject var observedInsuranceSelection = InsuranceSelectionObserver()
    @ObservedObject var GetUserData = UserInformationObserver()
    
    //    @ObservedObject var phoneNo = TextBindingManager()
    @State var fullName: String = ""
    @State var emailAddress: String = ""
    @State var address: String = ""
    @State var cityStateZip: String = ""
    //    @State var isNewPatient: Bool = false
    @State var noteForDoctor: String = ""
    @State var visitReason: String = "Sick Visit"
    
    let heightCommon: CGFloat = 40
    let leadingPaddingCommon : CGFloat = 15
    
    var arrGender = ["Male", "Female", "Others"]
    var arrPatient = ["Myself", "Spouse", "Parent", "Child", "Other"]
    
    
    @State private var selectedGender = 0
    @State private var selectedPatient = 0
    @State private var selectedInsurance = 0
    @State var isActiveBtn = false
    @State var showError = false
    @State var showErrorMessage: String = ""
    @Binding var isNavigationActive: Bool
    @State var isActiveInsurance = false
    @State var Phonenumber = ""
    
    @State var isDisappar = false
    
    var dateFormatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        return formatter
    }
    
    @State private var selectedVisitReasonNew = 0
    @State private var selectedVisitReason = 0
    
    @State var isNewPatient: Bool = false
    @State var isTeleHelth: Bool = false
    @State var isTeleHelthToggel: Bool = false

    @State private var birthDate = Date()
    @State var showingAlert = false
    @State var dict : [String: String] = [:]
    @State var isShowing = false
    @State var insuranceNames: [String] = []
    
    
    
    var body: some View {
        LoadingView(isShowing: .constant(isShowing)) {
            VStack {
                Group {
                    Form {
                        //PART-1 Header
                        VStack(alignment: .leading, spacing: 10) {
                            HStack {
                                Spacer()
                                Text("\(self.appointmentInfo.appointment_date?.convertDateFormat(inputDate: self.appointmentInfo.appointment_date ?? "", inputFormat: "MM/dd/yyyy", format: "EEEE MMM d, YYYY") ?? "")" + " - " + "\(self.appointmentInfo.appointment_time?.convertDateFormat(inputDate: self.appointmentInfo.appointment_time ?? "", inputFormat: "HH:mm", format: "hh:mma") ?? "")")
                                    .foregroundColor(kCustomOrangeColor)
                                    .fontWeight(.semibold)
                                    .font(.system(.headline))
                                    .multilineTextAlignment(.center)
                                Spacer()
                            }
                            .padding(.top, 15)
                            .padding(.bottom, 5)
                            HStack(spacing: 15) {
                                
                                ZStack{
                                    
                                    Image("doctor_profile")
                                        .resizable()
                                        .frame(width: 90, height: 95)
                                        .padding(.top, 10)
                                    if self.provider.virtual ?? false
                                    {
                                        Image("video_icon")
                                            .resizable()
                                            .frame(width: 18, height: 18)
                                            .padding(.leading, 67)
                                            .padding(.top, 72)
                                    }
                                    
                                    
                                }
                                
                                
                                VStack(alignment: .leading, spacing: 5, content: {   
                                    Text(Utils.getProviderFullname(self.provider))
                                        .font(.headline)
                                        .fontWeight(.bold)
                                        .foregroundColor(kPrimaryRegularColor)
                                        .lineLimit(nil)
                                    
                                    
                                    
                                    
                                    Text(self.provider.practice_phone?.formatePhoneNumber() ?? "")
                                        .font(.body)
                                        .fontWeight(.light)
                                        .foregroundColor(kPrimaryRegularColor)
                                        .lineLimit(nil)
                                 
                                    Text("\(self.provider.address1?.capitalized ?? ""), \(self.provider.city?.capitalized ?? "" ), \(self.provider.state?.uppercased() ?? ""), \(self.provider.zip ?? "")")
                                        .font(.footnote)
                                        .fontWeight(.regular)
                                        .foregroundColor(Color(red: 123/255, green: 122/255, blue: 122/255))
                                        .lineLimit(nil)
                                })
                            }
                        }
                        
                        Section(header: Text("Appointment Type").foregroundColor(kPrimaryRegularColor).fontWeight(.thin).font(.system(.title))) {
                            
                            Toggle(isOn: self.$isTeleHelth)
                            {
                                Text("Telehelth")
                                    .foregroundColor(kCustomGrayColor)
                            }.disabled(self.isTeleHelthToggel)
                        }
                        
                        Section(header: Text("Patient").foregroundColor(kPrimaryRegularColor).fontWeight(.thin).font(.system(.title))) {
                            
                            Picker(selection: self.$selectedPatient, label: Text("* Patient")) {
                                ForEach(0 ..< self.arrPatient.count) {
                                    Text(self.arrPatient[$0])
                                }
                            }
                            
                            TextField("* First Name, Last Name", text: self.$fullName)
                            //                            TextField("* Phone", text: self.$phoneNo.text).keyboardType(.numberPad)
                            
                            TextFieldWithAsInputFormate(placeholder: "* Phone", Border: false,fontsize: UIFont.systemFont(ofSize: 18), Alignment: .left, text: self.$Phonenumber).onAppear {
                                
                            }
                            //
                            
                            Picker(selection: self.$selectedGender, label: Text("* Gender")) {
                                ForEach(0 ..< self.arrGender.count) {
                                    Text(self.arrGender[$0])
                                }
                            }
                            
                            DatePicker(selection: self.$birthDate, in: ...Date(), displayedComponents: .date) {
                                HStack {
                                    Text("* Date of Birth")
                                    //                        Spacer()
                                    //                        Text("\(birthDate, formatter: dateFormatter)")
                                }
                                
                            }
                            
                            TextField("* Email", text: self.$emailAddress).keyboardType(.emailAddress)
                        }.padding(.vertical, 10)
                            .foregroundColor(kCustomGrayColor)
                        
                        Section(header: Text("Address").foregroundColor(kPrimaryRegularColor).fontWeight(.thin).font(.system(.title))) {
                            
                            TextField("Address 1, Address 2", text: self.$address)
                            TextField("City, State, Zip", text: self.$cityStateZip)
                            
                        }.padding(.vertical, 10)
                            .foregroundColor(kCustomGrayColor)
                        
                        Section(header: Text("Insurance").foregroundColor(kPrimaryRegularColor).fontWeight(.thin).font(.system(.title))) {
                            
                            HStack(alignment: .center, spacing: 10, content: {
                                NavigationLink(destination: InsuranceListView(insuranceNames: self.insuranceNames, insuranceDelegate: self), isActive: self.$isActiveInsurance) {
                                    Button("Insurance") {
                                        self.isActiveInsurance.toggle()
                                    }
                                    Spacer()
                                    Text(self.observedInsuranceSelection.insuranceName)
                                }
                                
                            })
                            //
                            Toggle(isOn: self.$isNewPatient) {
                                Text("I am a new patient")
                            }
                            
                            Picker(selection: self.$selectedVisitReason, label: Text("Visit Reason")) {
                                ForEach(0 ..< self.observedVisitReasons.arrVisitReasons.count) {
                                    if self.observedVisitReasons.arrVisitReasons.count != 0 {
                                        Text(self.observedVisitReasons.arrVisitReasons[$0].UIVisitReason ?? "")
                                    } else {
                                        Text("")
                                    }
                                }
                            }
                            
                            Text("\(self.appointmentInfo.appointment_date?.convertDateFormat(inputDate: self.appointmentInfo.appointment_date ?? "", inputFormat: "MM-dd-yyyy", format: "EEEE MMM d, YYYY") ?? "")" + " - " + "\(self.appointmentInfo.appointment_time?.convertDateFormat(inputDate: self.appointmentInfo.appointment_time ?? "", inputFormat: "HH:mm", format: "hh:mma") ?? "")")
                            
                        }.padding(.vertical, 10)
                            .foregroundColor(kCustomGrayColor)
                        
                        Section(header: Text("Note").foregroundColor(kPrimaryRegularColor).fontWeight(.thin).font(.system(.title))) {
                            TextField("Optional notes for doctor", text: self.$noteForDoctor)
                        }.padding(.vertical, 10)
                            .foregroundColor(kCustomGrayColor)
                    }
                    // Book button
                    NavigationLink(destination: AppointmentConfirmView(appointmentInfo: self.appointmentInfo, visitReason: self.visitReason, patientEmail: self.emailAddress,provider:self.provider, patientname: self.fullName,isNavigationActive: self.$isNavigationActive), isActive: self.$isActiveBtn) {
                        HStack {
                            Spacer()
                            Button(action: {
                                if !self.validateFields() {
                                    var strVisitReasonID = ""
                                    if self.observedVisitReasons.arrVisitReasons.count > self.selectedVisitReason {
                                        let visitReason = self.observedVisitReasons.arrVisitReasons[self.selectedVisitReason]
                                        strVisitReasonID = "\(visitReason.reasonid ?? 0)"
                                        self.visitReason = "\(visitReason.UIVisitReason ?? "")"
                                    }
                                    
                                    self.dict = [
                                        "appointment_id": self.appointmentInfo.appointment_id ?? "",
                                        "appointment_type_id": self.appointmentInfo.appointment_type_id ?? "",
                                        "appointment_virtual": String(self.isTeleHelth),
                                        "new_patient": String(self.isNewPatient),
                                        "simple_name": self.fullName,
                                        "address_line1": self.address,
                                        "address_line2": "",
                                        "city_state_zip": self.cityStateZip,
                                        "phone": self.Phonenumber.replacingOccurrences(of: "-", with: ""),
                                        "gender": self.arrGender[self.selectedGender],
                                        "dob": self.dateFormatter.string(from: self.birthDate),
                                        "patient_email": self.emailAddress,
                                        "booking_forself": self.arrPatient[self.selectedPatient],
                                        "insurance":  self.observedInsuranceSelection.insuranceName,
                                        "visit_reason": self.visitReason,
                                        "visit_reason_id": strVisitReasonID,
                                        "visit_note": self.noteForDoctor
                                    ]
                                    print("dict \(self.dict)")
                                    self.isShowing.toggle()
                                    self.bookDoctorAppointment(dict: self.dict)
                                }
                            }) {
                                Spacer()
                                Text("Book")
                                    .font(.system(.headline))
                                    .foregroundColor(Color.white)
                                    .padding(.vertical, 10)
                                    .frame(minWidth: 0, maxWidth: .infinity)
                                    .background(kPrimaryRegularColor)
                                    .cornerRadius(10)
                            }
                            Spacer()
                        }.padding()
                            .alert(isPresented: self.$showingAlert) {
                                Alert(title: Text("Error"), message: Text(self.showErrorMessage), dismissButton: .default(Text("OK")))
                                
                        }
                    }.isDetailLink(false)
                }.padding(.top, -35)
                    .navigationBarTitle("Review and Book",displayMode: .inline)
                    .navigationBarTitle(Text("Review and Book"), displayMode: .inline)
                    .navigationBarItems(leading: HStack {
                        Button(action: {
                            _ = DoctorProfileView(isNavigationBarHidden: .constant(true))
                            self.presentationMode.wrappedValue.dismiss()
                        }, label: {Image("ic_back")})
                        }, trailing: HStack {
                            Button(action: {
                                print("Book appointment")
                                if !self.validateFields() {
                                    var strVisitReasonID = ""
                                    if self.observedVisitReasons.arrVisitReasons.count > self.selectedVisitReason {
                                        let visitReason = self.observedVisitReasons.arrVisitReasons[self.selectedVisitReason]
                                        strVisitReasonID = "\(visitReason.reasonid ?? 0)"
                                        self.visitReason = "\(visitReason.UIVisitReason ?? "")"
                                    }
                                    
                                    self.dict = [
                                        "appointment_id": self.appointmentInfo.appointment_id ?? "",
                                        "appointment_type_id": self.appointmentInfo.appointment_type_id ?? "",
                                        "appointment_virtual": String(self.isTeleHelth),
                                        "new_patient": String(self.isNewPatient),
                                        "simple_name": self.fullName,
                                        "address_line1": self.address,
                                        "address_line2": "",
                                        "city_state_zip": self.cityStateZip,
                                        "phone": self.Phonenumber.replacingOccurrences(of: "-", with: ""),
                                        "gender": self.arrGender[self.selectedGender],
                                        "dob": self.dateFormatter.string(from: self.birthDate),
                                        "patient_email": self.emailAddress,
                                        "booking_forself": self.arrPatient[self.selectedPatient],
                                        "insurance": self.observedInsuranceSelection.insuranceName,
                                        "visit_reason": self.visitReason,
                                        "visit_reason_id": strVisitReasonID,
                                        "visit_note": self.noteForDoctor
                                    ]
                                    print("dict \(self.dict)")
                                    self.isShowing.toggle()
                                    self.bookDoctorAppointment(dict: self.dict)
                                }
                            }, label: {Text("Book")})
                    })
            }
        }
        .onDisappear{
            self.isDisappar = true
        }
        .onAppear {
            UITableView.appearance().backgroundColor = .clear
            UITableView.appearance().separatorStyle = .singleLine
            
            if self.provider.virtual ?? false
            {
                self.isTeleHelth = true
                self.isTeleHelthToggel = false
            }
            else{
                self.isTeleHelth = false
                self.isTeleHelthToggel = true
            }
            
            if !(UserDefaultsManager().isSign ?? false)
            {
                self.fullName = ""
                self.emailAddress = ""
                self.Phonenumber = ""
                self.address = ""
                self.cityStateZip = ""
                self.birthDate = Date()
                self.selectedGender = 0
            }
            else if self.selectedPatient == 0 || !self.isDisappar
            {
                self.fullName = UserDefaultsManager().userFullName ?? ""
                self.emailAddress = UserDefaultsManager().userEmail ?? ""
                self.Phonenumber = UserDefaultsManager().userPhoneNumber?.formatePhoneNumber() ?? ""
                self.address = UserDefaultsManager().useraddress1 ?? ""
                self.cityStateZip = ""
                if UserDefaultsManager().usercity?.count ?? 0 > 0
                {
                    self.cityStateZip = "\(UserDefaultsManager().usercity ?? ""),"
                }
                if UserDefaultsManager().userstate?.count ?? 0 > 0
                {
                    self.cityStateZip.append("\(UserDefaultsManager().userstate ?? ""),")
                }
                if UserDefaultsManager().userzip?.count ?? 0 > 0
                {
                    self.cityStateZip.append("\(UserDefaultsManager().userzip ?? "")")
                }
                self.birthDate = self.dateFormatter.date(from: UserDefaultsManager().userdob ?? "\(Date())") ?? Date()
                
                for i in 0 ..< self.arrGender.count
                {
                    if self.arrGender[i].lowercased() == UserDefaultsManager().user_gender
                    {
                        self.selectedGender = i
                        break
                    }
                }
                
            }
            if !self.isDisappar
            {
                
                
                self.GetUserData.getUser(strEmail: UserDefaultsManager().userEmail ?? "" ) { (status,message) in}
                self.emailAddress = UserDefaultsManager().userEmail ?? ""
                self.Phonenumber = UserDefaultsManager().userPhoneNumber?.formatePhoneNumber() ?? ""
                self.address = UserDefaultsManager().useraddress1 ?? ""
                
                self.cityStateZip = ""
                if UserDefaultsManager().usercity?.count ?? 0 > 0
                {
                    self.cityStateZip = "\(UserDefaultsManager().usercity ?? ""),"
                }
                if UserDefaultsManager().userstate?.count ?? 0 > 0
                {
                    self.cityStateZip.append("\(UserDefaultsManager().userstate ?? ""),")
                }
                if UserDefaultsManager().userzip?.count ?? 0 > 0
                {
                    self.cityStateZip.append("\(UserDefaultsManager().userzip ?? "")")
                }
                self.birthDate = self.dateFormatter.date(from: UserDefaultsManager().userdob ?? "\(Date())") ?? Date()
                
                
                for i in 0 ..< self.arrGender.count
                {
                    if self.arrGender[i].lowercased() == UserDefaultsManager().user_gender
                    {
                        self.selectedGender = i
                        break
                    }
                }
            }
            
            
            if self.observedInsurance.arrInsuranceList.count == 0 {
                self.isShowing = true
            }
            self.observedInsurance.getInsuranceList(searchText:"",isCommonAPI: false, complitionHandler: { (isSuccess) in
                if isSuccess {
                    self.isShowing = false
                    self.insuranceNames.removeAll()
                    for case let insurance as InsuranceCarrier in self.observedInsurance.arrInsuranceList {
                        self.insuranceNames.append(insurance.name ?? "")
                    }
                }
            })
            self.observedVisitReasons.getVisitResoons(isCommonAPI: false, isNewPatient: false)
            
        }
    }
    private func bookDoctorAppointment(dict: [String: String]) {
        self.observedForm.postAppointment(dictToPost: dict, complitionHandler: { (isSucces, error) in
            self.isShowing = true
            if isSucces {
                self.isShowing = false
                self.isActiveBtn = true
            } else {
                self.isShowing = false
                self.showErrorMessage = error?.localizedDescription ?? ""
                self.showingAlert.toggle()
            }
        })
    }
    
    func validateFields() -> Bool {
        if self.fullName == "" {
            self.showErrorMessage = "Please enter first name, last name"
            self.showingAlert.toggle()
            return true
        } else if self.Phonenumber == "" {
            self.showErrorMessage = "Please enter your phone number"
            self.showingAlert.toggle()
            return true
        } else if self.Phonenumber.count < 10 {
            self.showErrorMessage = "Please enter valid phone number"
            self.showingAlert.toggle()
            return true
        } else if self.emailAddress == "" {
            self.showErrorMessage = "Please enter your email address"
            self.showingAlert.toggle()
            return true
        } else if !self.emailAddress.isValid() {
            self.showErrorMessage = "Please enter valid email address"
            self.showingAlert.toggle()
            return true
        } else {
            return false
        }
    }
}

struct ReviewAndBookView_Previews: PreviewProvider {
    static var previews: some View {
        ReviewAndBookView(isNavigationActive: .constant(false))
        
    }
}

struct FullWidthDivider : View {
    var body: some View {
        HStack {
            Spacer()
                .frame(height:0.8)
                .background(kCustomLightGrayDividerColor)
        }.padding(.horizontal, -20)
    }
}

struct DividerNoLeading : View {
    var body: some View {
        HStack {
            Spacer()
                .frame(height:0.8)
                .background(kCustomLightGrayDividerColor)
        }.padding(.trailing, -20)
    }
}


extension ReviewAndBookView: InsuranceDelegate {
    func didSelectInsurance(index: Int) {
        print("Insurance ID = ",self.observedInsurance.arrInsuranceList[index].id!)
        self.observedInsuranceSelection.insuranceName = self.observedInsurance.arrInsuranceList[index].name!
    }
}

//
//  MapLocationView.swift
//  MyDoktorUp
//
//  Created by Mac on 08/02/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI
import MapKit

struct MapLocationView: UIViewRepresentable {
    typealias UIViewType = MKMapView
    
    var provider = Provider()
   
    func makeCoordinator() -> MapLocationView.MapViewCoordinator {
        MapViewCoordinator(self)
    }
    
    func makeUIView(context: UIViewRepresentableContext<MapLocationView>) -> MapLocationView.UIViewType {
        
        let mapView = MKMapView(frame: .zero)
        mapView.delegate = context.coordinator

        mapView.isZoomEnabled = false
        mapView.isScrollEnabled = false
        return mapView
    }
    
    func updateUIView(_ uiView: MapLocationView.UIViewType, context: UIViewRepresentableContext<MapLocationView>) {
        
        

        let lat = Double("\(provider.latitude ?? "")") ?? 0
        let lng = Double("\(provider.longitude ?? "")") ?? 0
        
        
        
        let coordinateCenter =  CLLocationCoordinate2D(latitude: lat, longitude: lng)
        let span = MKCoordinateSpan(latitudeDelta: 2, longitudeDelta: 2)
        let region = MKCoordinateRegion(center: coordinateCenter, span: span)
        let annotation = MKPointAnnotation()
        annotation.title = provider.address1
        annotation.coordinate = coordinateCenter
        uiView.addAnnotation(annotation)
        uiView.setRegion(region, animated: true)
    }
    class MapViewCoordinator: NSObject, MKMapViewDelegate {
          var mapViewController: MapLocationView
            
          init(_ control: MapLocationView) {
              self.mapViewController = control
          }
            
          func mapView(_ mapView: MKMapView, viewFor
               annotation: MKAnnotation) -> MKAnnotationView?{
             //Custom View for Annotation
              let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "customView")
            
              annotationView.canShowCallout = true
              //Your custom image icon
              annotationView.image = UIImage(named: "MapPinmarker")
              return annotationView
           }
    }
}

struct MapLocationView_Previews: PreviewProvider {
    static var previews: some View {
        MapLocationView()
    }
}

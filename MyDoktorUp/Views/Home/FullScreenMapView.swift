//
//  FullScreenMapView.swift
//  MyDoktorUp
//
//  Created by Mac on 19/04/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI
import MapKit

struct FullScreenMapView: View {
    
    @Environment(\.presentationMode) var presentationMode
    var provider = Provider()

    var body: some View {
        
        ZStack {
            
            VStack(alignment: .leading, spacing: 10) {
              
                
                HStack {
                    Button(action: {
                                      self.presentationMode.wrappedValue.dismiss()
                                  }) {
                                      Text("Cancel")
                                          .font(.system(.headline))
                                          .foregroundColor(kPrimaryRegularColor)
                                  }.padding()
                    
                    Spacer()
                    Button(action: {
                        self.openAppleMaps()
                    }) {
                        Text("Get Direction")
                            .font(.system(.headline))
                            .foregroundColor(kPrimaryRegularColor)
                    }.padding(.trailing,10)
                    
                }
                
                
                
                MapLocationView(provider: provider)
                
                
            }
        }
    }
    
    func openAppleMaps() {
        let urlString = "http://maps.apple.com/maps?saddr=\(provider.latitude ?? "0"),\(provider.longitude ?? "0")"
        
        if let url = URL(string: urlString) {
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
    }
}

struct FullScreenMapView_Previews: PreviewProvider {
    static var previews: some View {
        FullScreenMapView()
    }
}

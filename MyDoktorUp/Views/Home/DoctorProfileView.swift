//
//  DoctorProfileView.swift
//  MyDoktorUp
//
//  Created by SHREE SAHANA on 4/5/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI
struct DoctorProfileView: View {
    @EnvironmentObject var appState: AppState
    @Binding var isNavigationBarHidden: Bool
    @ObservedObject var providerDetails = ProviderDetailObserver()
    @ObservedObject var providerSlots = ProviderSlotDetailObserver()
    @State private var isShowing = true
    @State var isNavigationActive : Bool = false
    @State var isActive : Bool = false
    @Environment(\.presentationMode) var presentationMode
    @State var selectedProvider :Provider?
    @State var isMapTapped = false
    @State var AppointmentSlots = [ProviderSlots]()
  
    
    var body: some View {
        LoadingView(isShowing: .constant(isShowing)) {
            VStack {
                NavigationView {
                    VStack(alignment: .leading, spacing: 10){
                        ScrollView {
                            VStack(alignment: .leading, spacing: 10){
                                // Doctor Profile Details
                                DoctorProfileDetails(provider: self.selectedProvider ?? Provider())
                                
                                Divider().padding(.leading, 20).padding(.trailing, 20)
                                
                                // Doctor Available Appointment Slots
                                DoctorAvailableAppointmentSlots(providerSlots: self.AppointmentSlots, provider: self.selectedProvider ?? Provider())
                                
                                // MapView
                                Group {
                                    MapLocationView(provider: self.providerDetails.arrProviders.first ?? Provider())
                                        .onTapGesture {
                                            self.isMapTapped.toggle()
                                    }
                                    .sheet(isPresented: self.$isMapTapped, onDismiss: {
                                        self.appState.reloadDashboard(selectedTab: 0, selectedProvider: self.selectedProvider ?? Provider())
                                    }) {
                                        FullScreenMapView(provider: self.selectedProvider ?? Provider())
                                    }
                                }.frame(height: 150)
                                    .padding(.horizontal, -20)
                                
                                Divider().padding(.top, 15).padding(.leading, 20).padding(.trailing, 20)
                                
                                // Doctor Photos
                                DoctorPhotosView()
                                
                                //Review and Location
                                ReviewsAndLocationView(provider: self.selectedProvider ?? Provider())
                                Spacer()
                            }
                        }
                    }
                    .background(Color(red: 248/256, green: 251/256, blue: 250/256))
                    .background(Color(red: 248/255, green: 251/255, blue: 250/255))
                    .navigationBarTitle("My Doctor",displayMode: .inline)
                    .navigationBarHidden(false)
                    
                }
                .navigationBarHidden(self.isNavigationBarHidden)
                .onDisappear() {
                    self.isShowing = true
                }.navigationViewStyle(StackNavigationViewStyle()) // This line is supporting iPad, we should not comment out this line
            }
        }.onAppear() {
           
            self.isNavigationBarHidden = true
            self.providerDetails.getProviderDetail(provideId: UserDefaultsManager().providerID ?? "") { (isSuccess) in
                self.isShowing = true
                if isSuccess {
                    
                    if self.selectedProvider != nil {
                        self.selectedProvider = self.providerDetails.arrProviders.first
                    }
                    dLog("Success check in first if")
                    self.isShowing = false
                } else {
                    dLog("Failure")
                    self.isShowing = false
                }
            }
            self.providerSlots.getProviderAppointmentSlots(complitionHandler: { (isSuccess) in
                self.isShowing = true
                if isSuccess {
                    self.isShowing = false
                    self.AppointmentSlots = self.providerSlots.arrAppointmentSlots
                    
                    
                } else {
                    dLog("Failure")
                    self.isShowing = false
                }
            })
            
        }
        
    }
    
}

struct DoctorProfileView_Previews: PreviewProvider {
    static var previews: some View {
        DoctorProfileView(isNavigationBarHidden: .constant(false))
    }
}


struct DoctorProfileDetails: View {
    var provider = Provider()
    var body: some View {
        HStack(spacing: 15) {
            ZStack{
                Image("doctor_profile")
                    .resizable()
                    .frame(width: 90, height: 95)
                    .padding(.top, 10)
                if self.provider.virtual ?? false
                {
                    Image("video_icon")
                        .resizable()
                        .frame(width: 18, height: 18)
                        .padding(.leading, 67)
                        .padding(.top, 72)
                }
            }
            VStack(alignment: .leading, spacing: 5, content: {
                Text(Utils.getProviderFullname(self.provider))
                    .font(.headline)
                    .fontWeight(.bold)
                    .foregroundColor(kPrimaryRegularColor)
                    .lineLimit(nil)
                Text(self.provider.practice_phone?.formatePhoneNumber() ?? "")
                    .font(.body)
                    .fontWeight(.light)
                    .foregroundColor(kPrimaryRegularColor)
                    .lineLimit(nil)
                Text("\(self.provider.address1?.capitalized ?? ""), \(self.provider.city?.capitalized ?? "" ), \(self.provider.state?.uppercased() ?? ""), \(self.provider.zip ?? "")")
                    .font(.footnote)
                    .fontWeight(.regular)
                    .foregroundColor(Color(red: 123/255, green: 122/255, blue: 122/255))
                    .lineLimit(nil)
            })
        }.padding()
    }
}
struct DoctorAvailableAppointmentSlots: View {
    
    var providerSlots = [ProviderSlots]()
    var provider = Provider()
     @State   var MorningSlots = [ProviderSlots]()
     @State  var AfterNoonSlots = [ProviderSlots]()
     @State  var EveningSlots = [ProviderSlots]()

    
    var FilterSlote = ["Morning","Afternoon","Evening"]
    
    @State  var selectedTag = 0
    
    @State var MorebtnTap = false
    var body: some View {
        
        VStack {
            if self.providerSlots.count == 0 {
                HStack {
                    Text("There are no appointments available right now. Please try again later.")
                        .font(.system(Font.TextStyle.subheadline))
                        .fontWeight(.bold)
                        .foregroundColor(kPrimaryRegularColor)
                    Spacer()
                }.padding(.leading, 20)

            } else {
                HStack {
                    Text("Book now:")
                        .font(.system(Font.TextStyle.subheadline))
                        .fontWeight(.bold)
                        .foregroundColor(kPrimaryRegularColor)
                    Spacer()
                }.padding(.leading, 20)
                
                
                Picker(selection: $selectedTag, label: Text("")) {
                    ForEach(0 ..< FilterSlote.count) {
                        Text(self.FilterSlote[$0])
                        
                    }
                }
                .padding()
                .pickerStyle(SegmentedPickerStyle())
             
                
                ScrollView(.horizontal, showsIndicators: false) {
                    HStack {
                         ForEach(0..<self.providerSlots.count) { sIndex in
                            VStack() {
                                
                                if Calendar.current.isDateInToday(self.providerSlots[sIndex].date?.checkToday(inputDate: self.providerSlots[sIndex].date ?? "", inputFormat: "MM-dd-yyyy") ?? Date()) {
                                    Text("Today")
                                        .font(.headline)
                                        .fontWeight(.bold)
                                        .foregroundColor(Color(hex: CustomFontColor.appointmentDate))
                                        .lineLimit(nil)
                                        .padding(.bottom, 8)
                                        .padding(.top, 8)
                                }
                                else if Calendar.current.isDateInTomorrow(self.providerSlots[sIndex].date?.checkToday(inputDate: self.providerSlots[sIndex].date ?? "", inputFormat: "MM-dd-yyyy") ?? Date())
                                {
                                    Text("Tomorrow")
                                        .font(.headline)
                                        .fontWeight(.bold)
                                        .foregroundColor(Color(hex: CustomFontColor.appointmentDate))
                                        .lineLimit(nil)
                                        .padding(.bottom, 8)
                                        .padding(.top, 8)
                                    
                                }else {
                                    Text("\(self.providerSlots[sIndex].date?.convertDateFormat(inputDate: self.providerSlots[sIndex].date ?? "", inputFormat: "MM-dd-yyyy", format: "EEE, MMM dd") ?? "")")
                                        .font(.headline)
                                        .fontWeight(.medium)
                                        .foregroundColor(Color(hex: CustomFontColor.appointmentDate))
                                        .lineLimit(nil)
                                        .padding(.bottom, 8)
                                        .padding(.top, 8)
                                    
                                }
                                if self.providerSlots[sIndex].slots.count > 3 {
                                    if !self.MorebtnTap
                                    {
                                        TimeSloteWithMoreButton(providerSlots: self.providerSlots[sIndex], provider: self.provider, sIndex: sIndex, isMoreTapped: self.$MorebtnTap)
                                    }
                                    
                                } else {
                                    FixdTimeSlote(providerSlots: self.providerSlots[sIndex], provider: self.provider, sIndex: sIndex)
                                }
                                
                                if self.MorebtnTap
                                {
                                    FixdTimeSlote(providerSlots: self.providerSlots[sIndex], provider: self.provider, sIndex: sIndex)
                                    
                                }
                            Spacer()
                            }
                        }
                        
                        
                        
                    }
                }
                .onAppear {
                            
                            for ProviderSlot in self.providerSlots
                            {
                                var morningSlot = [AppointmentInfo]()
                                var AfterNoonSlot = [AppointmentInfo]()
                                var EveningSlot = [AppointmentInfo]()
                                
                                for slots in ProviderSlot.slots
                                {
                                    let time = slots.appointment_time?.split(separator: ":")
                                    
                                    let HourInt = Int("\(time?[0] ?? "0")")
                                    
                                    
                                    if HourInt! >= 9 &&  HourInt! < 12
                                    {
                                        morningSlot.append(slots)
                                    }
                                    
                                    if HourInt! >= 12 &&  HourInt! < 14
                                    {
                                       AfterNoonSlot.append(slots)

                                    }
                                    
                                    if HourInt! >= 14 &&  HourInt! < 19
                                    {
                                        EveningSlot.append(slots)
                                    }
                                    
                                }
                                
                                var providerSlot = ProviderSlot
                               
                                providerSlot.slots = morningSlot
                                self.MorningSlots.append(providerSlot)
                                
                                providerSlot.slots = AfterNoonSlot
                                self.AfterNoonSlots.append(providerSlot)
                                
                                providerSlot.slots = EveningSlot
                                self.EveningSlots.append(providerSlot)
                                
                                
                                
                            }
                            
                    print("AfterNoonSlots \(self.AfterNoonSlots)")
                        }
                .padding(.leading, 20)
                .padding(.trailing, 20)
            }
            
        }
    
        
    }
    
    
    
    func FilterTimeSlot(SelectedTime:Int)  {
        print("SelectedTime \(self.FilterSlote[SelectedTime])")
    }
    
}


struct DoctorPhotosView: View {
    @EnvironmentObject var appState: AppState
    var provider = Provider()
    
    @State var showFullImage = false
    
    var body: some View {
        VStack(alignment: .leading, spacing: 10) {
            Text("Photos")
                .font(.system(Font.TextStyle.title))
                .fontWeight(.thin)
                .foregroundColor(kPrimaryRegularColor)
            
            ScrollView(.horizontal, showsIndicators: true) {
                VStack(alignment: .leading, spacing: 0) {
                    
                    HStack {
                        ForEach(0 ..< 5) { number in
                            Image("doctor_profile")
                                .resizable()
                                .frame(width: 90, height: 95)
                                .padding(.top, 10)
                                .padding(.horizontal,5)
                                .onTapGesture {
                                    self.showFullImage.toggle()
                            }
                            .sheet(isPresented: self.$showFullImage, onDismiss: {
                                self.appState.reloadDashboard(selectedTab: 0, selectedProvider: self.provider)
                            }) {
                                FullScreenImageView(imageName: "doctor_profile")
                            }
                        }
                    }
                }
            }
        }.padding(.leading, 20)
            .padding(.trailing, 20)
        
        
    }
}

struct ReviewsAndLocationView: View {
    var provider = Provider()
    var body: some View {
        VStack(alignment: .leading, spacing: 10) {
            Group {
                Group {
                    Text("Insurances")
                        .font(.system(Font.TextStyle.title))
                        .fontWeight(.thin)
                        .foregroundColor(kPrimaryRegularColor)
                    
                    Text(provider.insurance_carriers ?? "Not Available")
                        .font(.system(size: 13))
                        .fontWeight(.medium)
                        .foregroundColor(kCustomTextGrayColor)
                    
                    Text("Board Certifications")
                        .font(.system(Font.TextStyle.title))
                        .fontWeight(.thin)
                        .foregroundColor(kPrimaryRegularColor)
                        .padding(.top, 10)
                    
                    Text("American Board of Family Medicine")
                        .font(.system(size: 13))
                        .fontWeight(.medium)
                        .foregroundColor(kCustomTextGrayColor)
                    
                    Text("Specialties")
                        .font(.system(Font.TextStyle.title))
                        .fontWeight(.thin)
                        .foregroundColor(kPrimaryRegularColor)
                        .padding(.top, 10)
                    
                    Text("Family Physician")
                        .font(.system(size: 13))
                        .fontWeight(.medium)
                        .foregroundColor(kCustomTextGrayColor)
                    
                    Text("Education")
                        .font(.system(Font.TextStyle.title))
                        .fontWeight(.thin)
                        .foregroundColor(kPrimaryRegularColor)
                        .padding(.top, 10)
                    
                    Text("Medical School - Name School of Medicine, Doctor of Medicine")
                        .font(.system(size: 13))
                        .fontWeight(.medium)
                        .foregroundColor(kCustomTextGrayColor)
                        .lineLimit(nil)
                }
                Group {
                    
                    Text("Professional Statement")
                        .font(.system(Font.TextStyle.title))
                        .fontWeight(.thin)
                        .foregroundColor(kPrimaryRegularColor)
                        .padding(.top, 10)
                    
                    Text("\(provider.professional_statement ?? "Not Available")")//(providerObj.professional_statement!)
                        .font(.system(size: 13))
                        .fontWeight(.medium)
                        .foregroundColor(kCustomTextGrayColor)
                    
                    Text("Languages")
                        .font(.system(Font.TextStyle.title))
                        .fontWeight(.thin)
                        .foregroundColor(kPrimaryRegularColor)
                        .padding(.top, 10)
                    
                    Text("English, Spanish, Mandarin, Hindi")
                        .font(.system(size: 13))
                        .fontWeight(.medium)
                        .foregroundColor(kCustomTextGrayColor)
                    
                    Text("Gender")
                        .font(.system(Font.TextStyle.title))
                        .fontWeight(.thin)
                        .foregroundColor(kPrimaryRegularColor)
                        .padding(.top, 10)
                    
                    Text("Male")
                        .font(.system(size: 13))
                        .fontWeight(.medium)
                        .foregroundColor(kCustomTextGrayColor)
                    
                    Text("Practice Name")
                        .font(.system(Font.TextStyle.title))
                        .fontWeight(.thin)
                        .foregroundColor(kPrimaryRegularColor)
                        .padding(.top, 10)
                    
                    Text("Practice Name")
                        .font(.system(size: 13))
                        .fontWeight(.medium)
                        .foregroundColor(kCustomTextGrayColor)
                    
                }
                
                Group {
                    Text("Locations")
                        .font(.system(Font.TextStyle.title))
                        .fontWeight(.thin)
                        .foregroundColor(kPrimaryRegularColor)
                        .padding(.top, 10)
                    
                    Text("\(provider.address1 ?? ""), \(provider.city ?? "" ), \(provider.state ?? ""), \(self.provider.zip ?? "")".capitalized)
                        .font(.system(size: 13))
                        .fontWeight(.medium)
                        .foregroundColor(kCustomTextGrayColor)
                    
                    
                    Text("NPI Number")
                        .font(.system(Font.TextStyle.title))
                        .fontWeight(.thin)
                        .foregroundColor(kPrimaryRegularColor)
                        .padding(.top, 10)
                    
                    Text("\(provider.npi?.formatePhoneNumber() ?? "")")
                        .font(.system(size: 13))
                        .foregroundColor(kCustomTextGrayColor)
                    
                }
            }
        }.padding(.leading, 20)
            .padding(.trailing, 20)
    }
}

struct FixdTimeSlote: View {
    
    
    var providerSlots = ProviderSlots()
    var provider = Provider()
    var sIndex:Int
    var body: some View {
        
        VStack(alignment: .leading, spacing: 10){
            ForEach(0..<self.providerSlots.slots.count) { index in
                if index == 0 && self.sIndex == 0 {
                    TimeView(appointmentInfo: self.providerSlots.slots[index], provider: self.provider, isSetTimeSlot: true)
                } else {
                    TimeView(appointmentInfo: self.providerSlots.slots[index], provider: self.provider, isSetTimeSlot: false)
                }
            }
        }.padding(.top, 10).padding(.bottom, 5)
        
    }
}


struct TimeSloteWithMoreButton: View {
    var providerSlots = ProviderSlots()
    var provider = Provider()
    var sIndex:Int
    @Binding var isMoreTapped:Bool
    
    var body: some View {
        ZStack{
            
            
            VStack(alignment: .leading, spacing: 10) {
                ForEach(0..<3) { index in
                    if index == 0 && self.sIndex == 0 {
                        TimeView(appointmentInfo: self.providerSlots.slots[index], provider: self.provider, isSetTimeSlot: true)
                    } else {
                        TimeView(appointmentInfo: self.providerSlots.slots[index], provider: self.provider, isSetTimeSlot: false)
                    }
                }
                
                Button(action: {
                    self.isMoreTapped = true
                    
                }) {
                    Text("More")
                        .font(.system(size: 16, weight: Font.Weight.medium, design: .default))
                        .foregroundColor(Color(hex: CustomFontColor.appointmentTime))
                        .lineLimit(nil)
                        .frame(minWidth: 120, maxWidth: .infinity, minHeight: 45, maxHeight: 45, alignment: .center)
                        .overlay(
                            RoundedRectangle(cornerRadius: 8)
                                .strokeBorder(style: StrokeStyle(lineWidth: 1))
                                .foregroundColor(kCustomGrayColor)
                                .cornerRadius(8)
                    )
                    
                }
                
            }.padding(.bottom, 10)
            
        }
        
        
    }
}

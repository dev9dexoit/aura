//
//  AppointmentConfirmView.swift
//  MyDoktorUp
//
//  Created by Mac on 08/02/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI
import UIKit
import MessageUI

enum AppointmentAddShareMailStatusSuccess {
    case Calender,Sms,Email,CalenderFail,AlreadyExist
}

enum AppointmentShowsheet {
    case Sms,Email
}


struct AppointmentConfirmView: View {
    @State private var activeAlertSuccess: AppointmentAddShareMailStatusSuccess = .Calender
    
    @State var showAlertSuccess = false
    @State var result: Result<MFMailComposeResult, Error>? = nil
    @State var isShowingMailView = false
    @State private var isSharePresented: Bool = false
    @EnvironmentObject var appState: AppState
    
    
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject var observedRefObj = ReviewBookFormObserver()
    var appointmentInfo = AppointmentInfo()
    var visitReason: String
    var patientEmail: String
    var provider = Provider()
    var patientname:String
    @State var isSyncCalendar = false
    
    
    
    @Binding var isNavigationActive: Bool
    
    var body: some View {
        ZStack {
            ScrollView {
                VStack(alignment: .leading, spacing: 20) {
                    Group {
                        HStack {
                            Spacer()
                            Text("\(appointmentInfo.appointment_date?.convertDateFormat(inputDate: appointmentInfo.appointment_date ?? "", inputFormat: "MM-dd-yyyy", format: "EEEE MMM d, YYYY") ?? "")" + " - " + "\(appointmentInfo.appointment_time?.convertDateFormat(inputDate: appointmentInfo.appointment_time ?? "", inputFormat: "HH:mm", format: "hh:mma") ?? "")")
                                .foregroundColor(kCustomOrangeColor)
                                .font(.system(size: 18, weight: Font.Weight.medium, design: Font.Design.default))
                                .multilineTextAlignment(.center)
                            Spacer()
                        }.padding(.top, 10)
                            .padding(.bottom, 15)
                        
                        HStack {
                            Spacer()
                            
                            Text("Visit reason: \(visitReason)")
                                .font(Font.system(size: 21))
                                .fontWeight(.regular)
                                .foregroundColor(kCustomTextBlueColor)
                                .multilineTextAlignment(.center)
                            //                            Text(visitReason)
                            //                                .foregroundColor(kCustomTextBlueColor)
                            //                                .font(.system(size: 20, weight: Font.Weight.regular, design: Font.Design.default))
                            //                                .multilineTextAlignment(.center)
                            Spacer()
                        }.padding(.horizontal, 20)
                        
                        HStack {
                            Spacer()
                            Text("An confirmation email has been sent to \(patientEmail).")
                                .foregroundColor(kCustomTextBlueColor)
                                .font(.system(size: 21, weight: Font.Weight.regular, design: Font.Design.default))
                                .multilineTextAlignment(.center)
                            Spacer()
                        }.padding(.horizontal, 20)
                        
                        HStack {
                            Spacer()
                            Text("If you need to cancel for any reason Goto the appointments tab.")
                                .foregroundColor(kCustomTextBlueColor)
                                .font(.system(size: 20, weight: Font.Weight.regular, design: Font.Design.default))
                                .multilineTextAlignment(.center)
                            Spacer()
                        }.padding(.horizontal, 20)
                        
                        Spacer()
                        
                        VStack(alignment: .center, spacing: 10) {
                            if getHideAccordingHour(date: appointmentInfo.appointment_date ?? "\(Date())", formate: "MM-dd-yyyy")
                            {
                                Button(action: {
                                    self.appState.reloadDashboard(selectedTab:1, selectedProvider: self.provider)
                                }) {
                                    HStack {
                                        Text("Digital Check-in")
                                            .foregroundColor(.white)
                                            .padding(.vertical, 12)
                                        
                                    }
                                    .frame(minWidth: 0, maxWidth: .infinity)
                                    .background(kPrimaryRegularColor)
                                    .cornerRadius(10)
                                }.padding(.top, 10)
                            }
                            
                            
                            Button(action: {
                                let result = Utils.AddPointmentInCalender(self.appointmentInfo, provider: self.provider)
                                if result == "Success"
                                {
                                    self.activeAlertSuccess = .Calender
                                    self.showAlertSuccess = true
                                }
                                else
                                {
                                    self.activeAlertSuccess = .CalenderFail
                                    self.showAlertSuccess = true
                                    
                                }
                                
                            }) {
                                HStack {
                                    Image("Calender")
                                        .resizable()
                                        .foregroundColor(kCustomGrayColor)
                                        .frame(width: 20, height: 20)
                                        .padding(.leading, 80)
                                        .padding(.trailing, 20)
                                    
                                    Text("Add to calendar")
                                        .font(.system(size: 16, weight: Font.Weight.medium, design: .default))
                                        .padding(.leading, 10)
                                        .foregroundColor(kCustomGrayColor)
                                    Spacer()
                                }
                                .frame(minWidth: 0, idealWidth: 100, maxWidth: .infinity, minHeight:45, idealHeight: 40, maxHeight: 40)
                                
                                
                            } .overlay(
                                RoundedRectangle(cornerRadius: 10)
                                    .strokeBorder(style: StrokeStyle(lineWidth: 1))
                                    .foregroundColor(kCustomGrayColor)
                                    .cornerRadius(10)
                            )
                                
                                .alert(isPresented: self.$showAlertSuccess) {
                                    
                                    switch self.activeAlertSuccess {
                                    case .Calender:
                                        
                                        return Alert(title: Text("Appointment Added in Calender Successfully"))
                                    case .Sms:
                                        
                                        return Alert(title: Text("SMS Sent Successfully"))
                                        
                                    case .Email:
                                        return Alert(title: Text("Email Sent Successfully"))
                                    case .CalenderFail:
                                        return Alert(title: Text("Sometime went wrong, please try again"))
                                    case .AlreadyExist:
                                        return Alert(title: Text("Event Already Exist."))
                                    }
                            }
                            
                            
                            Button(action: {
                                
                                self.isSharePresented = true
                            }) {
                                
                                HStack {
                                    
                                    Image("Sms_Icon")
                                        .resizable()
                                        .foregroundColor(kCustomGrayColor)
                                        .frame(width: 20, height: 20)
                                        .padding(.leading, 80)
                                        .padding(.trailing, 10)
                                    Text("Send text directions")
                                        .font(.system(size: 16, weight: Font.Weight.medium, design: .default))
                                        .foregroundColor(kCustomGrayColor)
                                    Spacer()
                                }
                                .frame(minWidth: 0, idealWidth: 100, maxWidth: .infinity, minHeight: 45, idealHeight: 40, maxHeight: 40)
                            }
                            .overlay(
                                RoundedRectangle(cornerRadius: 10)
                                    .strokeBorder(style: StrokeStyle(lineWidth: 1))
                                    .foregroundColor(kCustomGrayColor)
                                    .cornerRadius(10)
                            )
                                .sheet(isPresented: $isSharePresented) {
                                    
                                    ActivityViewController(activityItems: ["Appointment Confirmed!",
                                                                           "For: \(self.patientname)",
                                        "On: \(self.appointmentInfo.appointment_date?.convertDateFormat(inputDate: self.appointmentInfo.appointment_date ?? "", inputFormat: "MM-dd-yyyy", format: "MMM d, YYYY") ?? "")" + " - " + "\(self.appointmentInfo.appointment_time?.convertDateFormat(inputDate: self.appointmentInfo.appointment_time ?? "", inputFormat: "HH:mm", format: "hh:mma") ?? "")",
                                        "With: \(self.provider.last_name ?? "") \(self.provider.last_name ?? "")",
                                        "Address: \(self.provider.address1 ?? ""),\(self.provider.city ?? ""),\(self.provider.state ?? ""),\(self.provider.zip ?? "")"])
                                    
                            }
                            
                            Button(action: {
                                self.isShowingMailView = true
                                
                            }) {
                                HStack {
                                    Image("Email_Icon")
                                        .resizable()
                                        .foregroundColor(kCustomGrayColor)
                                        .frame(width: 20, height: 20)
                                        .padding(.leading, 80)
                                        .padding(.trailing, 5)
                                    Text("Send to another email")
                                        .font(.system(size: 16, weight: Font.Weight.medium, design: .default))
                                        .foregroundColor(kCustomGrayColor)
                                    Spacer()
                                }
                                .frame(minWidth: 0, idealWidth: 100, maxWidth: .infinity, minHeight: 46, idealHeight: 40, maxHeight: 40)
                                
                            }.overlay(
                                RoundedRectangle(cornerRadius: 10)
                                    .strokeBorder(style: StrokeStyle(lineWidth: 1))
                                    .foregroundColor(kCustomGrayColor)
                                    .cornerRadius(10)
                            )
                                .disabled(!MFMailComposeViewController.canSendMail())
                                .sheet(isPresented: $isShowingMailView) {
                                    MailView(result: self.$result)
                            }
                        }.padding(.horizontal, 15)
                    }
                }
            }.background(kCustomLightBlueColor)
        }
            
        .background(kPrimaryRegularColor)
        .navigationBarTitle(Text("Appointment Booked"), displayMode: .inline)
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(trailing:
            HStack {
                Button(action: {
                    self.appState.reloadDashboard(selectedTab:2, selectedProvider: self.provider)
                    
                }) {
                    Text("Done")
                }.foregroundColor(.white)
        })
            .onAppear(perform: {
                let StartDateAndTime = "\(self.appointmentInfo.appointment_date ?? "")-\(self.appointmentInfo.appointment_time ?? "")"

                
                let dateformator = DateFormatter()
                    dateformator.dateFormat = "MM-dd-yyyy-HH:mm"
                  
                  let BookingDate  = dateformator.date(from: "\(StartDateAndTime)")

                let Title =  "Appointment with Dr. \(self.provider.first_name ?? "") \(self.provider.last_name ?? "") (MD - \(self.provider.taxonomy_name ?? "")"
                
                
                let OneDayAddDate = Calendar.current.date(byAdding: .day, value: 1, to: BookingDate ?? Date())
                
                let OneDayAddcomponents = OneDayAddDate?.get(.day, .month, .year,.hour,.minute)
                
                 var notification = Notification(id: self.provider.id ?? "", title: "\(Title)", datetime: OneDayAddcomponents ?? DateComponents(),subTitle: "Please save your time, do Digital Check-in before reaching to Dr's office")
                notifications.append(notification)

                let MinuteAddDate = Calendar.current.date(byAdding: .hour, value: 1, to: BookingDate ?? Date())
                
                let MinuteAddDatecomponents = MinuteAddDate?.get(.day, .month, .year,.hour,.minute)
                
                  notification = Notification(id: self.provider.id ?? "", title: "\(Title)", datetime: MinuteAddDatecomponents ?? DateComponents(),subTitle: "Please save your time, do Digital Check-in before reaching to Dr's office")
                
                notifications.append(notification)

               
                manager.schedule()
                self.isSyncCalendar = UserDefaults.standard.value(forKey: UserDefault.isCalendarSync) as? Bool ?? true
               
                if self.isSyncCalendar == true && UserDefaultsManager().isSign ?? false
                {
                    let result = Utils.AddPointmentInCalender(self.appointmentInfo, provider: self.provider)
                    if result == "Success"
                    {
                        self.activeAlertSuccess = .Calender
                        self.showAlertSuccess = true
                    }
                    else if result == "AlreadyExists"
                    {
                        self.activeAlertSuccess = .AlreadyExist
                        self.showAlertSuccess = true
                    }
                    else
                    {
                        self.activeAlertSuccess = .CalenderFail
                        self.showAlertSuccess = true
                        
                    }
                }
            })
    }
}

struct AppointmentConfirmView_Previews: PreviewProvider {
    static var previews: some View {
        AppointmentConfirmView(visitReason: "", patientEmail: "", patientname: "", isNavigationActive: .constant(false))
    }
}


struct ActivityViewController: UIViewControllerRepresentable {
    
    var activityItems: [Any]
    var applicationActivities: [UIActivity]? = nil
    
    func makeUIViewController(context: UIViewControllerRepresentableContext<ActivityViewController>) -> UIActivityViewController {
        
        let controller = UIActivityViewController(activityItems: activityItems, applicationActivities: applicationActivities)
        controller.excludedActivityTypes = [
            UIActivity.ActivityType.mail,
            UIActivity.ActivityType.print,
            UIActivity.ActivityType.message,
        ]
        return controller
    }
    
    func updateUIViewController(_ uiViewController: UIActivityViewController, context: UIViewControllerRepresentableContext<ActivityViewController>) {}
    
}

//
//  HomeView.swift
//  MyDoktorUp
//
//  Created by Mac on 05/02/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI

struct HomeView: View {
    @ObservedObject var providerDetails = ProviderDetailObserver()
    @ObservedObject var providerSlots = ProviderSlotDetailObserver()
    @State private var isShowing = true    
    
    var body: some View {
        
        NavigationView{
            LoadingView(isShowing: .constant(isShowing)) {
                ScrollView {
                    VStack(alignment: .leading, spacing: 20, content: {
                        // Doctor Profile Details
                        DoctorProfileDetails(provider: self.providerDetails.arrProviders.first ?? Provider())
                        
                        Divider()
                        
                        // Doctor Available Appointment Slots
                        DoctorAvailableAppointmentSlots(provider: self.providerDetails.arrProviders.first ?? Provider())
                        
                        // MapView
                        Group {
                            MapLocationView()
                        }.frame(height: 150)
                            .padding(.horizontal, -20)
                        
                        Divider().padding(.top, 15)
                        
                        // Doctor Photos
                        DoctorPhotosView()
                        
                        //Review and Location
                        ReviewsAndLocationView()
                        Spacer()
                    })
                }.onAppear() {
                    
                    self.providerDetails.getProviderDetail(provideId: UserDefaultsManager().providerID ?? "") { (isSuccess) in
                        //commit - 24_04_2020
                        print("commit - 24_04_2020")
                        
                        self.isShowing = true
                        if isSuccess {
                            print("Success")
                            self.isShowing = false
                        } else {
                            print("Failure")
                            self.isShowing = false
                        }
                    }
                    self.providerSlots.getProviderAppointmentSlots(complitionHandler: { (isSuccess) in
                        self.isShowing = true
                        //commit - 24_04_2020
                        print("commit - 24_04_2020")
                        
                        if isSuccess {
                            self.isShowing = false
                            print("Success")
                        } else {
                            print("Failure")
                            self.isShowing = false
                        }
                    })
                }.onDisappear() {
                    self.isShowing = true
                }
            }
        
        }.navigationViewStyle(StackNavigationViewStyle()) // This line is supporting iPad, we should not comment out this line
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
struct TimeView : View {
    var appointmentInfo : AppointmentInfo = AppointmentInfo()
    var provider = Provider()
    @State var selection: Int? = nil
    @State var isActive: Bool = false
    @State var isSetTimeSlot: Bool = false
    var strTime = ""
    
    var body: some View {
        Button(action: {
        }) {
            NavigationLink(destination: ReviewAndBookView(appointmentInfo: appointmentInfo, provider: provider, isNavigationActive: self.$isActive), isActive: self.$isActive) {
                if isSetTimeSlot {
                    Text(appointmentInfo.appointment_time ?? "")
                        .font(.system(size: 16, weight: Font.Weight.medium, design: .default))
                        .lineLimit(nil)
                        .frame(minWidth: 120, maxWidth: .infinity, minHeight: 45, maxHeight: 45, alignment: .center)
                        .foregroundColor(.white)
                        .background(Color(hex: CustomFontColor.setDefaultApptTime))
                        .cornerRadius(8.0)
                } else {
                    Text(appointmentInfo.appointment_time ?? "")
                        .font(.system(size: 16, weight: Font.Weight.medium, design: .default))
                        .foregroundColor(Color(hex: CustomFontColor.appointmentTime))
                        .lineLimit(nil)
                        .frame(minWidth: 120, maxWidth: .infinity, minHeight: 45, maxHeight: 45, alignment: .center)
                        .overlay(
                            RoundedRectangle(cornerRadius: 8)
                                .strokeBorder(style: StrokeStyle(lineWidth: 1))
                                .foregroundColor(kCustomGrayColor)
                                .cornerRadius(8)
                    )

                }
            }.isDetailLink(false)
        }
    }
}
extension View {
    func hideNavigationBar() -> some View {
        self
            .navigationBarTitle("", displayMode: .inline)
            .navigationBarHidden(true)
    }
}

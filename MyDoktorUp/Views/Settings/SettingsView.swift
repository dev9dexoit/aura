//
//  SettingsView.swift
//  MyDoktorUp
//
//  Created by Mac on 05/02/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI

struct SettingsView: View {
    @EnvironmentObject var appState: AppState

    @State var logoutTab = false
    var body: some View {
        
         NavigationView {
            ZStack {
                Group {
                    List {
                        Section(header: Text("")) {
                            NavigationLink(destination: MyProfileView()) {
                                OptionRow(title: "My Profile")
                            }
                            NavigationLink(destination: TermsOfUseView()) {
                                OptionRow(title: "Terms of use")
                            }
                            NavigationLink(destination: PrivacyPolicyView()) {
                                OptionRow(title: "Privacy policy")
                            }
                        }
                        Section(header: Text("")) {
                            NavigationLink(destination: FeedbackView()) {
                                OptionRow(title: "Send us feedback")
                            }                            
                        }
                        Section(header: HStack {
                            Text("")
                                Spacer()
                        }) {
                            Button(action: {
                                UserDefaultsManager().setValueFor(isSign: false)
                                UserDefaultsManager().loginSessionId = nil
//                                self.logoutTab = true
                                self.appState.reloadDashboard(selectedTab: 0, selectedProvider: Provider())
                            }) {
                                HStack {
                                    Text("Log out").padding(.vertical, 10)
                                    Spacer()
                                }
                            }
                            
                            
                        }
                    }.padding(.vertical, -30)
                    .foregroundColor(kCustomGrayColor)
                }.background(kCustomLightBlueColor)
                .navigationBarTitle("Settings", displayMode: .inline)
            }.onAppear {
                UITableView.appearance().backgroundColor = .clear
                UITableView.appearance().separatorStyle = .singleLine
                UITableView.appearance().tableFooterView = UIView()
            }
        }.navigationViewStyle(StackNavigationViewStyle()) // This line is supporting iPad, we should not comment out this line
        .navigationBarTitle("", displayMode: .inline)
        .navigationBarBackButtonHidden(true)
        .hideNavigationBar()
    }
}

struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsView()
    }
}

//
//  ContactUsView.swift
//  MyDoktorUp
//
//  Created by Mac on 06/02/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI
import MessageUI

struct ContactUsView: View {
    
    @Environment(\.presentationMode) var presentationMode
    
    @State var isGeneralMail: Bool = false
    @State var isReportMail: Bool = false
    
    @State var result: Result<MFMailComposeResult, Error>? = nil
    
    var body: some View {
        Group {
            VStack(alignment: .leading, spacing: 15) {
                Text("Send us a message")
                    .font(.system(size: 28, weight: Font.Weight.thin, design: .default))
                    .foregroundColor(kPrimaryRegularColor)
                    .padding(.bottom, 20)
                
                Button(action: {
                    print("General email message")
                    self.isGeneralMail = true
                }) {
                    Text("General email message")
                        .font(.system(size: 16, weight: Font.Weight.medium, design: .default))
                        .foregroundColor(kCustomGrayColor)
                        .frame(minWidth: 0, idealWidth: 100, maxWidth: .infinity, minHeight: 40, idealHeight: 40, maxHeight: 40, alignment: .center)
                        .overlay(
                            RoundedRectangle(cornerRadius: 10)
                                .strokeBorder(style: StrokeStyle(lineWidth: 1))
                                .foregroundColor(kCustomGrayColor)
                                .cornerRadius(10)
                    )
                }.sheet(isPresented: self.$isGeneralMail) {
                    MailView(result: self.$result)
                }
                
                Button(action: {
                    print("Report a issue")
                    self.isReportMail = true
                }) {
                    Text("Report a issue")
                        .font(.system(size: 16, weight: Font.Weight.medium, design: .default))
                        .foregroundColor(kCustomGrayColor)
                        .frame(minWidth: 0, idealWidth: 100, maxWidth: .infinity, minHeight: 40, idealHeight: 40, maxHeight: 40, alignment: .center)
                        .overlay(
                            RoundedRectangle(cornerRadius: 10)
                                .strokeBorder(style: StrokeStyle(lineWidth: 1))
                                .foregroundColor(kCustomGrayColor)
                                .cornerRadius(10)
                    )
                }.sheet(isPresented: self.$isReportMail) {
                    MailView(result: self.$result)
                }
                
                Spacer()
                
            }
            .padding(17)
        }.background(kCustomLightBlueColor)
        .navigationBarTitle("Contact us")
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading:
            HStack {
                Button(action: {
                    self.presentationMode.wrappedValue.dismiss()
                }) {
                    Image("ic_back")
                }.foregroundColor(.white)
                .padding(.leading, -8)
        })
    }
}

struct ContactUsView_Previews: PreviewProvider {
    static var previews: some View {
        ContactUsView()
    }
}

//
//  FeedbackView.swift
//  MyDoktorUp
//
//  Created by Mac on 19/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI
import MessageUI

struct FeedbackView: View {
    
    @Environment(\.presentationMode) var presentationMode
    let functionalAreas = ["Scheduling", "Digital Check-in", "Telehealth", "Settings"]
    @State var selectedOption: String? = nil
    @State var isFeedbackMail: Bool = false
    @State var result: Result<MFMailComposeResult, Error>? = nil
    @State var mailRecipients: [String]? = ["feedback@doktorup.com"]
    @State var showingAlert = false
    @State var showErrorMessage: String = "Please select one functional area"
    
    var body: some View {
        
        ScrollView {
            VStack(alignment: .leading, spacing: 15) {
                Text("Send feedback")
                    .font(.system(.title))
                    .fontWeight(.regular)
                    .foregroundColor(kPrimaryRegularColor)
                
                Spacer()
                
                Text("Please select one functional area")
                    .font(.system(.body))
                    .fontWeight(.regular)
                    .foregroundColor(kCustomGrayColor)
                
                ForEach(functionalAreas, id: \.self) { item in
                    SelectionCell(feedbackOption: item, selectedOption: self.$selectedOption).padding(.leading, 5)
                }
                
                Spacer()
                Spacer()
                
                Text("Please describe the issue in detail and attach a screenshot in the email.").multilineTextAlignment(.center)
                
                Spacer()
                Button(action: {
                    print("Send Email tapped !!!")
                    if self.selectedOption == nil {
                        self.showingAlert = true
                        self.isFeedbackMail = false
                    } else {
                        self.showingAlert = false
                        self.isFeedbackMail = true
                    }
                    
                }) {
                        Spacer()
                        Text("Send Email")
                            .font(.system(.headline))
                            .foregroundColor(Color.white)
                            .padding(.vertical, 10)
                    .frame(minWidth: 0, maxWidth: .infinity)
                    .background(kPrimaryRegularColor)
                    .cornerRadius(10)
                }
                .sheet(isPresented: self.$isFeedbackMail) {
                    MailView(result: self.$result, mailSubject: self.selectedOption!, mailRecipients: self.mailRecipients)
                }
                .alert(isPresented: self.$showingAlert) {
                    Alert(title: Text("Error"), message: Text(self.showErrorMessage), dismissButton: .default(Text("OK")))
                        
                }
            }
            .padding(17)
        }.background(kCustomLightBlueColor)
            
            .navigationBarTitle("Feedback")
            .navigationBarBackButtonHidden(true)
            .navigationBarItems(leading:
                HStack {
                    Button(action: {
                        self.presentationMode.wrappedValue.dismiss()
                    }) {
                        Image("ic_back")
                    }.foregroundColor(.white)
                    .padding(.leading, -8)
            })
    }
    
}

struct FeedbackView_Previews: PreviewProvider {
    static var previews: some View {
        FeedbackView()
    }
}

struct SelectionCell: View {

    let feedbackOption: String
    @Binding var selectedOption: String?

    var body: some View {
        VStack(alignment: .leading, spacing: 0) {
            HStack {
                Text(feedbackOption).padding(.vertical, 10)
                Spacer()
                if feedbackOption == selectedOption {
                    Image(systemName: "checkmark")
                        .foregroundColor(kPrimaryRegularColor)
                }
                
            }
            FullWidthDivider().padding(.leading, 15)
        }
        .contentShape(Rectangle())
        .onTapGesture {
                self.selectedOption = self.feedbackOption
            }
    }
}

//
//  MyProfileView.swift
//  MyDoktorUp
//
//  Created by Mac on 05/02/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI

struct MyProfileView: View {
    
    let arrProfileOptions : [ProfileOption] = [ProfileOption(id: 0, title: "Insurance card"),
                                               ProfileOption(id: 1, title: "Driver License"),
                                               ProfileOption(id: 2, title: "Change Doctor"),
                                               ProfileOption(id: 3, title: "Account information"),
                                               ProfileOption(id: 4, title: "Touch ID / Passcode"),
                                               ProfileOption(id: 5, title: "Change Password"),
                                               ProfileOption(id: 6, title: "Notifications"),
                                               ProfileOption(id: 7, title: "Calendar Sync"),
                                               ProfileOption(id: 8, title: "Data privacy settings"),
                                               ProfileOption(id: 9, title: "HIPPA authorization")]
    
    @Environment(\.presentationMode) var presentationMode
    @EnvironmentObject var appState: AppState

    @State var doctorInfo = ""
    @State var isFromAppLogin = false
    @State var showError = false
    @State var showErrorMessage: String = ""
    @State var showingAlert = false
    
    var body: some View {
        
        ZStack {
            Group {
                List {
                    Section(header: EmptyView()) {
                        NavigationLink(destination: InsuranceCardView()) {
                            OptionRow(title: arrProfileOptions[0].title)
                        }
                        NavigationLink(destination: DrivingLicenseView()) {
                            OptionRow(title: arrProfileOptions[1].title)
                        }
                        
                        NavigationLink(destination: FindDoktor(isFirstFlow:false, isNavigationBarHidden: .constant(false))) {
                            HStack {
                                OptionRow(title: arrProfileOptions[2].title)
                                Spacer()
                                Text(UserDefaultsManager().doctorFullName ?? "")
                            }
                        }
                    }
                    Section(header: Text("")) {
                        NavigationLink(destination: AccountInfoView()) {
                            OptionRow(title: arrProfileOptions[3].title)
                        }                        
                        if isFromAppLogin {
                            NavigationLink(destination: ChangePasswordView()) {
                                OptionRow(title: arrProfileOptions[5].title)
                            }
                        } else {
                            HStack(alignment: .center, spacing: 0, content: {
                                OptionRow(title: arrProfileOptions[5].title).onTapGesture {
                                    self.showingAlert.toggle()
                                }
                                Spacer()
                            })
                                .alert(isPresented: self.$showingAlert) {
                                    Alert(title: Text("Error"), message: Text(self.showErrorMessage), dismissButton: .default(Text("OK")))
                            }
                        }
                    }
                    Section(header: Text("")) {
                        NavigationLink(destination: NotificationsView()) {
                            OptionRow(title: arrProfileOptions[6].title)
                        }
                        NavigationLink(destination: CalendarSyncView()) {
                            OptionRow(title: arrProfileOptions[7].title)
                        }
                    }
                    Section(header: Text("")) {
                        NavigationLink(destination: DataPrivacySettingsView()) {
                            OptionRow(title: arrProfileOptions[8].title)
                        }
                        NavigationLink(destination: HIPPAAuthorizationView()) {
                            OptionRow(title: arrProfileOptions[9].title)
                        }
                    }
                }
                    .foregroundColor(kCustomGrayColor)
            }.background(kCustomLightBlueColor)
        }.onAppear {
            UITableView.appearance().backgroundColor = .clear
            UITableView.appearance().separatorStyle = .singleLine
            UITableView.appearance().tableFooterView = UIView()
            
            self.doctorInfo = UserDefaults.standard.value(forKey: UserDefault.doctorInfo) as? String ?? ""
            if let typeLogin = UserDefaults.standard.value(forKey: UserDefault.loginType) as? String, typeLogin != LoginTypes.Email {
                self.isFromAppLogin = false
                //Set Error Message - Change Password is not available
                self.showErrorMessage = "Since you signed up with \(typeLogin), please change the password with \(typeLogin)."
                
            } else {
                self.isFromAppLogin = true                
            }
        }
        .navigationBarTitle(Text("My Profile"))
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading:
            HStack {
                Button(action: {
                    self.presentationMode.wrappedValue.dismiss()
                }) {
                    Image("ic_back")
                }.foregroundColor(.white)
                    .padding(.leading, -8)
        })
    }
}

struct MyProfileView_Previews: PreviewProvider {
    static var previews: some View {
        MyProfileView()
    }
}

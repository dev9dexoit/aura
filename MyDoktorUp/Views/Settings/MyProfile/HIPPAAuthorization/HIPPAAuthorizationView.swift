//
//  HIPPAAuthorizationView.swift
//  MyDoktorUp
//
//  Created by Mac on 05/02/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI

struct HIPPAAuthorizationView: View {
    
    @Environment(\.presentationMode) var presentationMode
    
    var healthCompany = "DoktorUp"
    @State var showSignatureView = false
    @State var isViewAdded = false
    @State var patientName = ""
    
    var body: some View {
        
        Group {
            VStack(alignment: .leading, spacing: 20, content: {
                HStack {
                    Spacer()
                    
                    Text("HIPPA Authorization")
                        .font(.system(size: 28, weight: Font.Weight.light, design: .default))
                        .foregroundColor(kPrimaryRegularColor)
                        .padding(.top, 20)
                    
                    Spacer()
                }
                
                HStack {
                    Spacer()
                    
                    Text("I understand that \(healthCompany) may disclose my health information to the authorized person below.")
                        .font(.system(size: 20, weight: Font.Weight.regular, design: .default))
                        .foregroundColor(kCustomTextBlueColor)
                        .multilineTextAlignment(.center)
                    
                    Spacer()
                }
                
                HStack(alignment: .top, spacing: 10, content: {
                    Spacer()
                    Button(action: {
                        self.isViewAdded = true
                    }) {
                        Image("ic_add")
                    }.foregroundColor(Color.blue)
                    Text("Add family or anyone that may have access to your medical records")
                        .font(.system(size: 20, weight: Font.Weight.regular, design: .default))
                    
                    Spacer()
                }).padding(.top, 10)
                
                if isViewAdded == true {
                    Form {
                        AddFamilyView()
                    }
                    
                }
                
            })
            
            Spacer()
            
            //Sign Agreement Button
            
            NavigationLink(destination: eSignatureView(), isActive: self.$showSignatureView) {
                Button(action: {
                    print("Sign Agreement Tapped!")
                    self.showSignatureView.toggle()
                }) {
                    HStack {
                        Text("Sign Agreement")
                            .font(.system(size: 16, weight: Font.Weight.medium, design: .default))
                            .foregroundColor(Color.white)
                    }
                    .frame(minWidth: 0, maxWidth: .infinity)
                    .frame(height:40)
                    .background(kPrimaryRegularColor)
                    .cornerRadius(10)
                }
                .padding(.all, 15)
            }
        }
        .navigationBarTitle("HIPPA Authorization")
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading:
            HStack {
                Button(action: {
                    self.presentationMode.wrappedValue.dismiss()
                }) {
                    Image("ic_back")
                }.foregroundColor(.white)
                    .padding(.leading, -8)
        })
    }
}

struct HIPPAAuthorizationView_Previews: PreviewProvider {
    static var previews: some View {
        HIPPAAuthorizationView()
    }
}


struct AddFamilyView: View {
    
    @State var isActive = false
    var relations = ["Spouse", "Parent", "Child", "Other"]
    @State var selectedRelation = 0
    
    var body: some View {
        Group {
            FreeTextView("Patient First Name, Last Name")
            
            FreeTextView("MM/DD/YYYY  HH:MM:SS")
            
            FreeTextView("* Authorized Person 1 First Name, Last Name")
            
            Picker(selection: self.$selectedRelation, label: Text("* Authorized Person 1 Relationship").foregroundColor(kCustomGrayColor)) {
                ForEach(0 ..< self.relations.count) {
                    Text(self.relations[$0])
                }
            }
            
            Toggle(isOn: self.$isActive) {
                Text("Patient able to sign?").foregroundColor(kCustomGrayColor)
            }.onTapGesture {
                self.isActive.toggle()
            }
            
            if self.isActive {
                FreeTextView("* Reason if patient is unable to sign")
            }            
        }
    }
}

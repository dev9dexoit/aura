//
//  eSignatureView.swift
//  MyDoktorUp
//
//  Created by Mac on 29/04/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI

struct eSignatureView: View {
    
    @Environment(\.presentationMode) var presentationMode
    @State var fullLegalName = ""
    @State var initials = ""
    
    var body: some View {
        
        Form {
            Group {
                
                TextField("Full Legal Name", text: $fullLegalName)
                
                TextField("Initiala", text: $initials)
                
                Group {
                    GeometryReader { geometry in
                        Group {
                            Text("Draw").foregroundColor(kCustomTextGrayColor)
                        }
                        .frame(width:geometry.size.width,height:geometry.size.height)
                            
                            
                        .overlay(
                            RoundedRectangle(cornerRadius: 10)
                                .strokeBorder(style: StrokeStyle(lineWidth: 1))
                                .foregroundColor(kCustomTextGrayColor)
                                .cornerRadius(10)
                        )
                    }.frame(height:180)
                    
                    Text("By clicking Sign, I agree that the signature, initials, will be digital representation of my paper signature.")
                    .foregroundColor(kCustomTextGrayColor)
                    .multilineTextAlignment(.center)
                    
                   Button(action: {
                        print("Sign Tapped!")
                        self.presentationMode.wrappedValue.dismiss()
                    }) {
                        HStack {
                            Text("Sign")
                                .font(.system(.body))
                                .fontWeight(.medium)
                                .foregroundColor(Color.white)
                                .padding(.vertical, 15)
                        }
                        .frame(minWidth: 0, maxWidth: .infinity)
                        .background(kPrimaryRegularColor)
                        .cornerRadius(10)
                    }
                }
            }
        }
        .padding(.top, -34)
        .background(kCustomLightBlueColor)
        .navigationBarTitle("eSignature")
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading:
            HStack {
                Button(action: {
                    self.presentationMode.wrappedValue.dismiss()
                }) {
                    Text("Cancel")
                }.foregroundColor(.white)
                    .padding(.leading, -8)
        })
    }
}

struct eSignatureView_Previews: PreviewProvider {
    static var previews: some View {
        eSignatureView()
    }
}

//
//  CalendarSyncView.swift
//  MyDoktorUp
//
//  Created by Mac on 05/02/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI

struct CalendarSyncView: View {

    @Environment(\.presentationMode) var presentationMode
    
    @State var isSyncCalendar = false
    
    let heightDivider: CGFloat = 0.8
    let leadingPaddingCommon: CGFloat = 17
    
    var body: some View {
        Group {
            VStack(alignment: .leading, spacing: 10, content: {
                
                Text("Sync with your Calendar").foregroundColor(kPrimaryRegularColor)
                    .font(.system(size: 16, weight: Font.Weight.regular, design: Font.Design.default))
                    .padding(.top, 20)
                
                Toggle(isOn: $isSyncCalendar) {
                    Text("Sync now").foregroundColor(kCustomDarkGrayColor)
                        .font(.system(size: 17, weight: Font.Weight.regular, design: Font.Design.default))
                }.padding(.top, 10)
                    .padding(.trailing, leadingPaddingCommon)
                
                HStack {
                    Spacer()
                        .frame(height:heightDivider)
                        .background(kCustomGrayColor)
                }
                
                Spacer()
            })
                .padding(.leading, leadingPaddingCommon)
        }
        .onAppear(perform: {
            self.isSyncCalendar = UserDefaults.standard.value(forKey: UserDefault.isCalendarSync) as? Bool ?? true
        })
            .background(kCustomLightBlueColor)
            .navigationBarTitle("Calendar Sync")
            .navigationBarBackButtonHidden(true)
            .navigationBarItems(leading:
                HStack {
                    Button(action: {
                        UserDefaults.standard.set(self.isSyncCalendar, forKey: UserDefault.isCalendarSync)
                        self.presentationMode.wrappedValue.dismiss()
                    }) {
                        Image("ic_back")
                    }.foregroundColor(.white)
                        .padding(.leading, -8)
            })
    }
}

struct CalendarSyncView_Previews: PreviewProvider {
    static var previews: some View {
        CalendarSyncView()
    }
}

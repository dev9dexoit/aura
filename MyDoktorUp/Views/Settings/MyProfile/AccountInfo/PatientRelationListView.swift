//
//  PatientRelationListView.swift
//  MyDoktorUp
//
//  Created by Mac on 31/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI

protocol PatientRelationDelegate {
    func didSelectRelationship(index: Int)
}

class PatientRelationSelectionObserver: ObservableObject {
    @Published var relationName: String = ""
    
    func setRelationshipName(name: String) {
        self.relationName = name
    }
}

struct PatientRelationListView: View {
    
    @Environment(\.presentationMode) var presentationMode
    @State var relationNames: [String] = []
    @State var patientRelationDelegate: PatientRelationDelegate?
    
    var body: some View {
        List {
            ForEach(self.relationNames, id: \.self) { relation in
                Button(action: {
                    if self.patientRelationDelegate != nil {
                        if self.relationNames.count > 0 {
                            self.patientRelationDelegate!.didSelectRelationship(index: self.relationNames.firstIndex(of: relation) ?? 0)
                            self.presentationMode.wrappedValue.dismiss()
                            self.patientRelationDelegate = nil
                        }
                    }
                }) {
                    Text(relation)
                }
            }
        }.padding(.top, -34)
    }
}

struct PatientRelationListView_Previews: PreviewProvider {
    static var previews: some View {
        PatientRelationListView()
    }
}

//
//  PatientGenderListView.swift
//  MyDoktorUp
//
//  Created by Mac on 08/06/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI

protocol PatientGenderDelegate {
    func didSelectGender(index: Int)
}

class PatientGenderSelectionObserver: ObservableObject {
    @Published var gender: String = ""
    
    func setGender(title: String) {
        self.gender = title
    }
}

struct PatientGenderListView: View {
    @Environment(\.presentationMode) var presentationMode
    @State var genders: [String] = []
    @State var patientGenderDelegate: PatientGenderDelegate?
    
    var body: some View {
        List {
            ForEach(self.genders, id: \.self) { gender in
                Button(action: {
                    if self.patientGenderDelegate != nil {
                        if self.genders.count > 0 {
                            self.patientGenderDelegate!.didSelectGender(index: self.genders.firstIndex(of: gender) ?? 0)
                            self.presentationMode.wrappedValue.dismiss()
                        }
                    }
                }) {
                    Text(gender)
                }
            }
        }.padding(.top, -34)
    }
}

struct PatientGenderListView_Previews: PreviewProvider {
    static var previews: some View {
        PatientGenderListView()
    }
}

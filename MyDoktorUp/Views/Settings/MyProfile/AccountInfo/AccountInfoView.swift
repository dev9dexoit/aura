//
//  AccountInfoView.swift
//  MyDoktorUp
//
//  Created by Mac on 05/02/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI

struct AccountInfoView: View {
    
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject var observedUserInformation = UserInformationObserver()
    
    @State var userName: String = UserDefaults.standard.value(forKey: "userFullName") as? String ?? ""
    @State var userEmail: String = UserDefaults.standard.value(forKey: "userEmail") as? String ?? ""
    @State var firstName: String = ""
    @State var lastName: String = ""
    @State var birthDate = Date()
    @State var selectedGender = 0
    @State var selectedPatient = 0
    @State var emailAddress: String = ""
    @State var genderString: String = ""
    @State var relationString: String = ""
    
    @State var address1: String = ""
    @State var address2: String = ""
    @State var city: String = ""
    @State var state: String = ""
    @State var zip: String = ""
    @State var isShowing = true
    
    @State var isActive = false
    @State var isActiveGender = false
    
    var arrGender = ["Male", "Female", "Others"]
    @State var onlyNames: [String] = []
    @State var relationshipWithNames: [String] = []
    @State var onlyRelationships: [String] = []
    @State var usersBookingFor = [UserBookingFor]()
    @State var patientInfo: UserBookingFor?
    
    @ObservedObject var phoneNo = TextBindingManager()
    @State var phoneNumber = ""
    @State var isDataSet = false
    @State var isForSelf: Bool = true
    @State var profileAttributes = UserProfileAttributes()
    @State var userSpecialAttributes = UserSpecialAttributes()
    @State var showErrorMessage: String = ""
    @State var showingAlert = false
    @ObservedObject var accountInformationObserver = AccountInformationObserver()
    @State var dobString = ""
    @State var alertTitle = "Error"
    
    var body: some View {
        LoadingView(isShowing: .constant(isShowing)) {
            Group {
                Form {
                    Section(header:
                        Group {
                            VStack(alignment: .center, spacing: 10) {
                                HStack(alignment: .center, spacing: 0){
                                    Spacer()
                                    Group {
                                        Image("ic_user_placeholder")
                                            .resizable()
                                            .scaledToFit()
                                            .frame(height: 50)
                                            .clipped()
                                            .listRowInsets(EdgeInsets())
                                            .background(kCustomLightBlueColor)
                                        
                                    }
                                    .frame(width:100,height:100)
                                        
                                    .overlay(
                                        RoundedRectangle(cornerRadius: 100/2)
                                            .strokeBorder(style: StrokeStyle(lineWidth: 1))
                                            .foregroundColor(kCustomGrayColor)
                                    )
                                    
                                    Spacer()
                                }
                                
                                Text(self.userName).fontWeight(.medium).font(.body)
                                Text(self.userEmail).fontWeight(.regular).font(.body)                                
                            }
                            Text("Personal information").foregroundColor(kPrimaryRegularColor).fontWeight(.thin).font(.system(.title))
                        }
                        ) {
                        
                        HStack(alignment: .center, spacing: 10, content: {
                            NavigationLink(destination: PatientRelationListView(relationNames: self.relationshipWithNames, patientRelationDelegate: self), isActive: self.$isActive) {
                                Button("* Patient") {
                                    self.isActive = true
                                }
                                Spacer()
                                Text(self.relationString)
                            }
                            
                        })
                        
                        TextField("* First Name", text: self.$firstName)
                        
                        TextField("* Last Name", text: self.$lastName)
                        
                        TextField("* Phone", text: self.$phoneNumber).keyboardType(.numberPad).disabled(!self.isForSelf)
                        
                        DatePicker(selection: self.$birthDate, in: ...Date(), displayedComponents: .date) {
                            HStack {
                                Text("* Date of Birth")
                            }
                        }
                        
                        HStack(alignment: .center, spacing: 10, content: {
                            NavigationLink(destination: PatientGenderListView(genders: self.arrGender, patientGenderDelegate: self), isActive: self.$isActiveGender) {
                                Button("* Gender") {
                                    self.isActiveGender = true
                                }
                                Spacer()
                                Text(self.genderString)
                            }
                            
                        })
                        
                        TextField("* Email", text: self.$emailAddress).keyboardType(.emailAddress).disabled(true)
                    }.padding(.vertical, 10)
                        .foregroundColor(kCustomGrayColor)
                    
                    Section(header: Text("Address").foregroundColor(kPrimaryRegularColor).fontWeight(.thin).font(.system(.title))) {
                        
                        TextField("Address 1", text: self.$address1).disabled(!self.isForSelf)
                        TextField("Address 2", text: self.$address2).disabled(!self.isForSelf)
                        TextField("City", text: self.$city).disabled(!self.isForSelf)
                        TextField("State", text: self.$state).disabled(!self.isForSelf)
                        TextField("Zip", text: self.$zip).keyboardType(.numberPad).disabled(!self.isForSelf)
                        
                        HStack {
                            Button(action: {
                                //API CALL
                                if self.validateFields() {
                                    self.updateAccountInformation()
                                }
                                
                            }) {
                                Text("Save")
                                    .font(.system(.headline))
                                    .foregroundColor(Color.white)
                                    .padding(.vertical, 10)
                                    .frame(minWidth: 0, maxWidth: .infinity)
                                    .background(kPrimaryRegularColor)
                                    .cornerRadius(10)
                            }
                        }
                        .alert(isPresented: self.$showingAlert) {
                            Alert(title: Text(self.alertTitle), message: Text(self.showErrorMessage), dismissButton: .default(Text("OK")))
                        }
                        
                    }.padding(.vertical, 10)
                        .foregroundColor(kCustomGrayColor)
                }
            }
        }
        .navigationBarTitle("Account Information")
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading:
            HStack {
                Button(action: {
                    self.presentationMode.wrappedValue.dismiss()
                }) {
                    Image("ic_back")
                }.foregroundColor(.white)
                .padding(.leading, -8)
        })
        .onAppear {
            self.isActive = false
            self.isActiveGender = false
            
            if !self.isDataSet {
                if let userEmail = UserDefaults.standard.value(forKey: UserDefault.userEmail) as? String {
                    self.observedUserInformation.getUser(strEmail: userEmail) { (_,_)  in
                        self.isShowing = false
                        self.relationshipWithNames.removeAll()
                        self.usersBookingFor.removeAll()
                        self.onlyNames.removeAll()
                        self.onlyRelationships.removeAll()
                        
                        for case let relationWithName in self.observedUserInformation.arrBookingFor {
                            self.relationshipWithNames.append(relationWithName.relationDesignation)
                        }
                        for case let name in self.observedUserInformation.onlyPersonNames {
                            self.onlyNames.append(name)
                        }
                        for case let relation in self.observedUserInformation.onlyPersonRelations {
                            self.onlyRelationships.append(relation)
                        }
                        
                        self.isDataSet = true
                        if let userProfileAttributes = self.observedUserInformation.userProfileAttributes {
                            self.profileAttributes = userProfileAttributes
                            self.firstName = self.profileAttributes.first_name ?? ""
                            self.lastName = self.profileAttributes.last_name ?? ""
                            self.emailAddress = self.userEmail
                            self.address1 = userProfileAttributes.address1 ?? ""
                            self.address2 = userProfileAttributes.address2 ?? ""
                            self.city = userProfileAttributes.city ?? ""
                            self.state = userProfileAttributes.state ?? ""
                            self.zip = userProfileAttributes.zip ?? ""
                            
                            if userProfileAttributes.dob == nil {
                                self.birthDate = Date()
                            } else {
                                self.birthDate = self.convertStringToDate(strDate: (userProfileAttributes.dob ?? "\(Date())"))
                            }
                                                        
                            self.phoneNo.text = userProfileAttributes.phone ?? "0000000000"
                            self.phoneNumber = userProfileAttributes.phone ?? ""
                            self.onlyNames.insert(self.firstName + " " + self.lastName, at: 0)
                        }
                        
                        if let userSpecialAttributes = self.observedUserInformation.userSpecialAttributes {
                            self.userSpecialAttributes = userSpecialAttributes
                            self.genderString = self.userSpecialAttributes.gender ?? "Male"
                        }
                        
                        if self.observedUserInformation.usersBookingFor.count > 0 {
                            self.usersBookingFor = self.observedUserInformation.usersBookingFor
                        }
                        
                        self.relationString = self.relationshipWithNames[0]
                    }
                }
            }
        }
    }
    func convertStringToDate(strDate: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        return dateFormatter.date(from:strDate)!
    }
    
    func validateFields() -> Bool {
        if self.firstName == "" {
            self.showErrorMessage = "Please enter first name"
            self.showingAlert.toggle()
            return false
        } else if self.lastName == "" {
            self.showErrorMessage = "Please enter last name"
            self.showingAlert.toggle()
            return false
        } else if self.phoneNumber == "" {
            self.showErrorMessage = "Please enter phone number"
            self.showingAlert.toggle()
            return false
        } else if self.phoneNumber.count < 10 {
            self.showErrorMessage = "Please enter valid phone number"
            self.showingAlert.toggle()
            return false
        } else {
            return true
        }
    }
    
    func updateAccountInformation() {
        
        var user_booking_forself: [String: Any] = [:]
        self.dobString = self.birthDate.getCurrentDateAsFormattedString()
        
        let profile_attributes: [String:String] = ["last_name": self.lastName,
                                                   "first_name": self.firstName,
                                                   "address1": self.address1,
                                                   "address2": self.address2,
                                                   "dob": self.dobString,
                                                   "zip": self.zip,
                                                   "city": self.city,
                                                   "state": self.state,
                                                   "phone": self.phoneNumber]
        
        if let userBookingInfo = self.patientInfo {
            let patientId = userBookingInfo.patient_id ?? 0
            let relationship = self.relationString
            let simpleName = self.firstName + " " + self.lastName
            let dob = self.dobString
            let gender = self.genderString
            let user_allowed_to_book_forself = userBookingInfo.user_allowed_to_book_forself ?? []
            
            user_booking_forself = [
                "patient_id": patientId,
                "relationship": relationship,
                "simple_name": simpleName,
                "dob": dob,
                "gender": gender,
                "user_allowed_to_book_forself": user_allowed_to_book_forself
            ]
        }
        
        let special_attributes: [String:String] = ["gender": self.genderString,
                                                   "marital_status": "",
                                                   "language": "",
                                                   "race": "",
                                                   "ethnicity": ""]
            
        var dictToSend: [String: Any] = [:]
        
        if self.isForSelf {
            dictToSend = ["profile_attributes": profile_attributes,
                          "special_attributes": special_attributes]
        } else {
            dictToSend = ["user_booking_forself": [user_booking_forself]]
        }
        
        self.accountInformationObserver.updateAccountInfo(infoToSend: dictToSend, complitionHandler: { (status, meessage) in
            if status {
                //Success
                //Redirect Back
                self.alertTitle = "Success"
                self.showErrorMessage = meessage
                self.showingAlert.toggle()
                self.presentationMode.wrappedValue.dismiss()
            } else {
                //Failure
                self.showErrorMessage = meessage
                self.showingAlert.toggle()
            }
        })
    }
    
    func showResponseAlert(status:Bool, title:String, message:String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let actionOk = UIAlertAction(title: "OK", style: .default) { (_) in
            if status {
                
            } else {
                //Try Again
            }
        }
        alertController.addAction(actionOk)
    }
    
}

struct AccountInfoView_Previews: PreviewProvider {
    static var previews: some View {
        AccountInfoView()
    }
}

extension AccountInfoView: PatientRelationDelegate {
    func didSelectRelationship(index: Int) {
        self.isActive = false
        self.relationString = self.onlyRelationships[index]
        
        if index == 0 {
            self.isForSelf = true
            print(self.isForSelf)
            self.genderString = self.profileAttributes.gender ?? "Male"
            self.firstName = self.profileAttributes.first_name ?? ""
            self.lastName = self.profileAttributes.last_name ?? ""
            
            if self.profileAttributes.dob == nil {
                self.birthDate = Date()
            } else {
                self.birthDate = self.convertStringToDate(strDate: (self.profileAttributes.dob ?? "\(Date())"))
            }
            
        } else {
            self.isForSelf = false
            print(self.isForSelf)
            self.patientInfo = self.usersBookingFor[index-1]
            self.genderString = self.patientInfo!.gender ?? ""
            self.birthDate = self.convertStringToDate(strDate: (self.patientInfo!.dob ?? "\(Date())"))
            if let name = self.patientInfo?.simple_name {
                let names = name.components(separatedBy: " ")
                if names.count == 1 {
                    self.firstName = names[0]
                    self.lastName = ""
                } else if names.count == 2 {
                    self.firstName = names[0]
                    self.lastName = names[1]
                }
            }
        }
    }
}

extension AccountInfoView: PatientGenderDelegate {
    func didSelectGender(index: Int) {
        self.isActiveGender = false
        self.genderString = self.arrGender[index]
    }
}


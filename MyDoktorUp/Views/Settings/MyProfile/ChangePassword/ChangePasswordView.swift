//
//  ChangePasswordView.swift
//  MyDoktorUp
//
//  Created by Mac on 19/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI

struct ChangePasswordView: View {
    
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject var observedAPICall = ChangePasswordObserver()
    
    @State var showError = false
    @State var showErrorMessage: String = ""
    @State var showingAlert = false
    @State var isShowing = false
    
    @State var email = UserDefaults.standard.value(forKey: UserDefault.userEmail) as? String ?? "user@email.com"
    @State var currentPassword = ""
    @State var newPassword = ""
    @State var isSecuredCurrentPassword = false
    @State var isSecuredNewPassword = false
    
    var body: some View {
        LoadingView(isShowing: .constant(isShowing)) {
            
            ScrollView {
                VStack(alignment: .leading, spacing: 0, content: {
                    Group {
                        
                        Text("Change your password")
                            .foregroundColor(kCustomTextBlueColor)
                            .font(.body).fontWeight(.regular)
                            .multilineTextAlignment(.leading)
                            .padding(.top, 15)
                            .padding(.bottom, 10)
                        
                        Text("Minimum 8 characters (no spaces) Letters, numbers and special characters").foregroundColor(kCustomGrayColor).font(.body).fontWeight(.regular)
                            .lineLimit(3)
                            .padding(.vertical, 20)
                        
                        
                        Group {
                            
                            TextField("user@email.com", text: self.$email).disabled(true)
                            DividerViewGray()
                            
                            VStack(alignment: .leading, spacing: 6) {
                                ZStack(alignment: .leading) {
                                    if self.currentPassword.isEmpty { Text("Existing Password").foregroundColor(kCustomGrayColor) }
                                    
                                    if self.isSecuredCurrentPassword {
                                        HStack {
                                            TextField("", text: self.$currentPassword)
                                                .foregroundColor(kCustomGrayColor)
                                                .accentColor(kCustomGrayColor)
                                            Spacer()
                                            Button(action: {
                                                self.isSecuredCurrentPassword = false
                                            }) {
                                                
                                                Image("Hide-Password")
                                                    .resizable()
                                                    .foregroundColor(kPrimaryRegularColor)
                                                    .frame(width: 20, height:20, alignment: .trailing)
                                                    .padding(.trailing, 15)
                                            }
                                        }
                                    } else {
                                        
                                        HStack {
                                            SecureField("", text: self.$currentPassword)
                                                .foregroundColor(kCustomGrayColor)
                                                .accentColor(kCustomGrayColor)
                                            Spacer()
                                            Button(action: {
                                                self.isSecuredCurrentPassword = true
                                            }) {
                                                
                                                Image("Show-Password")
                                                    .resizable()
                                                    .foregroundColor(kPrimaryRegularColor)
                                                    .frame(width: 20, height: 20, alignment: .trailing)
                                                    .padding(.trailing, 15)
                                            }
                                        }
                                    }
                                }.padding(.vertical, 5)
                            }
                            DividerViewGray()
                            
                            VStack(alignment: .leading, spacing: 6) {
                                ZStack(alignment: .leading) {
                                    if self.newPassword.isEmpty { Text("New Password").foregroundColor(kCustomGrayColor) }
                                    
                                    if self.isSecuredNewPassword {
                                        HStack {
                                            TextField("", text: self.$newPassword)
                                                .foregroundColor(kCustomGrayColor)
                                                .accentColor(kCustomGrayColor)
                                            Spacer()
                                            Button(action: {
                                                self.isSecuredNewPassword = false
                                            }) {
                                                
                                                Image("Hide-Password")
                                                    .resizable()
                                                    .foregroundColor(kPrimaryRegularColor)
                                                    .frame(width: 20, height:20, alignment: .trailing)
                                                    .padding(.trailing, 15)
                                            }
                                        }
                                    } else {
                                        
                                        HStack {
                                            SecureField("", text: self.$newPassword)
                                                .foregroundColor(kCustomGrayColor)
                                                .accentColor(kCustomGrayColor)
                                            Spacer()
                                            Button(action: {
                                                self.isSecuredNewPassword = true
                                            }) {
                                                
                                                Image("Show-Password")
                                                    .resizable()
                                                    .foregroundColor(kPrimaryRegularColor)
                                                    .frame(width: 20, height: 20, alignment: .trailing)
                                                    .padding(.trailing, 15)
                                            }
                                        }
                                    }
                                }.padding(.vertical, 5)
                            }
                            
                            DividerViewGray()
                        }.padding(.vertical, 5)
                        
                        
                        HStack {
                            Button(action: {
                                if self.validateFields() {
                                    self.changePassword()
                                }
                                
                            }) {
                                Text("Save")
                                    .font(.system(.headline))
                                    .foregroundColor(Color.white)
                                    .padding(.vertical, 10)
                                    .frame(minWidth: 0, maxWidth: .infinity)
                                    .background(kPrimaryRegularColor)
                                    .cornerRadius(10)
                            }
                        }.padding(.top, 20)
                        .padding(.trailing, 20)
                            .alert(isPresented: self.$showingAlert) {
                                Alert(title: Text("Error"), message: Text(self.showErrorMessage), dismissButton: .default(Text("OK")))
                                
                        }
                    }.padding(.leading, 20)
                })
            }
            
            
        }.background(kCustomLightBlueColor)
        .navigationBarTitle("Change Password")
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading:
            HStack {
                Button(action: {
                    self.presentationMode.wrappedValue.dismiss()
                }) {
                    Image("ic_back")
                }.foregroundColor(.white)
                .padding(.leading, -8)
        })
    }
    
    private func validateFields() -> Bool {
        if self.currentPassword == "" {
            self.showErrorMessage = "Please enter existing password"
            self.showingAlert.toggle()
            return false
        } else if self.newPassword == "" {
            self.showErrorMessage = "Please enter new password"
            self.showingAlert.toggle()
            return false
        } else {
            return true
        }
    }
    
    private func changePassword() {
        let dict = ["password": self.newPassword,
                    "old_password": self.currentPassword]
        self.isShowing = true
        self.observedAPICall.changePassword(dictToPost: dict, complitionHandler: { (isSucces, error) in
            if isSucces {
                self.isShowing = false
            } else {
                self.isShowing = false
                self.showErrorMessage = error?.localizedDescription ?? ""
                self.showingAlert.toggle()
            }
        })
    }
}

struct ChangePasswordView_Previews: PreviewProvider {
    static var previews: some View {
        ChangePasswordView()
    }
}

struct DividerViewGray: View {
    var body: some View {
        HStack {
            Spacer()
                .frame(height:1)
                .background(kCustomLightGrayDisable)
                .padding(.leading, -4)
        }
    }
}

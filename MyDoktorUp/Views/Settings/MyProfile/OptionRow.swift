//
//  OptionView.swift
//  MyDoktorUp
//
//  Created by Mac on 05/02/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI

struct OptionRow: View {
    
    var title: String
    var body: some View {


        HStack {
            Text(title).padding(.vertical, 10)
            Spacer()
        }
       
    }

}

struct OptionView_Previews: PreviewProvider {
    static var previews: some View {
        OptionRow(title: "")
    }
}

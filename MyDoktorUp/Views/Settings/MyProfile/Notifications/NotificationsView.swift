//
//  NotificationsView.swift
//  MyDoktorUp
//
//  Created by Mac on 05/02/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI

struct NotificationsView: View {
    
    @Environment(\.presentationMode) var presentationMode
    @State var isNotificationAllow = false
    
    let heightDivider: CGFloat = 0.8
    let leadingPaddingCommon: CGFloat = 17
    
    var body: some View {
        Group {
            VStack(alignment: .leading, spacing: 10, content: {
                
                Text("Notifications").foregroundColor(kPrimaryRegularColor)
                    .font(.system(size: 16, weight: Font.Weight.regular, design: Font.Design.default))
                    .padding(.top, 20)
                
                Toggle(isOn: $isNotificationAllow) {
                    Text("Allow push notifications?").foregroundColor(kCustomDarkGrayColor)
                        .font(.system(size: 17, weight: Font.Weight.regular, design: Font.Design.default))
                }.padding(.top, 10)
                    .padding(.trailing, leadingPaddingCommon)
                
                HStack {
                    Spacer()
                        .frame(height:heightDivider)
                        .background(kCustomGrayColor)
                }
                
                Spacer()
            })
                .padding(.leading, leadingPaddingCommon)
        }
            .onAppear(perform: {
                self.isNotificationAllow = UserDefaults.standard.value(forKey: UserDefault.LocalPushNotification) as? Bool ?? true
            })
        .background(kCustomLightBlueColor)
        .navigationBarTitle("Notifications")
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading:
            HStack {
                Button(action: {
                    UserDefaults.standard.set(self.isNotificationAllow, forKey: UserDefault.LocalPushNotification)
                    self.presentationMode.wrappedValue.dismiss()
                }) {
                    Image("ic_back")
                }.foregroundColor(.white)
                .padding(.leading, -8)
        })
    }
}

struct NotificationsView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationsView()
    }
}

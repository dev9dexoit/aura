//
//  InsuranceCardView.swift
//  MyDoktorUp
//
//  Created by Mac on 05/02/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI

struct InsuranceCardView: View {
    
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject var observerChekingAppointment = ChekingAppointment_Observer()
    
    @State var fWidth: CGFloat = 35.0
    @State var fHeight: CGFloat = 35.0
    @State var bWidth: CGFloat = 35.0
    @State var bHeight: CGFloat = 35.0
    @State var isInsuranceFront: Bool = false
    @State var insuranceFrontImage:UIImage = UIImage(named: "ic_camera")!
    @State var isInsuranceBack: Bool = false
    @State var insuranceBackImage:UIImage = UIImage(named: "ic_camera")!
    
    var body: some View {
        
        ScrollView {
            Group {
                
                HStack {
                    Text("Add your insurance card")
                        .fontWeight(.regular)
                        .font(.headline)
                        .foregroundColor(kPrimaryRegularColor)
                        .padding(.top, 15)
                    Spacer()
                    
                }.padding(.horizontal, 17)
                
                HStack {
                    Text("Store your insurance card here securely to share with doctors and staff.")
                        .fontWeight(.regular)
                        .font(.headline)
                        .foregroundColor(kCustomGrayColor)
                        .padding(.top, 15)
                    Spacer()
                    
                }.padding(.horizontal, 17)
                
                HStack {
                    Text("Front side of card")
                        .fontWeight(.regular)
                        .font(.headline)
                        .foregroundColor(kCustomGrayColor)
                        .padding(.top, 30)
                    
                    Spacer()
                }.padding(.horizontal, 17)
                
                GeometryReader { geometry in
                    
                    Button(action: {
                        self.isInsuranceFront = true
                    }) {
                        Group {
                            Image(uiImage: self.insuranceFrontImage)
                                .renderingMode(.original)
                                .resizable()
                                .scaledToFit()
                                .frame(width: self.fWidth, height: self.fHeight)
                                .clipped()
                                .listRowInsets(EdgeInsets())
                        }
                        .frame(width:geometry.size.width, height:geometry.size.height)
                            
                            
                        .overlay(
                            RoundedRectangle(cornerRadius: 10)
                                .strokeBorder(style: StrokeStyle(lineWidth: 1))
                                .foregroundColor(kCustomGrayColor)
                                .cornerRadius(10)
                        )
                        
                    }.sheet(isPresented: self.$isInsuranceFront) {
                        ScannerView(Title:"Insurance Back side of card",completion: { image in
                            self.fWidth = geometry.size.width
                            self.fHeight = geometry.size.height
                            self.insuranceFrontImage = image
                        })
                    }
                    
                }.padding(.horizontal, 17)
                    .frame(height:180)
                
                
                HStack {
                    Text("Back side of card")
                        .fontWeight(.regular)
                        .font(.headline)
                        .foregroundColor(kCustomGrayColor)
                        .padding(.top, 30)
                    Spacer()
                    
                }.padding(.horizontal, 17)
                
                
                GeometryReader { geometry in
                    
                    Button(action: {
                        self.isInsuranceBack = true
                    }) {
                        Group {
                            Image(uiImage: self.insuranceBackImage)
                                .renderingMode(.original)
                                .resizable()
                                .scaledToFit()
                                .frame(width: self.bWidth, height: self.bHeight)
                                .clipped()
                                .listRowInsets(EdgeInsets())
                        }
                        .frame(width:geometry.size.width,height:geometry.size.height)
                            
                            
                        .overlay(
                            RoundedRectangle(cornerRadius: 10)
                                .strokeBorder(style: StrokeStyle(lineWidth: 1))
                                .foregroundColor(kCustomGrayColor)
                                .cornerRadius(10)
                        )
                        
                    }.sheet(isPresented: self.$isInsuranceBack) {
                        ScannerView(Title:"Insurance Back side of card",completion: { image in
                            self.bWidth = geometry.size.width
                            self.bHeight = geometry.size.height
                            self.insuranceBackImage = image
                        })
                    }
                    
                }.padding(.horizontal, 17)
                    .frame(height:180)
                
                Spacer(minLength: 20)
                
                Button(action: {
                    self.observerChekingAppointment.postInsurance(Frontimage: self.insuranceFrontImage, Backimage: self.insuranceBackImage) { (postSucess) in
                        print("postInsurance sucess")
                    }
                }) {
                        Spacer()
                        Text("Save")
                            .font(.system(.headline))
                            .foregroundColor(Color.white)
                            .padding(.vertical, 10)
                    .frame(minWidth: 0, maxWidth: .infinity)
                    .background(kPrimaryRegularColor)
                    .cornerRadius(10)
                }.padding(.trailing, 17)
                .padding(.leading, 9)
            }
        }
        .background(kCustomLightBlueColor)
        .navigationBarTitle("Insurance Card")
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading:
            HStack {
                Button(action: {
                    self.presentationMode.wrappedValue.dismiss()
                }) {
                    Image("ic_back")
                }.foregroundColor(.white)
                .padding(.leading, -8)
        })
    }
    
}

struct InsuranceCardView_Previews: PreviewProvider {
    static var previews: some View {
        InsuranceCardView()
    }
}

//
//  TouchIdPasscodeView.swift
//  MyDoktorUp
//
//  Created by Mac on 05/02/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI

struct TouchIdPasscodeView: View {
    
    @Environment(\.presentationMode) var presentationMode
    
    @State var isUserPasscode = false
    
    let heightCommon: CGFloat = 40
    let heightDivider: CGFloat = 0.8
    let leadingPaddingCommon: CGFloat = 17
    
    var body: some View {
        
        Group {
            VStack(alignment: .leading, spacing: 10, content: {
                
                Text("Make your medical information more secure by entering your passcode when the app launches.").foregroundColor(kPrimaryRegularColor)
                    .font(.system(size: 16, weight: Font.Weight.regular, design: Font.Design.default))
                    .padding(.top, 20)
                    .padding(.trailing, leadingPaddingCommon)
                
                Toggle(isOn: $isUserPasscode) {
                    Text("Use Passcode").foregroundColor(kCustomDarkGrayColor)
                        .font(.system(size: 17, weight: Font.Weight.regular, design: Font.Design.default))
                }.padding(.top, 10)
                    .padding(.trailing, leadingPaddingCommon)
                
                HStack {
                    Spacer()
                        .frame(height:heightDivider)
                        .background(kCustomGrayColor)
                }
                
                HStack {
                    Text("Change Passcode")
                        .font(.system(size: 17, weight: Font.Weight.regular, design: Font.Design.default))
                        .foregroundColor(kCustomDarkGrayColor)
                    
                    Spacer()
                    
                    Image("ic_right_arrow")
                        .resizable()
                        .scaledToFit()
                        .frame(height: 15)
                        .clipped()
                        .listRowInsets(EdgeInsets())
                        .padding(.trailing, leadingPaddingCommon)
                    
                }.frame(height:heightCommon)
                
                HStack {
                    Spacer()
                        .frame(height:heightDivider)
                        .background(kCustomGrayColor)
                }.padding(.trailing, 0)
                
                Spacer()
            })
                .padding(.leading, leadingPaddingCommon)
        }
        .background(kCustomLightBlueColor)
        .navigationBarTitle("Touch ID / Passcode")
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading:
            HStack {
                Button(action: {
                    self.presentationMode.wrappedValue.dismiss()
                }) {
                    Image("ic_back")
                }.foregroundColor(.white)
                .padding(.leading, -8)
        })
    }
}

struct TouchIdPasscodeView_Previews: PreviewProvider {
    static var previews: some View {
        TouchIdPasscodeView()
    }
}

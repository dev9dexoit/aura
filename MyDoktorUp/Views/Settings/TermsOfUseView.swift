//
//  TermsOfUseView.swift
//  MyDoktorUp
//
//  Created by Mac on 06/02/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI

struct TermsOfUseView: View {
    
    @Environment(\.presentationMode) var presentationMode
    
    var body: some View {
        VStack {
            WebView(request: URLRequest(url: URL(string: "https://www.sheldonbrown.com/web_sample1.html")!))
        }.background(kCustomLightBlueColor)
        .navigationBarTitle("Terms of use")
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading:
            HStack {
                Button(action: {
                    self.presentationMode.wrappedValue.dismiss()
                }) {
                    Image("ic_back")
                }.foregroundColor(.white)
                .padding(.leading, -8)
        })
    }
}

struct TermsOfUseView_Previews: PreviewProvider {
    static var previews: some View {
        TermsOfUseView()
    }
}

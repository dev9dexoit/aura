//
//  InitialView.swift
//  MyDoktorUp
//
//  Created by Mac on 11/02/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI
import Combine
class TextBindingManager: ObservableObject {
    @Published var isValidNumber = false
    var isDeleteStarted = false
    
    
    @Published var text:String = ""{
        
        didSet {
            print("text \(text)")
            if text.count > characterLimit && oldValue.count <= characterLimit {
                
                text = oldValue
                isValidNumber = true
                
            } else {
                
                
                if text.count == 12 {
                    isValidNumber = true
                } else {
                    
                    isValidNumber = false
                }
            }
            
        }
    }
    let characterLimit = 12
}


struct InitialView: View {
    @State var isNavigationBarHidden: Bool = false
    @State var isFirstFlow:Bool = true
    
    var body: some View {
        
        
        NavigationView {
            
            FindDoktor(isFirstFlow:self.isFirstFlow,isNavigationBarHidden: self.$isNavigationBarHidden)
                .navigationBarTitle("",displayMode: .inline)
                .navigationBarHidden(self.isNavigationBarHidden)
                .onAppear {
                    self.isNavigationBarHidden = false
            }
            
            
            
        }.navigationViewStyle(StackNavigationViewStyle())
    }
}
struct InitialView_Previews: PreviewProvider {
    static var previews: some View {
        InitialView()
    }
}
struct FindDoktor: View {
    @State var isFirstFlow:Bool = false
    
    @Binding var isNavigationBarHidden: Bool
    @State var mobileNumber = ""
    @State var showDoctorList = false
    @State var showDoctorProfile = false
    @State var showError = false
    @State var isShowing = false
    @State var Phonenumber: String = ""
    @EnvironmentObject var appState: AppState
    
    @ObservedObject var providerListVC = ProviderListObserver()
    @ObservedObject var textBindingManager = TextBindingManager()
    @ObservedObject var userDefaultsManager = UserDefaultsManager()
    var navigation = DoktorUpNavigation()
    var body: some View {
        LoadingView(isShowing: .constant(isShowing)) {
            
            GeometryReader { geometry in
                
                ZStack {
                    Image("FintDocktor_Background")
                        .resizable()
                        .edgesIgnoringSafeArea(.all)
                        .aspectRatio(geometry.size, contentMode: .fit)
                    
                    VStack() {
                        
                        Image("doktorup_find_icon")
                            .renderingMode(.original)
                            .aspectRatio(CGSize(width: 90, height: 90), contentMode: .fit)
                            .padding(.top, 50)
                        
                        
                        
                        Text("Lets’ find your doctor")
                            .font(.system(size: 28))
                            .foregroundColor(kPrimaryRegularColor)
                            .fontWeight(.heavy)
                        
                        Text("Please enter doctor’s phone.")
                            .font(.system(size: 22))
                            .fontWeight(.light)
                            .foregroundColor(kPrimaryRegularColor)
                            .padding(.top, 40)
                        
                        VStack(alignment: .leading, spacing: 5) {
                            TextFieldWithAsInputFormate(placeholder: "000-000-0000", Border: true,fontsize: UIFont.systemFont(ofSize: 20), Alignment: .center, text: self.$Phonenumber)
                                .frame(height:40)
                                .padding(.top, 10)
                                .padding(.horizontal, 15)
                        }
                        Button(action: {
                            self.isShowing.toggle()
                            self.providerListVC.getProvidersByPhone(strPhone: self.Phonenumber.replacingOccurrences(of: "-", with: ""),complitionHandler: { isSuccess in
                                if isSuccess {
                                    if self.providerListVC.arrProviders.count == 1 {
                                        
                                        //                                             if self.userDefaultsManager.providerID == nil {
                                        self.userDefaultsManager.setValueFor(provider: self.providerListVC.arrProviders[0])
                                        //                                             }
                                        if self.isFirstFlow
                                        {
                                            self.showDoctorProfile = true
                                        }else
                                        {
                                            self.appState.reloadDashboard(selectedTab: 0, selectedProvider: self.providerListVC.arrProviders[0] )
                                        }
                                    } else {
                                        self.showDoctorList = true
                                    }
                                    self.isShowing.toggle()
                                } else {
                                    self.showError.toggle()
                                    self.isShowing.toggle()
                                }
                            })
                        }) {
                            if self.Phonenumber.count == 12 {
                                HStack {
                                    Text("Find Doctor")
                                        .font(.system(.headline))
                                        .foregroundColor(Color.white)
                                        .padding(.vertical, 10)
                                }
                                .frame(minWidth: 0, maxWidth: .infinity)
                                .background(kPrimaryRegularColor)
                                .cornerRadius(10)
                            } else {
                                HStack {
                                    Text("Find Doctor")
                                        .font(.system(.headline))
                                        .foregroundColor(Color.white)
                                        .padding(.vertical, 10)
                                }
                                .frame(minWidth: 0, maxWidth: .infinity)
                                .background(kCustomLightGrayDisable)
                                .cornerRadius(10)
                            }
                        }.alert(isPresented: self.$showError) {
                            Alert(title: Text("Something went wrong! \n Please try again later."))
                        }
                        .padding(.top, 10)
                        .disabled(self.Phonenumber.count == 12 ? false:true)
                        Spacer()
                            
                            
                        self.navigation.link(
                            destination: DoctorListingView(
                                isNavigationBarHidden: self.$isNavigationBarHidden,arrProviders: self.providerListVC.arrProviders, isFirstFlow: self.isFirstFlow),
                            action: self.$showDoctorList)
                        self.navigation.link(
                            destination: LoadTabbarViewForProvider(
                                isNavigationBarHidden: self.$isNavigationBarHidden),
                            action: self.$showDoctorProfile)
                    }
                    .padding(.horizontal, 10)
                        
                        
                    .navigationBarTitle("")
                    .navigationBarHidden(self.isNavigationBarHidden)
                    .onAppear {
                        self.isNavigationBarHidden = false
                    }
                    .navigationViewStyle(StackNavigationViewStyle())
                    
                } // This line is supporting iPad, we should not comment out this line
            }
            
            
        }
    }
}
struct LoadTabbarViewForProvider: View {
    @Binding var isNavigationBarHidden: Bool
    @EnvironmentObject var model: NavigationIsActiveModel
    @EnvironmentObject var appState: AppState
    @State var selection = 0
    @State var selectedProvider = Provider()
    var window: UIWindow?
    
    var body: some View {
        ZStack {
            
            
            if Utils.SYSTEM_VERSION_LESS_THAN(version: CustomApp.iOSVersion) {
                ContentView(isNavigationBarHidden: self.$isNavigationBarHidden,
                            selection: self.selection,
                            selectedProvider: self.selectedProvider)
            } else {
                MyTabBarView(isNavigationBarHidden: self.$isNavigationBarHidden,
                             selection: self.selection,
                             selectedProvider: self.selectedProvider)
            }
        }
        .navigationBarBackButtonHidden(true)
        .navigationBarTitle("", displayMode: .inline)
        .navigationBarHidden(self.isNavigationBarHidden)
        .onAppear {
            
            
            self.isNavigationBarHidden = true
        }
    }
}

extension UIApplication {
    func endEditing() {
        sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}

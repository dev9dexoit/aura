//
//  CustomPopupView.swift
//  MyDoktorUp
//
//  Created by Mac on 25/02/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI
import UIKit
import MessageUI

struct CustomPopupView: View {
    
    @Environment(\.presentationMode) var presentationMode
    @State var show = true
    @State var isShowingMailView = false
    @State var result: Result<MFMailComposeResult, Error>? = nil
    @State var mailRecipients: [String]? = ["signup@doktorup.com"]

    var body: some View {
//        ScrollView(.vertical, showsIndicators: false, content: {
            
            VStack(alignment: .leading, spacing: 20) {
                
                HStack {
                    Spacer()
                    Text("Doctor not found!")
                        .font(.system(size: 25))
                        .fontWeight(.heavy)
                        .multilineTextAlignment(.center)
                        .foregroundColor(kPrimaryRegularColor)
                    .padding(.top, 5)
                    Spacer()
                }
                
                HStack {
                    Spacer()
                    Text("Please check with your doctor’s office if they have signed up with DoktorUp")
                        .font(.system(size: 22))
                        .fontWeight(.light)
                        .multilineTextAlignment(.center)
                        .foregroundColor(kPrimaryRegularColor)
                        .padding(.top, 10)
                        .padding(.leading, 5)
                        .padding(.trailing, 5)
                    
                }
                
              Button(action: {
                                 guard let url = URL(string: "http://doktorup.com/") else { return }
                                 UIApplication.shared.open(url)
                             }) {
                                 Text("DoktorUP.com")
                                     .font(.system(.headline))
                                     .padding(.vertical, 10)
                                     .foregroundColor(Color.white)
                                     
                             }
                             .frame(minWidth: 0, maxWidth: .infinity)
                             .background(kPrimaryRegularColor)
                             .cornerRadius(10)
                             
                             
                             Button(action: {
                                 self.isShowingMailView = true
                             }) {
                                 Text("Send request to sign up")
                                 .font(.system(.headline))
                                 .padding(.vertical, 10)
                                 .foregroundColor(kCustomGrayColor)
                                 .frame(minWidth: 0, maxWidth: .infinity)
                                 .overlay(
                                         RoundedRectangle(cornerRadius: 10)
                                             .strokeBorder(style: StrokeStyle(lineWidth: 1))
                                             .foregroundColor(kCustomGrayColor)
                                             .cornerRadius(10)
                                 )
                             }
                             .disabled(!MFMailComposeViewController.canSendMail())
                             .sheet(isPresented: $isShowingMailView) {
//                                FullScreenMailView()
                                
                                MailView(result: self.$result, mailSubject: "New doctor sign up request", mailRecipients: self.mailRecipients)

                                
//                                 MailView(result: self.$result)
                             }
                             
                             Button(action: {
                                 self.presentationMode.wrappedValue.dismiss()
                             }) {
                                 Text("Cancel")
                                 .font(.system(.headline))
                                 .padding(.vertical, 10)
                                 .foregroundColor(kCustomGrayColor)
                                 .frame(minWidth: 0, maxWidth: .infinity)
                                 .overlay(
                                         RoundedRectangle(cornerRadius: 10)
                                             .strokeBorder(style: StrokeStyle(lineWidth: 1))
                                             .foregroundColor(kCustomGrayColor)
                                             .cornerRadius(10)
                                 )
                             }
                
                Spacer()
                             
                
            }.padding()
            
//        })
    }
}

struct CustomPopupView_Previews: PreviewProvider {
    static var previews: some View {
        CustomPopupView()
    }
}

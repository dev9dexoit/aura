//
//  DoctorListingView.swift
//  MyDoktorUp
//
//  Created by Mac on 12/02/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI

struct DoctorListingView: View {
    @Binding var isNavigationBarHidden: Bool
    @Environment(\.presentationMode) var presentationMode
    @State var arrProviders = [Provider]()
    @State var isActiveSingle = false
    @EnvironmentObject var appState: AppState
    @State  var isFirstFlow:Bool = true
    @State var isProfileDetails = false
    var body: some View {
        ZStack {
            if self.arrProviders.count == 0 {
                CustomPopupView()
            }
            else if self.arrProviders.count == 1 {
                List(arrProviders) { provider in
                    
                    
                    Button(action: {
                        if self.isFirstFlow
                        {
                            self.isActiveSingle  = true
                        }else
                        {
                            self.appState.reloadDashboard(selectedTab: 0, selectedProvider: provider)
                        }
                        
                    }) {
                        VStack(alignment: .center, spacing: 10, content: {
                                                  DoctorCellView(providerObj: provider, isLiked: false)
                                                      .background(Color.white)
                                                      .clipped()
                                                      .shadow(color: kCustomGrayColor, radius: 2, x: 0, y: 2 )
                                              }).padding(.leading,8).padding(.trailing,8).padding(.bottom,12).padding(.top,0)
                    }
                    
                    NavigationLink(destination: LoadTabbarViewForProvider(
                        isNavigationBarHidden: self.$isNavigationBarHidden,
                        selectedProvider: provider),
                                   isActive: self.$isActiveSingle) {
                      EmptyView()
                    }.isDetailLink(false)
                }
            } else {
                VStack{
                    
                    Text("Please select the doctor to change the doctor in your user profile.")
                        .font(.system(.body))
                        .fontWeight(.regular)
                        .foregroundColor(kCustomGrayColor)
                        .multilineTextAlignment(.center)
                        .padding(.leading, 20)
                        .padding(.vertical, 10)
                        .padding(.trailing, 20)
                        .padding(.top, 10)
                    
                
                    ScrollView(.vertical, showsIndicators: false, content: {
                        ForEach(arrProviders) { provider in
                            

                            Button(action: {
                                self.appState.reloadDashboard(selectedTab: 0, selectedProvider: provider)
                            }) {
                                VStack(alignment: .center, spacing: 10, content: {
                                    DoctorCellView(providerObj: provider, isLiked: false)
                                      
                                        .background(Color.white)
                                        .clipped()
                                        .shadow(color: kCustomGrayColor, radius: 2, x: 0, y: 2 )
                                }).padding(.leading,8).padding(.trailing,8).padding(.bottom,12).padding(.top,0)

                            }
                                                    }
                    }).buttonStyle(BorderlessButtonStyle())
                 }
                .background(kCustomLightBlueColor)
                
            }
            
        } .onAppear {
            UITableView.appearance().separatorStyle = .none
            UITableViewCell.appearance().accessoryType = .none
            self.isNavigationBarHidden = false
        }
            
            
        .navigationBarTitle(
            Text("Select Your Doctor")
                .fontWeight(.regular)
                .font(.title)
                .foregroundColor(.white),
            displayMode: .inline
        )
            .navigationBarHidden(self.isNavigationBarHidden)
            .navigationBarBackButtonHidden(true)
            .navigationBarItems(leading:
                HStack {
                    Button(action: {
                        
                        self.presentationMode.wrappedValue.dismiss()
                    }) {
                        Image("ic_back")
                            .foregroundColor(Color.white)
                    }
                    .padding(.leading, -8)
            })
    }
}

struct DoctorListingView_Previews: PreviewProvider {
    static var previews: some View {
        DoctorListingView(isNavigationBarHidden: .constant(false))
    }
}


struct DoctorCellView : View {
    var providerObj = Provider()
    @State var isLiked = false
    @ObservedObject var userDefaultsManager = UserDefaultsManager()
    
    var body: some View {
        
        VStack {
            HStack() {
                VStack() {
                    Image("doctor_profile")
                        .renderingMode(.original)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .clipped()
                        .background(kCustomLightGrayPhotoBg)

                }.frame(width: 80).padding(.leading, 10)
                
                VStack(alignment: .leading, spacing: 5) {
                    Spacer()
                    
                    Text(Utils.getProviderFullname(self.providerObj))
                        .font(.headline)
                        .fontWeight(.bold)
                        .foregroundColor(kPrimaryRegularColor)
                        .multilineTextAlignment(.leading)
                        .lineLimit(nil)
                    
                        
                    
                    Text("\(self.providerObj.taxonomy_name ?? "")").foregroundColor(Color.black).font(.headline).fontWeight(.bold).lineLimit(nil)
                        
                    Text("\(self.providerObj.address1?.capitalized ?? ""), \(self.providerObj.city?.capitalized ?? "" ), \(self.providerObj.state?.uppercased() ?? "")" ).foregroundColor(kCustomGrayColor).font(.callout).fontWeight(.regular)
                        .lineLimit(nil)
                    
                    if self.providerObj.distance != "" || self.providerObj.distance != nil
                    {
                        Text("\(self.providerObj.distance ?? "") Miles").foregroundColor(kCustomDarkGrayColor).font(.callout).fontWeight(.semibold).lineLimit(nil)
                            
                    }
                    
                    
                    
                    Text("\(self.providerObj.practice_phone?.formatePhoneNumber() ?? "")")
                        
                        
                        .foregroundColor(Color.gray.opacity(90)).font(.body).fontWeight(.semibold).lineLimit(nil)
                    Spacer()
                }
                Spacer()
                if self.userDefaultsManager.providerID == self.providerObj.id {
                    VStack(alignment: .leading, spacing: 0) {
                        Spacer()
                        Image("Orange-checkMark")
                            .renderingMode(.original)
                            .resizable()
                            .padding(.trailing,5)
                            .frame(width:35,height: 30)
                        Spacer()
                    }
                    .padding()
                }
                Spacer()
                
            }
        }
        
    }
}

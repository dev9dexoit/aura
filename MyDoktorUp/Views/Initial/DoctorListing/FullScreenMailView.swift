//
//  FullScreenMailView.swift
//  MyDoktorUp
//
//  Created by RMV on 25/06/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import SwiftUI
import MessageUI

struct FullScreenMailView: View {
    
    @Environment(\.presentationMode) var presentationMode
    @State var result: Result<MFMailComposeResult, Error>? = nil

    var body: some View {
        
        ZStack {
            
            VStack(alignment: .leading, spacing: 10) {
               HStack {
                    Button(action: {
                                      self.presentationMode.wrappedValue.dismiss()
                                  }) {
                                      Text("Cancel")
                                          .font(.system(.headline))
                                          .foregroundColor(kPrimaryRegularColor)
                                  }.padding()
                    
                    Spacer()
                   
                }
                 MailView(result: self.$result)
            }
        }
    }
   
}


struct FullScreenMailView_Previews: PreviewProvider {
    static var previews: some View {
        FullScreenMailView()
    }
}


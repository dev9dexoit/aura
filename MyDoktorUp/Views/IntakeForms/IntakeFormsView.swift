//
//  IntakeFormsView.swift
//  MyDoktorUp
//
//  Created by Mac on 05/02/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI

struct IntakeFormsView: View {
    
    @ObservedObject var intakeFormObserver = IntakeFormObserver()
    @Environment(\.presentationMode) var presentationMode

    var body: some View {
//
//        NavigationView {
            ZStack {
                
                Group() {
                    if self.intakeFormObserver.intakeForm != nil {
                        
                        List(self.intakeFormObserver.intakeForm!.providerformdata) { formData in
                            NavigationLink(destination: FormGeneraicView(formData: formData)) {
                                Text(formData.formdescription)
                            }
                        }
                    } else {
                        Text("Please wait..")
                    }
                }.onAppear {
                    self.intakeFormObserver.parceJsonData()
                }
                
            }
            .navigationBarTitle("Step 3 of 3", displayMode: .inline)
            .navigationBarItems(leading: HStack{
                Button(action: {
                    self.presentationMode.wrappedValue.dismiss()
                }){Image("ic_back")}
            })
            
//        } 
            .navigationViewStyle(StackNavigationViewStyle()) // This line is supporting iPad, we should not comment out this line this line
    }
}

struct IntakeFormsView_Previews: PreviewProvider {
    static var previews: some View {
        IntakeFormsView()
    }
}

//
//  IntakeMedicalFormView.swift
//  MyDoktorUp
//
//  Created by RMV on 02/07/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI

struct IntakeMedicalFormView: View {
    @ObservedObject var accountInformationObserver = AccountInformationObserver()
    
    @ObservedObject var observerChekingAppointment = ChekingAppointment_Observer()
    @Environment(\.presentationMode) var presentationMode
    @Binding var rootIsActive : Bool
    @State var showCheckInCompleteView = false
    @State var isPaymentEnabled = false
    @State var isSignAllForm = false
    @State var Selectedmodel = AppointmentModel()
    @State var isPaymentTap = false
    @State var isSigUpAllFormTap = false
    let dateformate = DateFormatter()
    @State var isShowing = false
    @State var isNext = false
    @State var showErrorMessage: String = ""
    @State var showingAlert = false
    var body: some View {
        LoadingView(isShowing: .constant(isShowing)) {
            VStack {
                VStack {
                    Form{
                        Section(header: Text("Medical forms").foregroundColor(kPrimaryRegularColor).fontWeight(.thin).font(.system(.title)).padding(.top,15).padding(.bottom,20)) {
                            
                            NavigationLink(destination: FinancialPolicyView(title: "Financial Policy", formNumber: 1,PhoneNumber:self.Selectedmodel.patient_phone ?? "",id:self.Selectedmodel.appointment_id ?? "")) {
                                
                                OptionRow(title: "Financial Policy")
                                
                            }
                            
                            NavigationLink(destination: HIPAAAcknowledgementView(title: "HIPAA Acknowledgement", formNumber: 2,PhoneNumber:self.Selectedmodel.patient_phone ?? "",id:self.Selectedmodel.appointment_id ?? "")) {
                                OptionRow(title: "HIPAA Acknowledgement")
                                
                            }
                            
                            
                            NavigationLink(destination: HIPPAAuthorizationView()) {
                                OptionRow(title: "HIPPA Authorization")
                                
                            }
                            
                            
                            NavigationLink(destination: AssignmentBenefitView(title: "Assignment of Benefits", formNumber: 3,PhoneNumber:self.Selectedmodel.patient_phone ?? "",id:self.Selectedmodel.appointment_id ?? "")) {
                                OptionRow(title: "Assignment of Benefits")
                                
                            }
                            
                            
                            NavigationLink(destination: NoticeOfPrivacyFormView(title: "Notice of Privacy Form", formNumber: 4,PhoneNumber:self.Selectedmodel.patient_phone ?? "",id:self.Selectedmodel.appointment_id ?? "")) {
                                OptionRow(title: "Notice of Privacy Form")
                                
                            }
                            
                            NavigationLink(destination: NoticeOfPrivacySignatureFormView(title: "Notice of Privacy signature Form", formNumber: 5,PhoneNumber:self.Selectedmodel.patient_phone ?? "",id:self.Selectedmodel.appointment_id ?? "")) {
                                OptionRow(title: "Notice of Privacy signature Form")
                                
                            }
                            
                            
                            
                        }.foregroundColor(kCustomGrayColor).padding(.bottom, 5)
                            .listRowBackground(kCustomLightBlueColor)
                    }
                    
                    Spacer()
                    if self.isSignAllForm {
                        Text("You signed on \(self.getCheingkingAllSign(date: self.Selectedmodel.checkin_consent ?? ""))")
                            .foregroundColor(kCustomDarkGrayColor)
                            .font(Font.system(size: 22))
                            .fontWeight(.regular)
                            .padding(.top, 5)
                            .padding(.bottom, 15)
                    }
                    
                    if !self.isSigUpAllFormTap {
                        Button(action: {
                            let selectePhonenumber = self.Selectedmodel.patient_phone
                            MedicalFormJsonObject = NSArray()
                            MedicalFormJsonObject.adding(SetModelObject(Number: selectePhonenumber ?? "", FormName: "Financial Policy", formNumber: 1))
                            MedicalFormJsonObject.adding(SetModelObject(Number: selectePhonenumber ?? "", FormName: "HIPAA Acknowledgement", formNumber: 2))
                            MedicalFormJsonObject.adding(SetModelObject(Number: selectePhonenumber ?? "", FormName: "Assignment of Benefits", formNumber: 3))
                            MedicalFormJsonObject.adding(SetModelObject(Number: selectePhonenumber ?? "", FormName: "Notice of Privacy Form", formNumber: 4))
                            MedicalFormJsonObject.adding(SetModelObject(Number: selectePhonenumber ?? "", FormName: "Notice of Privacy signature Form", formNumber: 5))
                            
                            var dictToSend: [String: Any] = [:]
                            dictToSend = ["practice_attributes": ["user_consent_form":["consent":MedicalFormJsonObject]]]
                            self.isShowing = true
                            
                            self.accountInformationObserver.updateAccountInfo(infoToSend: dictToSend, complitionHandler: { (status, meessage) in
                                if status {
                                    self.isShowing = false
                                    
                                    self.showErrorMessage = meessage
                                    self.showingAlert.toggle()
                                    self.presentationMode.wrappedValue.dismiss()
                                } else {
                                    self.isShowing = false
                                    
                                    self.showErrorMessage = meessage
                                    self.showingAlert.toggle()
                                }
                            })
                            
                            
                        }) {
                            HStack {
                                Text("Sign all forms")
                                    .font(.system(.body))
                                    .fontWeight(.medium)
                                    .foregroundColor(Color.white)
                                    .padding(.vertical, 15)
                            }
                            .frame(minWidth: 0, maxWidth: .infinity)
                            .background(kPrimaryRegularColor)
                            .cornerRadius(10)
                        }.padding(.bottom, 15)
                    } else {
                        Button(action: {
                            
                            
                        }) {
                            HStack {
                                Text("Sign all forms")
                                    .font(.system(.body))
                                    .fontWeight(.medium)
                                    .foregroundColor(Color.white)
                                    .padding(.vertical, 15)
                            }
                            .frame(minWidth: 0, maxWidth: .infinity)
                            .background(kCustomLightGrayDisable)
                            .cornerRadius(10)
                        }.padding(.bottom, 15)
                    }
                    
                    
                    
                }.padding(.horizontal, 20).padding(.bottom, 20).background(kCustomLightBlueColor)
                    .navigationBarTitle("Step 2 of 3", displayMode: .inline)
                    .navigationBarItems(leading: HStack{
                        Button(action: {
                            self.presentationMode.wrappedValue.dismiss()
                        }){Image("ic_back")}
                        }, trailing:
                        
                        
                        NavigationLink(destination: IntakeFormsView(), isActive: self.$isNext){
                            HStack {
                                Button(action: {
                                    self.isNext = true
                                    
                                }) {
                                    Text("Next").foregroundColor(.white)
                                }
                            }
                        }
                        
                )
                    
                    .onAppear() {
                        if self.Selectedmodel.checkin_consent != nil && self.Selectedmodel.checkin_consent !=  "" {
                            self.isSignAllForm = true
                            self.isPaymentEnabled = true
                        } else {
                            self.isPaymentEnabled = false
                            self.isSignAllForm = false
                        }
                }
            }
        }
    }
    
    func getCheingkingAllSign(date:String) -> String {
        self.dateformate.dateFormat = "MM/dd/YYYY HH:mm:SS"
        
        if date == "" {
            return dateformate.string(from: Date())
        } else {
            let chekingDate = dateformate.date(from: date)
            return dateformate.string(from: chekingDate ?? Date())
        }
    }
    
}

struct IntakeMedicalFormView_Previews: PreviewProvider {
    static var previews: some View {
        IntakeMedicalFormView(rootIsActive: .constant(false))
    }
}

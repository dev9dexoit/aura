//
//  IntakePersonalInfoView.swift
//  MyDoktorUp
//
//  Created by RMV on 02/07/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI

struct IntakePersonalInfoView: View {
    
    
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject var accountInformationObserver = AccountInformationObserver()

    @ObservedObject var GetUserData = UserInformationObserver()
    @ObservedObject var observedVisitReasons = VisitReasonsObserver()
    @Binding var rootIsActive: Bool
    @ObservedObject var observedInsurance = InsuranceCarrierObserver()
    @ObservedObject var observerChekingAppointment = ChekingAppointment_Observer()
    @ObservedObject var observedInsuranceSelection = InsuranceSelectionObserver()
    @State var insuranceNames: [String] = []
    
    @State var isActiveInsurance = false
    
    
    @State var fullName: String = ""
    @State var FirstName: String = ""
    @State var LastName: String = ""
    @State private var birthDate = Date()
    
    @State var selectedGender = 0
    @State var emailAddress: String = ""
    @State var Gender = ""
    
    
    @State var address1: String = ""
    @State var address2: String = ""
    
    @State var city: String = ""
    @State var State: String = ""
    @State var Zip: String = ""
    
    @State var Selectedmodel = AppointmentModel()
    @State var SelectedId = ""
    var arrGender = ["Male", "Female", "Others"]
    
    var arrMaritalStatus = ["Married", "Unmarried","Divorced"]
    var arrLanguage = [
        "English",
        "Latin",
        "French",
        "Spanish",
        "Hindi",
        "Mandarin Chinese",
        "Arabic"
    ]
    
    var arrRace = [
        "Bushmen",
        "Negroes",
        "Negritoes",
        "Melanochroi",
        "Australoids",
        "Xanthochroi",
        "Polynesians",
        "Mongoloids A",
        "Mongoloids B",
        "Mongoloids C",
        "Esquimaux"
    ]
    
    var arrEthnicity = [
        "African Americans",
        "American Indian group",
        "Jewish people",
        "Americans",
        "Asian people"
    ]
    
    @State var selectedMaritalStatus = 0
    @State var selectedLanguage = 0
    @State var selectedRace = 0
    @State var selectedEthnicity = 0
    @State var isShowing = true
    
    @State var isNext = false
    @State var isChekingInfo = false
    //    @ObservedObject var phoneNo = TextBindingManager()
    @State var Phonenumber = ""
    
    @State var InsuranceImage1:UIImage = UIImage(named: "ic_camera")!
    @State var InsuranceImage2:UIImage = UIImage(named:"ic_camera")!
    @State var DrivingImage1:UIImage = UIImage(named:"ic_camera")!
    @State var DrivingImage2:UIImage = UIImage(named:"ic_camera")!
    @State var showErrorMessage: String = ""
    @State var showingAlert = false

    
    var dateFormatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        return formatter
    }
    
    @State var IsInsuranceImage1:Bool = false
    @State var IsInsuranceImage2:Bool = false
    @State var IsDrivingImage1:Bool = false
    @State var IsDrivingImage2:Bool = false
    @State var isDisappar = false
    
    var body: some View {
        LoadingView(isShowing: .constant(isShowing)) {
            NavigationView {
                ZStack {
                    Form {
                        Section(header: VStack(alignment: .leading, spacing: 0){
                            HStack(alignment: .center, spacing: 0){
                                Spacer()
                                Group {
                                    Image("ic_user_placeholder")
                                        .resizable()
                                        .scaledToFit()
                                        .frame(height: 50)
                                        .clipped()
                                        .listRowInsets(EdgeInsets())
                                        .background(kCustomLightBlueColor)
                                    
                                }
                                .frame(width:120,height:120)
                                    
                                .overlay(
                                    RoundedRectangle(cornerRadius: 120/2)
                                        .strokeBorder(style: StrokeStyle(lineWidth: 1))
                                        .foregroundColor(kCustomGrayColor)
                                )
                                
                                Spacer()
                            }
                            
                            Text("Personal information").foregroundColor(kPrimaryRegularColor).fontWeight(.thin).font(.system(.title)).padding(.top, 10)
                            
                        }.padding(.top, 30)
                        ) {
                            
                            TextField("* First Name", text: self.$FirstName)
                            
                            TextField("* Last Name", text: self.$LastName)
                            
                            TextFieldWithAsInputFormate(placeholder: "* Phone", Border: false,fontsize: UIFont.systemFont(ofSize: 18), Alignment: .left, text: self.$Phonenumber).onAppear {
                                
                            }
                            
                            
                            Picker(selection: self.$selectedGender, label: Text("* Gender")) {
                                ForEach(0 ..< self.arrGender.count) {
                                    Text(self.arrGender[$0])
                                }
                            }
                            
                            DatePicker(selection: self.$birthDate, in: ...Date(), displayedComponents: .date) {
                                HStack {
                                    Text("* Date of Birth")
                                    //                        Spacer()
                                    //                        Text("\(birthDate, formatter: dateFormatter)")
                                }
                                
                            }
                            
                            TextField("* Email", text: self.$emailAddress).keyboardType(.emailAddress)
                        }.padding(.vertical, 10)
                            .foregroundColor(kCustomGrayColor)
                        
                        Section(header: Text("Address").foregroundColor(kPrimaryRegularColor).fontWeight(.thin).font(.system(.title))) {
                            TextField("Address 1", text: self.$address1)
                            TextField("Address 2", text: self.$address2)
                            
                            TextField("City", text: self.$city)
                            TextField("State", text: self.$State)
                            TextField("Zip", text: self.$Zip)
                            
                            
                        }.padding(.vertical, 10)
                            .foregroundColor(kCustomGrayColor)
                        
                        Section(header: Text("Insurance").foregroundColor(kPrimaryRegularColor).fontWeight(.thin).font(.system(.title))) {
                            HStack(alignment: .center, spacing: 10){
                                NavigationLink(destination: InsuranceListView(insuranceNames: self.insuranceNames, insuranceDelegate: self), isActive: self.$isActiveInsurance) {
                                    Button("Insurance") {
                                        self.isActiveInsurance.toggle()
                                    }
                                    Spacer()
                                    Text(self.observedInsuranceSelection.insuranceName)
                                }
                            }
                        }
                        .padding(.vertical, 10)
                        Section(header: Text("Insurance card").foregroundColor(kPrimaryRegularColor).fontWeight(.thin).font(.system(.title))) {
                            
                            HStack(alignment: .top, spacing: 15, content: {
                                VStack (alignment: .leading, spacing: 10) {
                                    Text("Front side of card")
                                    
                                    GeometryReader { geometry in
                                        
                                        Image(uiImage: self.InsuranceImage1)
                                            .renderingMode(.original)
                                            .scaledToFill()
                                            .frame(height:100)
                                            .clipped()
                                            .listRowInsets(EdgeInsets())
                                            .frame(width:geometry.size.width,height:geometry.size.height)
                                            .overlay(
                                                RoundedRectangle(cornerRadius: 10)
                                                    .strokeBorder(style: StrokeStyle(lineWidth: 1))
                                                    .foregroundColor(kCustomGrayColor)
                                                    .cornerRadius(10)
                                        )
                                            .onTapGesture {
                                                if !Platform.isSimulator {
                                                    self.IsInsuranceImage1 = true
                                                }
                                        }
                                        .sheet(isPresented: self.$IsInsuranceImage1) {
                                            
                                            ScannerView(Title: "Insurance Front side of card", completion: { image in
                                                self.InsuranceImage1 = image
                                            })
                                        }
                                        
                                    }.frame(height:100)
                                    
                                }
                                
                                VStack (alignment: .leading, spacing: 10) {
                                    Text("Back side of card")
                                    
                                    GeometryReader { geometry in
                                        Button(action: {
                                        }) {
                                            
                                            Image(uiImage: self.InsuranceImage2)
                                                .renderingMode(.original)
                                                .scaledToFill()
                                                .frame(height:100)
                                                .clipped()
                                                .listRowInsets(EdgeInsets())
                                                .frame(width:geometry.size.width,height:geometry.size.height)
                                                .overlay(
                                                    RoundedRectangle(cornerRadius: 10)
                                                        .strokeBorder(style: StrokeStyle(lineWidth: 1))
                                                        .foregroundColor(kCustomGrayColor)
                                                        .cornerRadius(10)
                                            )
                                            
                                        }
                                        .onTapGesture {
                                            if !Platform.isSimulator {
                                                self.IsInsuranceImage2 = true
                                            }
                                        }
                                        .sheet(isPresented: self.$IsInsuranceImage2) {
                                            ScannerView(Title: "Insurance Back side of card", completion: { image in
                                                self.InsuranceImage2 = image
                                            })
                                        }
                                    }.frame(height:100)
                                    
                                }
                            })
                            
                        }.padding(.vertical, 0)
                        
                        
                        Section(header: Text("Drivers license").foregroundColor(kPrimaryRegularColor).fontWeight(.thin).font(.system(.title))) {
                            
                            HStack(alignment: .top, spacing: 15, content: {
                                VStack (alignment: .leading, spacing: 10) {
                                    Text("Front side of card")
                                    
                                    GeometryReader { geometry in
                                        Button(action: {
                                        }) {
                                            
                                            Image(uiImage: self.DrivingImage1)
                                                .renderingMode(.original)
                                                .scaledToFill()
                                                .frame(height:100)
                                                .clipped()
                                                .listRowInsets(EdgeInsets())
                                                .frame(width:geometry.size.width,height:geometry.size.height)
                                                .overlay(
                                                    RoundedRectangle(cornerRadius: 10)
                                                        .strokeBorder(style: StrokeStyle(lineWidth: 1))
                                                        .foregroundColor(kCustomGrayColor)
                                                        .cornerRadius(10)
                                            )
                                        }
                                        .onTapGesture {
                                            if !Platform.isSimulator {
                                                self.IsDrivingImage1 = true
                                            }
                                        }
                                            
                                        .sheet(isPresented: self.$IsDrivingImage1) {
                                            ScannerView(Title: "Drivers license Front side of card", completion: { image in
                                                self.DrivingImage1 = image
                                            })
                                        }
                                    }.frame(height:100)
                                }
                                
                                VStack (alignment: .leading, spacing: 10) {
                                    Text("Back side of card")
                                    
                                    GeometryReader { geometry in
                                        Button(action: {
                                            
                                        }) {
                                            
                                            Image(uiImage: self.DrivingImage2)
                                                .renderingMode(.original)
                                                .scaledToFill()
                                                .frame(height:100)
                                                .clipped()
                                                .listRowInsets(EdgeInsets())
                                                .frame(width:geometry.size.width,height:geometry.size.height)
                                                .overlay(
                                                    RoundedRectangle(cornerRadius: 10)
                                                        .strokeBorder(style: StrokeStyle(lineWidth: 1))
                                                        .foregroundColor(kCustomGrayColor)
                                                        .cornerRadius(10)
                                            )
                                            
                                        }
                                        .onTapGesture {
                                            if !Platform.isSimulator {
                                                self.IsDrivingImage2 = true
                                            }
                                        }
                                        .sheet(isPresented: self.$IsDrivingImage2) {
                                            ScannerView(Title: "Drivers license Back side of card ", completion: { image in
                                                self.DrivingImage2 = image
                                            })
                                        }
                                    }.frame(height:100)
                                }
                                
                            })
                            
                            Picker(selection: self.$selectedMaritalStatus, label: Text("Marital Status")) {
                                ForEach(0 ..< self.arrMaritalStatus.count) {
                                    Text(self.arrMaritalStatus[$0]).padding(.vertical, 10)
                                }
                            }
                            
                            Picker(selection: self.$selectedLanguage, label: Text("Language")) {
                                ForEach(0 ..< self.arrLanguage.count) {
                                    Text(self.arrLanguage[$0]).padding(.vertical, 10)
                                }
                            }
                            
                            Picker(selection: self.$selectedRace, label: Text("Race")) {
                                ForEach(0 ..< self.arrRace.count) {
                                    Text(self.arrRace[$0]).padding(.vertical, 10)
                                }
                            }
                            
                            Picker(selection: self.$selectedEthnicity, label: Text("Ethnicity")) {
                                ForEach(0 ..< self.arrEthnicity.count) {
                                    Text(self.arrEthnicity[$0]).padding(.vertical, 10)
                                }
                            }
                            
                        }.padding(.vertical, 0)
                            .foregroundColor(kCustomGrayColor)
                        
                    }.padding(.vertical, -34)
                        .foregroundColor(kCustomGrayColor)
                        .navigationBarTitle("Step 1 of 3", displayMode: .inline)
                        .navigationBarItems(trailing:
                            
                            NavigationLink(destination: IntakeMedicalFormView(rootIsActive: self.$rootIsActive,Selectedmodel: self.Selectedmodel), isActive: self.$isNext){
                                HStack {
                                    Button(action: {
                                      //  self.isNext = true
                                          if !self.validateFields() {
                                            self.Updateuserprofile()
                                        }
                                    }) {
                                        Text("Next").foregroundColor(.white)
                                    }
                                    .alert(isPresented: self.$showingAlert) {
                                                              Alert(title: Text("Error"), message: Text(self.showErrorMessage), dismissButton: .default(Text("OK")))
                                                              
                                                      }
                                }
                            }
                    )
                        .background(kCustomLightBlueColor)
                    
                }.background(kCustomLightBlueColor)
            }
            .onDisappear{
                self.isDisappar = true
            }
            .onAppear {
                UITableView.appearance().backgroundColor = .clear
                UITableView.appearance().separatorStyle = .singleLine
                
                if !self.isDisappar
                {
                    self.GetUserData.getUser(strEmail: UserDefaultsManager().userEmail ?? "" ) { (status,message) in}
                    
                    self.FirstName = "\(UserDefaultsManager().userFullName?.split(separator: " ")[0] ?? "")"
                    self.LastName = "\(UserDefaultsManager().userFullName?.split(separator: " ")[1] ?? "")"
                    
                    self.emailAddress = UserDefaultsManager().userEmail ?? ""
                    self.Phonenumber = UserDefaultsManager().userPhoneNumber?.formatePhoneNumber() ?? ""
                    self.address1 = UserDefaultsManager().useraddress1 ?? ""
                    self.address2 = UserDefaultsManager().useraddress2 ?? ""
                    
                    self.city = ""
                    self.State = ""
                    self.Zip = ""
                    if UserDefaultsManager().usercity?.count ?? 0 > 0
                    {
                        self.city = UserDefaultsManager().usercity ?? ""
                    }
                    if UserDefaultsManager().userstate?.count ?? 0 > 0
                    {
                        self.State = UserDefaultsManager().userstate ?? ""
                    }
                    if UserDefaultsManager().userzip?.count ?? 0 > 0
                    {
                        self.Zip  = UserDefaultsManager().userzip ?? ""
                    }
                    self.birthDate = self.dateFormatter.date(from: UserDefaultsManager().userdob ?? "\(Date())") ?? Date()
                    
                    for i in 0 ..< self.arrGender.count
                    {
                        if self.arrGender[i].lowercased() == UserDefaultsManager().user_gender
                        {
                            self.selectedGender = i
                            break
                        }
                    }
                    
                    
                    for i in 0 ..< self.arrMaritalStatus.count
                    {
                        if self.arrMaritalStatus[i].lowercased() == UserDefaultsManager().user_martial
                        {
                            self.selectedMaritalStatus = i
                            break
                        }
                    }
                    
                    
                    for i in 0 ..< self.arrLanguage.count
                    {
                        if self.arrLanguage[i].lowercased() == UserDefaultsManager().user_language
                        {
                            self.selectedLanguage = i
                            break
                        }
                    }
                    for i in 0 ..< self.arrRace.count
                    {
                        if self.arrRace[i].lowercased() == UserDefaultsManager().user_race
                        {
                            self.selectedRace = i
                            break
                        }
                    }
                    
                    for i in 0 ..< self.arrEthnicity.count
                    {
                        if self.arrEthnicity[i].lowercased() == UserDefaultsManager().user_ethnicity
                        {
                            self.selectedEthnicity = i
                            break
                        }
                    }
                       
                }
                else  if !(UserDefaultsManager().isSign ?? false) && !self.isDisappar
                {
                    self.FirstName = ""
                    self.LastName = ""
                    
                    self.emailAddress = ""
                    self.Phonenumber = ""
                    self.address1 = ""
                    self.address2 = ""
                    self.city = ""
                    self.State = ""
                    self.Zip = ""
                    self.birthDate = Date()
                    self.selectedGender = 0
                }
                
                if self.observedInsurance.arrInsuranceList.count == 0 {
                    self.isShowing = true
                }
                self.observedInsurance.getInsuranceList(searchText:"",isCommonAPI: false, complitionHandler: { (isSuccess) in
                    if isSuccess {
                        self.isShowing = false
                        self.insuranceNames.removeAll()
                        for case let insurance as InsuranceCarrier in self.observedInsurance.arrInsuranceList {
                            self.insuranceNames.append(insurance.name ?? "")
                        }
                        for i in self.insuranceNames
                                        {
                                            
                                            if i.lowercased() == UserDefaultsManager().user_insurance?.lowercased()
                                            {
                                                self.observedInsuranceSelection.insuranceName = i
                                                break
                                            }
                                        }
                    }
                    
                })
                self.observedVisitReasons.getVisitResoons(isCommonAPI: false, isNewPatient: false)
                
            }
        }
    }
 private func Updateuserprofile() {
    var special_attributes = [String:String]()
    var profile_attributes =  [String:String]()
    let firstname = UserDefaultsManager().userFullName?.split(separator: " ")[0]
    let lastname = UserDefaultsManager().userFullName?.split(separator: " ")[1]
    let dobString = self.birthDate.getCurrentDateAsFormattedString()

    if firstname?.lowercased() != self.FirstName.lowercased()
    {
        profile_attributes.updateValue(self.FirstName, forKey: "first_name")
    }
    if lastname?.lowercased() != self.LastName.lowercased()
    {
           profile_attributes.updateValue(self.LastName, forKey: "last_name")
    }
    if UserDefaultsManager().useraddress1?.lowercased() != self.address1.lowercased()
    {
             profile_attributes.updateValue(self.address1, forKey: "address1")
    }
    if UserDefaultsManager().useraddress2?.lowercased() != self.address2.lowercased()
    {
             profile_attributes.updateValue(self.address1, forKey: "address2")
    }
    if UserDefaultsManager().userdob != dobString
       {
                profile_attributes.updateValue(dobString, forKey: "dob")
       }
    if UserDefaultsManager().userPhoneNumber != self.Phonenumber.replacingOccurrences(of: "-", with: "")
    {
             profile_attributes.updateValue(self.Phonenumber.replacingOccurrences(of: "-", with: ""), forKey: "phone")
    }
    if UserDefaultsManager().usercity != self.city
       {
                profile_attributes.updateValue(self.city, forKey: "city")
       }
    if UserDefaultsManager().userstate != self.State
       {
                profile_attributes.updateValue(self.State, forKey: "state")
       }
    if UserDefaultsManager().userzip != self.Zip
       {
                profile_attributes.updateValue(self.Zip, forKey: "zip")
       }
   
    if UserDefaultsManager().user_gender != self.Gender
    {
             special_attributes.updateValue(self.Gender, forKey: "gender")
    }
    if UserDefaultsManager().user_martial?.lowercased() != self.arrMaritalStatus[self.selectedMaritalStatus].lowercased()
    {
             special_attributes.updateValue(self.arrMaritalStatus[self.selectedMaritalStatus], forKey: "marital_status")
    }
    if UserDefaultsManager().user_language?.lowercased() != self.arrLanguage[self.selectedLanguage].lowercased()
      {
               special_attributes.updateValue(self.arrLanguage[self.selectedLanguage], forKey: "language")
      }
    if UserDefaultsManager().user_race?.lowercased() != self.arrRace[self.selectedRace].lowercased()
      {
               special_attributes.updateValue(self.arrRace[self.selectedRace].lowercased(), forKey: "race")
      }
    if UserDefaultsManager().user_ethnicity?.lowercased() != self.arrEthnicity[self.selectedEthnicity].lowercased()
      {
               special_attributes.updateValue(self.arrEthnicity[self.selectedEthnicity].lowercased(), forKey: "ethnicity")
      }
    
    
    var dictToSend: [String: Any] = [:]
    dictToSend = ["profile_attributes": profile_attributes,
                  "special_attributes":special_attributes]
  
        self.isShowing = true

    self.accountInformationObserver.updateAccountInfo(infoToSend: dictToSend, complitionHandler: { (status, meessage) in
        if status {
            self.isShowing = false
            self.GetUserData.getUser(strEmail: self.emailAddress ) { (status,message) in
             }
            self.showErrorMessage = meessage
            self.showingAlert.toggle()
            self.presentationMode.wrappedValue.dismiss()
        } else {
            //Failure
            self.showErrorMessage = meessage
            self.showingAlert.toggle()
        }
    })
 }
    
    func validateFields() -> Bool {
        if self.FirstName == "" {
                   self.showErrorMessage = "Please enter first name"
                   self.showingAlert.toggle()
                   return true
               }
        if self.LastName == "" {
            self.showErrorMessage = "Please enter last name"
            self.showingAlert.toggle()
            return true
        } else if self.Phonenumber == "" {
            self.showErrorMessage = "Please enter your phone number"
            self.showingAlert.toggle()
            return true
        } else if self.Phonenumber.count < 10 {
            self.showErrorMessage = "Please enter valid phone number"
            self.showingAlert.toggle()
            return true
        } else if self.emailAddress == "" {
            self.showErrorMessage = "Please enter your email address"
            self.showingAlert.toggle()
            return true
        } else if !self.emailAddress.isValid() {
            self.showErrorMessage = "Please enter valid email address"
            self.showingAlert.toggle()
            return true
        } else {
            return false
        }
    }
    
}

struct IntakePersonalInfoView_Previews: PreviewProvider {
    static var previews: some View {
        IntakePersonalInfoView(rootIsActive: .constant(false))
    }
}

extension IntakePersonalInfoView: InsuranceDelegate {
    func didSelectInsurance(index: Int) {
        print("Insurance ID = ",self.observedInsurance.arrInsuranceList[index].id!)
        self.observedInsuranceSelection.insuranceName = self.observedInsurance.arrInsuranceList[index].name!
    }
}

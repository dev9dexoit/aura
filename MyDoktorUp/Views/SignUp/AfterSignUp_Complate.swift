//
//  AfterSignUp_Complate.swift
//  MyDoktorUp
//
//  Created by MACBOOK PRO on 20/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI

struct AfterSignUp_Complate: View {
    
    @Environment(\.presentationMode) var presentationMode
    var SelectedTab: Int = 0
    var EnterdEmail:String = ""
    @Binding var isGoToSignIpTap:Bool
    @Binding var isDoneTap:Bool
    
    var body: some View {
        NavigationView {
            ZStack {
                kCustomLightBlueColor
                VStack(alignment: .center, spacing: 70) {
                    Text("Your account is created. The verification email has been sent to \(EnterdEmail).")
                        .foregroundColor(kCustomTextBlueColor)
                        .font(.system(size: 21, weight: Font.Weight.regular, design: Font.Design.default))
                        .multilineTextAlignment(.center)
                    
                    NavigationLink(destination: LoginView(SelectedTab: SelectedTab), isActive: self.$isGoToSignIpTap){
                        Button(action: {
                            print("checking")
                            self.isGoToSignIpTap = true
                        }) {
                            Text("Go Back to Sign In")
                                .font(.system(.body))
                                .fontWeight(.medium)
                                .foregroundColor(Color.gray)
                                .padding(.vertical, 12)
                                .frame(minWidth: 0, maxWidth: .infinity)
                                .overlay(
                                    RoundedRectangle(cornerRadius: 10)
                                        .strokeBorder(style: StrokeStyle(lineWidth: 1))
                                        .foregroundColor(Color.gray)
                                        .cornerRadius(10)
                                    
                            )
                        }
                        .background(Color.white)
                    }
                    .isDetailLink(false)
                }.padding(.horizontal, 20)
                    .background(kCustomLightBlueColor)
                    .hideNavigationBar()
            }
        }
        .navigationBarTitle("Account Creation", displayMode: .inline)
            .navigationViewStyle(StackNavigationViewStyle()) // This line is supporting iPad, we should not comment out this line
    }
}

struct AfterSignUp_Complate_Previews: PreviewProvider {
    static var previews: some View {
        AfterSignUp_Complate(isGoToSignIpTap: .constant(false), isDoneTap: .constant(false))
    }
}

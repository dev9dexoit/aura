//
//  SignupView.swift
//  MyDoktorUp
//
//  Created by Mac on 27/02/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI

enum ActiveAlert {
    case Login,SignUp,validation
}

struct SignupView: View {
    
    @EnvironmentObject var model: NavigationIsActiveModel
    @Environment(\.presentationMode) var presentationMode
    @State private var activeAlert: ActiveAlert = .validation
    
    @State var SelectedTab: Int = 0
    @State var username = ""
    @State var firstname = ""
    @State var lirstname = ""
    @State var email = ""
    @State var password = ""
    @State var confirmPassword = ""
    @State var showValidation = false
    @State var LoginshowError = false
    @State var issecured = false
    
    @State private var isShowing = false
    @State var LoginRespose = [String:Any]()
    @State var SignUpRespose = [String:Any]()
    
    @State var isSignUpComplate = false
    var body: some View {
        LoadingView(isShowing: .constant(isShowing)) {
            ZStack {
                kPrimaryRegularColor
                    .edgesIgnoringSafeArea(.top)
                    .edgesIgnoringSafeArea(.bottom)
                
                // Custom Back Button
                CustomBackButton()
                
                VStack(alignment: .center, spacing: 5) {
                    // SignUp title view
                    AppLogoTitleView(SelectedTab: self.SelectedTab)
                    
                    VStack(alignment: .leading, spacing: 10) {
                        VStack(alignment: .leading, spacing: 5) {
                            ZStack(alignment: .leading) {
                                if self.email.isEmpty { Text("Email").foregroundColor(.white).padding(.all, 6) }
                                TextField("", text: self.$email)
                                    .foregroundColor(Color.white)
                                    .padding(.all, 6)
                                    .accentColor(Color.white)
                            }
                            DividerView()
                        }
                        VStack(alignment: .leading, spacing: 5) {
                            ZStack(alignment: .leading) {
                                if self.password.isEmpty { Text("Password").foregroundColor(.white).padding(.all, 6) }
                                
                                
                                if self.issecured {
                                    HStack() {
                                        
                                        TextField("", text: self.$password)
                                            .foregroundColor(Color.white)
                                            .padding(.all, 6)
                                            .accentColor(Color.white)
                                        Spacer()
                                        Button(action: {
                                            self.issecured = false
                                        }) {
                                            
                                            Image("Hide-Password")
                                                .resizable()
                                                .foregroundColor(.white)
                                                .frame(width: 20, height:20, alignment: .trailing)
                                        }
                                    }
                                } else {
                                    
                                    HStack()
                                        {
                                            SecureField("", text: self.$password)
                                                .foregroundColor(Color.white)
                                                .padding(.all, 6)
                                                .accentColor(Color.white)
                                            Spacer()
                                            Button(action: {
                                                self.issecured = true
                                            }) {
                                                
                                                Image("Show-Password")
                                                    .resizable()
                                                    .foregroundColor(.white)
                                                    .frame(width: 20, height: 20, alignment: .trailing)
                                            }
                                    }
                                }
                            }
                            DividerView()
                        }
                        VStack(alignment: .leading, spacing: 15) {
                            Text("• Minimum 8 characters (no spaces)")
                                .font(.system(.caption))
                                .fontWeight(.regular)
                                .foregroundColor(Color.white)
                                .padding(.top, 15)
                            Text("* Letters, numbers and special characters")
                                .font(.system(.caption))
                                .fontWeight(.regular)
                                .foregroundColor(Color.white)
                                .padding(.bottom, 15)
                        }.padding(.top, 10)
                        Button(action: {
                            print("Sign Up Tapped!")
                            if self.isPassword(password:self.password)  && self.isValidEmail(Email:self.email) {
                                self.isShowing = true
                                let SliptEmail = self.email.split(separator: "@")
                                self.firstname = String(SliptEmail[0])
                                SignUpObserver().signUpViaEmail(email: self.email, firstname: self.firstname, lastname: self.firstname, username: self.email, password: self.password) { (isSucces) in
                                    print("isSucces \(isSucces)")
                                    self.SignUpRespose = isSucces
                                    let Succes = isSucces["success"] as? Bool
                                    if  Succes == true{
                                        print("sucess signup")
                                        print("isSucces \(isSucces)")
                                        self.isSignUpComplate = true
                                        self.isShowing = false
                                    } else {
                                        self.isShowing = false
                                        self.activeAlert = .SignUp
                                        self.showValidation = true
                                    }
                                }
                            } else {
                                self.activeAlert = .validation
                                self.showValidation = true
                            }
                        }) {
                            HStack {
                                Text("Sign Up")
                                    .font(.system(.body))
                                    .fontWeight(.medium)
                                    .foregroundColor(Color.white)
                                    .padding(.vertical, 12)
                            }
                            .frame(minWidth: 0, maxWidth: .infinity)
                            .background(kCustomGreenColor)
                            .cornerRadius(10)
                        }
                        .padding(.top, 10)
                        .alert(isPresented: self.$showValidation) {
                            switch self.activeAlert {
                            case .Login:
                                let error = self.LoginRespose["error"] as?[String:String]
                                let errorMessage:String = error!["message"] ?? ""
                                return Alert(title: Text(errorMessage))
                            case .SignUp:
                                let error = self.SignUpRespose["error"] as?[String:String]
                                let errorMessage:String = error!["message"] ?? ""
                                print(errorMessage)
                                return Alert(title: Text(errorMessage))
                            case .validation:
                                if !self.isValidEmail(Email:self.email) {
                                    return Alert(title: Text("Please enter valid email address"))
                                } else if !self.isPassword(password:self.password) {
                                    return  Alert(title: Text("Password must be at least 8 characters long. Letters, numbers, special characters and no spaces are allowed"))
                                } else {
                                    return Alert(title: Text(""))
                                }
                            }
                        }
                        VStack {
                            NavigationLink(destination:AfterSignUp_Complate(SelectedTab:self.SelectedTab, EnterdEmail: self.email,isGoToSignIpTap: .constant(false),isDoneTap: .constant(false)), isActive: self.$isSignUpComplate) {
                                EmptyView()
                            }
                        }.padding(.top, 15)
                    }.padding()
                }.padding(.horizontal, 20)
                    .fixedSize(horizontal: false, vertical: true)
            }  .onAppear(){
                    if UserDefaultsManager().loginType == LoginTypes.Email
                    {
                          self.email = UserDefaultsManager().userEmail ?? ""
                    }
            }
            .navigationBarTitle("")
            .navigationBarHidden(true)
            .navigationBarBackButtonHidden(true)
            .onDisappear {
                self.model.pushed = false
            }
        }
    }
    
    func isPassword(password: String) -> Bool {
        let regularExpression = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$"
        let passwordValidation = NSPredicate.init(format: "SELF MATCHES %@", regularExpression)
        return passwordValidation.evaluate(with: password)
    }
    func isValidEmail(Email:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: Email)
        return result
    }
}

struct SignupView_Previews: PreviewProvider {
    static var previews: some View {
        SignupView(SelectedTab: 0)
    }
}

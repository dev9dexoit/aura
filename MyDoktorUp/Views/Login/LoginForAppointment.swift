//
//  LoginForAppointment.swift
//  MyDoktorUp
//
//  Created by MACBOOK PRO on 09/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI
import GoogleSignIn
import AuthenticationServices

struct LoginForAppointment: View {
    @EnvironmentObject var model: NavigationIsActiveModel
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    
    @State var SelectedTab: Int
    
    @State var selection = 0
    
    var body: some View {
        NavigationView {
            ZStack {
                kPrimaryRegularColor
                    .edgesIgnoringSafeArea(.top)
                    .edgesIgnoringSafeArea(.bottom)
                VStack {
                    VStack(alignment: .center, spacing: 2) {
                        // Signin logo & title
                        AppLogoTitleViewForAppointment(SelectedTab: self.SelectedTab)
                        
                        // SignUp view
                        AppSignUpViewForAppointment(SelectedTab: self.SelectedTab, isSelectSignIn: false)
                            .padding(.top, 25)
                        Spacer()
                    }
                    .fixedSize(horizontal: false, vertical: true)
                }
                .navigationBarTitle("")
                .navigationBarHidden(true)
            }
        }.navigationViewStyle(StackNavigationViewStyle()) // This line is supporting iPad, we should not comment out this line
            .navigationBarTitle("")
            .navigationBarHidden(true)
    }
}
struct AppLogoTitleViewForAppointment: View {
    @State var SelectedTab: Int
    var body: some View {
        VStack(alignment: .center, spacing: 20){
            Image("doktoup_logo").padding(.top, 10)
            Text("\(LogInTitleText[self.SelectedTab] ?? "")")
                .font(.system(.title))
                .fontWeight(.bold)
                .foregroundColor(Color.white)
                .multilineTextAlignment(.center)
        }.padding(.horizontal, 20)
    }
}
struct AppSignUpViewForAppointment: View {
    @EnvironmentObject var model: NavigationIsActiveModel
    @Environment(\.presentationMode) var presentationMode
    @State var GobackFirstTab = false
    @State var GobackSecondTab = false
    @State var GobackThirdTab = false
    @State var GobackForthTab = false
    @State var SelectedTab: Int
    @State var isShowing = false
    @ObservedObject var GetUserData = UserInformationObserver()
    @ObservedObject var userDefaultsManager = UserDefaultsManager()
    @EnvironmentObject var appState: AppState
    
    @State var isSelectSignIn:Bool
    var body: some View {
        VStack() {
            
            SignInWithApple { (Success) in
                
                if  Success == true {
                    if  UserDefaultsManager().isSign ?? false
                    {
                        self.GetUserData.getUser(strEmail: self.userDefaultsManager.userEmail ?? "") { (status,message) in
                            print("user number \(self.userDefaultsManager.userPhoneNumber ?? "")")
                        }
                    }
                    print("sucess 2 login")
                    self.isShowing = false
                    
                    switch self.SelectedTab {
                    case 1:
                        self.appState.reloadDashboard(selectedTab: 1, selectedProvider: Provider())
                        self.GobackFirstTab = true
                        break
                    case 2:
                        self.appState.reloadDashboard(selectedTab: 2, selectedProvider: Provider())
                        break
                    case 3:
                        self.appState.reloadDashboard(selectedTab: 3, selectedProvider: Provider())
                        
                        break
                    case 4:
                        self.appState.reloadDashboard(selectedTab: 4, selectedProvider: Provider())
                        
                        break
                        
                    default:
                        break
                    }
                    
                } else {
                    self.isShowing = false
                }
            }
            
            GoogleSignIn(completion: { (Success) in
                
                if  Success == true {
                    if  UserDefaultsManager().isSign ?? false
                    {
                        self.GetUserData.getUser(strEmail: self.userDefaultsManager.userEmail ?? "") { (status,message) in
                            print("user number \(self.userDefaultsManager.userPhoneNumber ?? "")")
                        }
                    }
                    print("sucess 2 login")
                    self.isShowing = false
                    
                    switch self.SelectedTab {
                    case 1:
                        self.appState.reloadDashboard(selectedTab: 1, selectedProvider: Provider())
                        self.GobackFirstTab = true
                        break
                    case 2:
                        self.appState.reloadDashboard(selectedTab: 2, selectedProvider: Provider())
                        break
                    case 3:
                        self.appState.reloadDashboard(selectedTab: 3, selectedProvider: Provider())
                        
                        break
                    case 4:
                        self.appState.reloadDashboard(selectedTab: 4, selectedProvider: Provider())
                        
                        break
                        
                    default:
                        break
                    }
                    
                } else {
                    self.isShowing = false
                }
            }).hidden()
            
            Button(action: {
                GIDSignIn.sharedInstance()?.signIn()
                
            }) {
                HStack {
                    Image("ic_google_logo")
                        .renderingMode(.original)
                    Text("Sign in with Google")
                        .font(.system(.body))
                        .fontWeight(.medium)
                        .foregroundColor(Color(red: 92/255, green: 92/255, blue: 92/255))
                        .padding(.vertical, 12)
                    
                }
                .frame(minWidth: 0, maxWidth: .infinity)
                .background(Color.white)
                .cornerRadius(10)
            }.padding(.bottom, 5)
            
            
            Button(action: {
                
                self.model.pushed = true
            }) {
                Text("Sign up with Email")
                    .font(.system(.body))
                    .fontWeight(.medium)
                    .foregroundColor(Color.white)
                    .padding(.vertical, 12)
                    .frame(minWidth: 0, maxWidth: .infinity)
                    .overlay(
                        RoundedRectangle(cornerRadius: 10)
                            .strokeBorder(style: StrokeStyle(lineWidth: 1))
                            .foregroundColor(Color.white)
                            .cornerRadius(10)
                        
                )}.padding(.bottom, 5)
            
            Spacer()
            
            Button(action: {
                self.isSelectSignIn = true
            }) {
                Text("Have an account? Sign in")
                    .font(.system(.body))
                    .fontWeight(.medium)
                    .foregroundColor(Color.white)
            }.padding(.top, 40)
            
            
            Spacer()
            
            VStack {
                NavigationLink(
                    destination:SignupView(SelectedTab:self.SelectedTab),
                    isActive: self.$model.pushed) {
                        EmptyView()
                }
                
                
                NavigationLink(
                    destination:LoginView(SelectedTab:self.SelectedTab),
                    isActive: self.$isSelectSignIn) {
                        EmptyView()
                }
                
            }
        }.padding(.horizontal, 20)
        
    }
    
    
}
struct LoginForAppointment_Previews: PreviewProvider {
    static var previews: some View {
        LoginForAppointment(SelectedTab: 1)
    }
}

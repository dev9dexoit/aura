//
//  LoginView.swift
//  MyDoktorUp
//
//  Created by Mac on 11/02/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI
import GoogleSignIn




struct LoginView: View {
    @EnvironmentObject var model: NavigationIsActiveModel
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    @ObservedObject var GetUserData = UserInformationObserver()
    @ObservedObject var userDefaultsManager = UserDefaultsManager()
    
    @State var SelectedTab: Int
    @State var LoginRespose = [String:Any]()
    @State var email = ""
    @State var password = ""
    @State var isShowing = false
    @State var isActive = false
    @State var showError = false
    @State var GobackFirstTab = false
    @State var GobackSecondTab = false
    @State var GobackThirdTab = false
    @State var GobackForthTab = false
    @State var selection = 0
    @State var isForgotPassword = false
    @State var issecured = false
    @State var isForgotTap = false
    @EnvironmentObject var appState: AppState
    
    
    @State var Errormessage = ""
    
    @State private var activeAlert: ForgotPassworsdAlert = .InvalidEmail
    
    @State var isSingInButtonShow = false
    
    var body: some View {
        LoadingView(isShowing: .constant(self.isShowing)) {
            ZStack {
                kPrimaryRegularColor
                    .edgesIgnoringSafeArea(.all)
                
                // Custom Back Button
                CustomBackButton()
                
                VStack(alignment: .center, spacing: 5) {
                    // Signin logo & title
                    AppLogoTitleView(SelectedTab: self.SelectedTab)
                    
                    // Signin View
                    VStack(alignment: .leading, spacing: 3) {
                        VStack(alignment: .leading, spacing: 6) {
                            ZStack(alignment: .leading) {
                                if self.email.isEmpty { Text("Email").foregroundColor(.white).padding(.all, 6) }
                                TextField("", text: self.$email)
                                    .keyboardType(.emailAddress)
                                    .foregroundColor(Color.white)
                                    .padding(.all, 6)
                                    .accentColor(Color.white)
                            }
                            DividerView()
                        }
                        VStack(alignment: .leading, spacing: 6) {
                            
                            if self.isSingInButtonShow{
                                ZStack(alignment: .leading) {
                                    if self.password.isEmpty { Text("Password").foregroundColor(.white).padding(.all, 6) }
                                    
                                    if self.issecured
                                    {
                                        HStack()
                                            {
                                                
                                                TextField("", text: self.$password)
                                                    .foregroundColor(Color.white)
                                                    .padding(.all, 6)
                                                    .accentColor(Color.white)
                                                Spacer()
                                                Button(action: {
                                                    self.issecured = false
                                                }) {
                                                    
                                                    Image("Hide-Password")
                                                        .resizable()
                                                        .foregroundColor(.white)
                                                        .frame(width: 20, height:20, alignment: .trailing)
                                                }
                                        }
                                        
                                        
                                        
                                    }
                                    else{
                                        
                                        HStack()
                                            {
                                                SecureField("", text: self.$password)
                                                    .foregroundColor(Color.white)
                                                    .padding(.all, 6)
                                                    .accentColor(Color.white)
                                                Spacer()
                                                Button(action: {
                                                    self.issecured = true
                                                }) {
                                                    
                                                    Image("Show-Password")
                                                        .resizable()
                                                        .foregroundColor(.white)
                                                        .frame(width: 20, height: 20, alignment: .trailing)
                                                }
                                                
                                        }
                                        
                                        
                                    }
                                    
                                    
                                }
                                DividerView()
                            }
                            
                            
                        }
                        
                        Button(action: {
                            print("Sign in Tapped!")
                            
                            if self.isSingInButtonShow
                            {
                                if self.userDefaultsManager.user_last_login != ""
                                {
                                    self.isShowing = true
                                    SignInObserver().signInViaEmail(username: self.email, password: self.password) { (isSucces) in
                                        
                                        
                                        
                                        self.LoginRespose = isSucces
                                        let LoginSucces = isSucces["success"] as? Bool
                                        
                                        if  LoginSucces == true {
                                            print("sucess 2 login")
                                            self.isShowing = false
                                            
                                            switch self.SelectedTab {
                                            case 1:
                                                self.appState.reloadDashboard(selectedTab: 1, selectedProvider: Provider())
                                                self.GobackFirstTab = true
                                                break
                                            case 2:
                                                self.appState.reloadDashboard(selectedTab: 2, selectedProvider: Provider())
                                                break
                                            case 3:
                                                self.appState.reloadDashboard(selectedTab: 3, selectedProvider: Provider())
                                                
                                                break
                                            case 4:
                                                self.appState.reloadDashboard(selectedTab: 4, selectedProvider: Provider())
                                                
                                                break
                                                
                                            default:
                                                break
                                            }
                                            
                                        } else {
                                            let error = self.LoginRespose["error"] as?[String:String]
                                            
                                            self.Errormessage = error!["message"] ?? ""
                                            self.showError = true
                                            self.isShowing = false
                                            
                                        }
                                    }
                                }
                                else
                                {
                                    self.isForgotPassword = true
                                }
                            }else
                            {
                                self.isShowing = true
                                
                                
                                
                                self.GetUserData.getUser(strEmail: self.email ) { (status,message) in
                                    
                                    if status == true{
                                        self.isShowing = false
                                        
                                        if self.userDefaultsManager.user_last_login != ""
                                        {
                                            self.isSingInButtonShow = true
                                        }
                                        else
                                        {
                                            self.isForgotPassword = true
                                            
                                        }
                                        
                                        
                                    }else
                                    {
                                        self.Errormessage = message
                                        self.isShowing = false
                                        self.showError = true
                                        
                                    }
                                    
                                }
                            }
                            
                            
                        }) {
                            HStack {
                                if self.isSingInButtonShow
                                {
                                    Text("Sign in")
                                        .font(.system(.body))
                                        .fontWeight(.medium)
                                        .foregroundColor(Color.white)
                                        .padding(.vertical, 10)
                                }else
                                {
                                    Text("Next")
                                        .font(.system(.body))
                                        .fontWeight(.medium)
                                        .foregroundColor(Color.white)
                                        .padding(.vertical, 10)
                                }
                                
                            }
                            .frame(minWidth: 0, maxWidth: .infinity)
                            .background(kCustomGreenColor)
                            .cornerRadius(10)
                        }
                        .alert(isPresented: self.$showError)
                        {
                            
                            return Alert(title: Text(self.Errormessage))
                        }
                        .padding(.top, 20)
                        
                        
                        HStack {
                            Spacer()
                            Button(action: {
                                self.isForgotPassword = true
                            }) {
                                Text("Forgot password?")
                                    .font(.system(.headline))
                                    .fontWeight(.medium)
                                    .padding(.vertical, 10)
                                    .foregroundColor(Color.white)
                                    .multilineTextAlignment(.center)
                            }
                            Spacer()
                        }.padding(.top, 10)
                        
                        VStack {
                            NavigationLink(
                                destination:ForgotPasswordView(),
                                isActive: self.$isForgotPassword) {
                                    EmptyView()
                            }
                            
                        }
                    }.padding()
                        .navigationBarTitle("")
                        .navigationBarHidden(true)
                        .navigationBarBackButtonHidden(true)
                }.padding(.horizontal, 20)
                    .fixedSize(horizontal: false, vertical: true)
            }
            .onAppear(){
                
                if UserDefaultsManager().loginType == LoginTypes.Email
                {
                    self.email = UserDefaultsManager().userEmail ?? ""
                }
                
            }
            .navigationBarTitle("")
        }
        
    }
}
struct AppLogoTitleView: View {
    @State var SelectedTab: Int
    var body: some View {
        VStack(alignment: .center, spacing: 20){
            Image("doktoup_logo").padding(.top, 10)
            
            Text("\(LogInTitleText[self.SelectedTab] ?? "")")
                .font(.system(.title))
                .fontWeight(.bold)
                .foregroundColor(Color.white)
                .multilineTextAlignment(.center)
            
        }
        
    }
}


struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        
        LoginView(SelectedTab: 0)
        
    }
}



struct DividerView: View {
    var body: some View {
        HStack {
            Spacer()
                .frame(height:1)
                .background(Color.white)
        }
    }
}


struct SocialLogin: UIViewRepresentable {
    
    func makeUIView(context: UIViewRepresentableContext<SocialLogin>) -> UIView {
        return UIView()
    }
    
    func updateUIView(_ uiView: UIView, context: UIViewRepresentableContext<SocialLogin>) {
    }
    
    func attemptLoginGoogle() {
        GIDSignIn.sharedInstance()?.presentingViewController = UIApplication.shared.windows.last?.rootViewController
        GIDSignIn.sharedInstance()?.signIn()
    }
    
    
    
}
struct CustomBackButton: View {
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Button(action: {
                    self.mode.wrappedValue.dismiss()
                }) {
                    Image("ic_back")
                        .foregroundColor(Color.white).padding(20.0)
                }
                Spacer()
            }
            Spacer()
        }
    }
}

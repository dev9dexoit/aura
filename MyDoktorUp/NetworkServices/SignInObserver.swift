//
//  SignInObserver.swift
//  MyDoktorUp
//
//  Created by SHREE SAHANA on 4/19/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import Combine
import Alamofire


class SignInObserver : ObservableObject {
    
    
    let objectWillChange = PassthroughSubject<Void,Never>()
    @Published var userInfo = User()
    func getRequestBody(username: String, password: String,auth_token:String) -> [String: String] {
        var requestBody: [String: String] = [:]
        requestBody["email"] = username
        requestBody["password"] = password
        //TODOs: Need to pass auth token for Google and Apple login
        requestBody["auth_token"] = auth_token
        
        return requestBody
    }

    func signInViaEmail(username: String, password: String, completionHander: @escaping ([String: Any])->Void) {
        let serverUrl = baseUrl + ServiceURI.userLogin
        guard let serviceUrl = URL(string: serverUrl) else {
            return
        }
        dLog("Service URL -> \(serviceUrl)")
        var urlReq = URLRequest(url: serviceUrl)
        urlReq.allHTTPHeaderFields = NetworkManager.getHeaders()
        urlReq.httpMethod = HTTPMethodType.postMethod.rawValue
        let requestBodyDict = self.getRequestBody(username: username, password: password, auth_token: "")
        let data = try! JSONSerialization.data(withJSONObject: requestBodyDict, options: JSONSerialization.WritingOptions.prettyPrinted)
        let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        if let json = json {
            urlReq.httpBody = json.data(using: String.Encoding.utf8.rawValue)
        }
        

        AF.request(urlReq).responseJSON { (response) in
            if response.error == nil {
                if let dataResponse = response.data {
                    do {
                        // make sure this JSON is in the format we expect
                        if let dictData = try JSONSerialization.jsonObject(with: dataResponse, options: []) as? [String: Any] {
                            let isSuccess = dictData["success"] as? Bool
                            if  isSuccess == true {
                                if let arrResult = dictData["result"] {
                                    print(arrResult)
                                    guard let jsonData = try? JSONSerialization.data(withJSONObject: arrResult, options: .prettyPrinted) else {
                                        return
                                    }
                                    
                                    self.userInfo = try! JSONDecoder().decode(User.self, from: jsonData)
                                    UserDefaultsManager().loginType = LoginTypes.Email
                                                                      
                                    UserDefaultsManager().setValueFor(isSign: true)
                                    UserDefaultsManager().setValueFor(user: self.userInfo)
                                    print(self.userInfo)
                                }
                                print("Success")
                                completionHander(dictData)
                            } else {

                                completionHander(dictData)
                            }
                        }
                    } catch let error as NSError {
                        print("Failed to load: \(error.localizedDescription)")
                        
                    }
                }
            } else {
                print("Failure")
               
            }
        }
    }
    
    func Forgotpassword(username: String, completionHander: @escaping (Bool)->Void) {
        let serverUrl = baseUrl + ServiceURI.ForgotPassword
        guard let serviceUrl = URL(string: serverUrl + "/\(username)") else {
            return
        }
        dLog("Service URL -> \(serviceUrl)")
        var urlReq = URLRequest(url: serviceUrl)
        urlReq.allHTTPHeaderFields = NetworkManager.getHeaders()
        urlReq.httpMethod = HTTPMethodType.getMethod.rawValue
        
        AF.request(urlReq).responseJSON { (response) in
            
            print("response \(response)")
            
            if response.error == nil {
                if let dataResponse = response.data {
                    do {
                        // make sure this JSON is in the format we expect
                        if let dictData = try JSONSerialization.jsonObject(with: dataResponse, options: []) as? [String: Any] {
                            let isSuccess = dictData["success"] as? Bool
                            if  isSuccess == true {
                                
                                print("Success")
                                completionHander(true)
                            } else {

                                completionHander(false)
                            }
                        }
                    } catch let error as NSError {
                        completionHander(false)
                        print("Failed to load: \(error.localizedDescription)")
                        
                    }
                }
            } else {
                print("Failure")
               
            }
        }
    }
    
    
    
    func signInViaSocialSite(Token:String,authType:String, completionHander: @escaping (Bool)->Void) {
        let serverUrl = baseUrl + ServiceURI.userLogin + ServiceURI.Oauth_Provider
        

        guard let serviceUrl = URL(string: serverUrl + "=\(authType)") else {
            return
        }
       
        var urlReq = URLRequest(url: serviceUrl)
        urlReq.allHTTPHeaderFields = NetworkManager.getHeaders()
        
        if  authType == LoginTypes.Apple
        {
            var header = NetworkManager.getHeaders()
            
            header[HeaderFields.UserAgent] = "DoktorUp(Build:\(Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? ""); MobileOS: \(HeaderFields.MobileOSValue); Version: \(UIDevice.current.systemVersion)"
        }
        urlReq.httpMethod = HTTPMethodType.postMethod.rawValue
        let requestBodyDict = self.getRequestBody(username: "", password: "", auth_token: Token)
        let data = try! JSONSerialization.data(withJSONObject: requestBodyDict, options: JSONSerialization.WritingOptions.prettyPrinted)
        let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        if let json = json {
            urlReq.httpBody = json.data(using: String.Encoding.utf8.rawValue)
        }
        AF.request(urlReq).responseJSON { (response) in
            if response.error == nil {
                if let dataResponse = response.data {
                    do {
                        // make sure this JSON is in the format we expect
                        if let dictData = try JSONSerialization.jsonObject(with: dataResponse, options: []) as? [String: Any] {
                            let isSuccess = dictData["success"] as? Bool
                            if  isSuccess == true {
                                if let arrResult = dictData["result"] {
                                    print(arrResult)
                                    guard let jsonData = try? JSONSerialization.data(withJSONObject: arrResult, options: .prettyPrinted) else {
                                        return
                                    }
                                    
                                    self.userInfo = try! JSONDecoder().decode(User.self, from: jsonData)
//                                    self.userInfo.type =  authType
                                    UserDefaultsManager().loginType = authType
                                    
                                    UserDefaultsManager().setValueFor(isSign: true)
                                    UserDefaultsManager().setValueFor(user: self.userInfo)
                                }
                                completionHander(true)
                            } else {

                                completionHander(false)
                            }
                        }
                    } catch let error as NSError {
                        completionHander(false)
                        print("Failed to load: \(error.localizedDescription)")
                        
                    }
                }
            } else {
                print("Failure")
               
            }
        }
    }
    
}

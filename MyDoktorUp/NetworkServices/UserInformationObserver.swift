//
//  UserInformationObserver.swift
//  MyDoktorUp
//
//  Created by Mac on 24/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import Alamofire

class UserInformationObserver : ObservableObject {
    
    @Published var arrBookingFor = [PatientRelation]()
    @Published var userProfileAttributes: UserProfileAttributes?
    @Published var userSpecialAttributes: UserSpecialAttributes?
    @Published var usersBookingFor = [UserBookingFor]()
    @Published var onlyPersonNames = [String]()
    @Published var onlyPersonRelations = [String]()
    
    func getUser(strEmail: String, complitionHandler: @escaping (Bool,String)->Void) {
        self.arrBookingFor.removeAll()
        let strURL = baseUrl + ServiceURI.userSignUp +  "/" + "\(strEmail)"
        guard let url = URL(string: strURL) else {
            return
        }
        let finalURL = url.appending("display", value: "all")
        print("finalURL \(finalURL)")

        var urlReq = URLRequest(url: finalURL)
        urlReq.allHTTPHeaderFields = NetworkManager.getHeaders()
        urlReq.httpMethod = HTTPMethodType.getMethod.rawValue
        
        AF.request(urlReq).responseJSON { (response) in
            if response.error == nil {
                if let dataResponse = response.data {
                    
                    do {
                        // make sure this JSON is in the format we expect
                        if let dictData = try JSONSerialization.jsonObject(with: dataResponse, options: []) as? [String: Any] {
                            print("dictData \(dictData)")
                            if let isSuccess = dictData["success"] as? Bool, isSuccess {
                                self.arrBookingFor.removeAll()
                                self.usersBookingFor.removeAll()
                                self.onlyPersonNames.removeAll()
                                self.onlyPersonRelations.removeAll()
                                
                                if let result = dictData["result"] as? [String: Any] {
                                    //                                    print("result Data \(result["user_profile_attributes"])")
                                    
                                    
                                    if let user_special_attributes = result["user_special_attributes"] as? [String:Any  ]
                                    {
                                        let user_special_attributes = User_Special_Attributes.initWith(dictInfo: user_special_attributes)
                                        UserDefaultsManager().user_type = user_special_attributes.ethnicity
                                        UserDefaultsManager().user_is_Active = user_special_attributes.gender
                                        UserDefaultsManager().user_insurance = user_special_attributes.insurance
                                        UserDefaultsManager().user_created_at = user_special_attributes.language
                                        UserDefaultsManager().user_update_at = user_special_attributes.marital_status
                                        UserDefaultsManager().user_update_at = user_special_attributes.martial
                                        UserDefaultsManager().user_update_at = user_special_attributes.race
                                        
                                    }
                                    
                                    
                                    if let user_profile_attributes = result["user_system_attributes"] as? [String:Any  ]
                                    {
                                        let user_System_Attribute = User_System_Attribute.initWith(dictInfo: user_profile_attributes)
                                      UserDefaultsManager().user_type = user_System_Attribute.user_type
                                        UserDefaultsManager().user_is_Active = user_System_Attribute.is_Active
                                        UserDefaultsManager().user_last_login = user_System_Attribute.last_login
                                        UserDefaultsManager().user_created_at = user_System_Attribute.created_at
                                        UserDefaultsManager().user_update_at = user_System_Attribute.update_at

                                    }
                                    
                                    if let user_profile_attributes = result["user_profile_attributes"] as? [String:Any  ]
                                    {
                                        let userInformation = UserProfileInfo.initWith(dictInfo: user_profile_attributes)
                                        UserDefaultsManager().userPhoneNumber = userInformation.phone
                                        
                                        
                                        UserDefaultsManager().userdob = userInformation.dob
                                        UserDefaultsManager().userzip = userInformation.zip
                                        UserDefaultsManager().userstate = userInformation.state
                                        UserDefaultsManager().useraddress1 = userInformation.address1
                                        UserDefaultsManager().useraddress2 = userInformation.address2
                                        
                                        let firstname = userInformation.first_name ?? ""
                                        let lastname = userInformation.last_name ?? ""
                                        UserDefaultsManager().userFullName = firstname + " " + lastname
                                        
                                        
                                        
                                        print("userInformation name \(UserDefaultsManager().userFullName ?? "")")
                                    }
                                    if let user_booking_forself = result["user_booking_forself"] as? [Any], user_booking_forself.count > 0 {
                                        for i in 0..<user_booking_forself.count {
                                            if let dictResult = user_booking_forself[i] as? [String:Any] {
                                                let userInformation = UserBookingFor.initWith(dictInfo: dictResult)
                                                var relationDesignation = ""
                                                if let relation = userInformation.relationship {
                                                    if relation == "Yes" {
                                                        relationDesignation = "Myself"
                                                    } else {
                                                        relationDesignation = relation
                                                    }
                                                    self.onlyPersonRelations.append(relationDesignation)
                                                }
                                                if let designation = userInformation.simple_name {
                                                    self.onlyPersonNames.append(designation)
                                                    relationDesignation = relationDesignation + " - " + designation
                                                }
                                                self.usersBookingFor.append(userInformation)
                                                self.arrBookingFor.append(PatientRelation(relationDesignation: relationDesignation))
                                            }
                                        }
                                        
                                        self.arrBookingFor.insert(PatientRelation(relationDesignation: "Myself"), at: 0)
                                        self.onlyPersonRelations.insert("Myself", at: 0)
                                        
                                    } else {
                                        self.arrBookingFor.append(PatientRelation(relationDesignation: "Myself"))
                                        self.onlyPersonRelations.append("Myself")
                                    }
                                    
                                    //Set User Profile Attributes
                                    if let userProfileAttributes = result["user_profile_attributes"] as? [String: Any] {
                                        self.userProfileAttributes = UserProfileAttributes.initWith(dictInfo: userProfileAttributes)
                                    }
                                    if let userSpecialAttributes = result["user_special_attributes"] as? [String: Any] {
                                        self.userSpecialAttributes = UserSpecialAttributes.initWith(dictInfo: userSpecialAttributes)
                                    }
                                }
                                let isError = dictData["error"] as? [String:String]
                                complitionHandler(true, "\(isError!["message"] ?? "")")
                            } else {
                                let isError = dictData["error"] as? [String:String]
                                complitionHandler(false,"\(isError!["message"] ?? "")")
                            }
                        }
                    } catch let error as NSError {
                        
                        complitionHandler(false,"")
                    }
                }
            } else {
                complitionHandler(false, "")
            }
        }
    }
}


class AccountInformationObserver : ObservableObject {
    
    func updateAccountInfo(infoToSend:[String:Any], complitionHandler: @escaping (Bool,String)->Void) {
        
        let id = UserDefaults.standard.value(forKey: UserDefault.userId) ?? ""
        
        let strURL = baseUrl + ServiceURI.userSignUp +  "/" + "\(id)"
        guard let url = URL(string: strURL) else {
            return
        }
        
        var urlReq = URLRequest(url: url)
        urlReq.allHTTPHeaderFields = NetworkManager.getHeaders()
        urlReq.httpMethod = HTTPMethodType.putMethod.rawValue
        let data = try! JSONSerialization.data(withJSONObject: infoToSend, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        print("json data \(json!)")
        if let json = json {
            urlReq.httpBody = json.data(using: String.Encoding.utf8.rawValue)
        }
        
        AF.request(urlReq).responseJSON { (response) in
            
            if response.error == nil {
                if let dataResponse = response.data {
                    
                    do {
                        // make sure this JSON is in the format we expect
                        if let dictData = try JSONSerialization.jsonObject(with: dataResponse, options: []) as? [String: Any] {
                            if let isSuccess = dictData["success"] as? Bool, isSuccess {
                                
                                if let error = dictData["error"] as? [String:Any] {
                                    let errorMessage = error["message"] as? String ?? ""
                                    complitionHandler(true, errorMessage)
                                }                                
                            } else {
                                if let error = dictData["error"] as? [String:Any] {
                                    let errorMessage = error["message"] as? String ?? ""
                                    complitionHandler(false, errorMessage)
                                }
                            }
                        }
                    } catch let error as NSError {
                        complitionHandler(false, error.localizedDescription)
                    }
                }
            } else {
                complitionHandler(false, "Something went wrong, please try again.")
            }
        }
    }
}

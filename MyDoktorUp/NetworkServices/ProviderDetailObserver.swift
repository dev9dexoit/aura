//
//  ProviderDetailObserver.swift
//  MyDoktorUp
//
//  Created by Mac on 14/02/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import Alamofire

class ProviderDetailObserver : ObservableObject{
    
    @Published var arrProviders = [Provider]()
    var ud = UserDefaultsManager()
    
    func getProviderDetail(provideId:String,complitionHandler:@escaping (Bool)->Void) {
//        guard let strProviderID = ud.providerID else {
//            return
//        }
        
        let strURL = baseUrl + ServiceURI.providerPath + provideId
        guard let url = URL(string: strURL) else {
            return
        }
        print("strURL \(strURL)")
        var urlReq = URLRequest(url: url)
        urlReq.allHTTPHeaderFields = NetworkManager.getHeaders()
        urlReq.httpMethod = HTTPMethodType.getMethod.rawValue
        
        AF.request(urlReq).responseJSON { (response) in
             if response.error == nil {
                if let dataResponse = response.data {
                    
                    do {
                        // make sure this JSON is in the format we expect
                        if let dictData = try JSONSerialization.jsonObject(with: dataResponse, options: []) as? [String: Any] {
                            if let isSuccess = dictData["success"] as? Bool, isSuccess {
                                if let dictResult = dictData["result"] as? [String:Any] {
                                    
                                    self.arrProviders.append(Provider.initWith(dictInfo: dictResult))
                                   
                                }
                                complitionHandler(true)
                            } else {
                                complitionHandler(false)    
                            }
                        }
                    } catch let error as NSError {
                        print("Failed to load: \(error.localizedDescription)")
                        complitionHandler(false)
                    }
                }
            } else {
                complitionHandler(false)
            }
        }
        
//        Alamofire.request(urlReq).responseJSON {
//                response in
//            if let json = response.result.value {
//                if  (json as? [String : AnyObject]) != nil{
//                    if let dictionaryArray = json as? Dictionary<String, AnyObject?> {
//                        self.dictProviderDetail.removeAll()
//                        if let jsonDict = dictionaryArray["result"] as? [String:Any] {
//                            self.dictProviderDetail.append(Provider.initWith(dictInfo: jsonDict))
//                        }
//                    }
//                }
//            }
//        }
    }
}


class ProviderSlotDetailObserver : ObservableObject {
    
    @Published var arrAppointmentSlots = [ProviderSlots]()
    var ud = UserDefaultsManager()
    func getProviderAppointmentSlots(complitionHandler: @escaping (Bool) -> Void)
    {
        guard let strProviderID = ud.providerID else {
            return
        }
        print(strProviderID)
        let strURL = baseUrl + ServiceURI.appointmentForProviderPath + strProviderID
        guard let url = URL(string: strURL) else {
            return
        }
        dLog("Service URL -> \(url)")
        var urlReq = URLRequest(url: url)
        urlReq.allHTTPHeaderFields = NetworkManager.getHeaders()
        urlReq.httpMethod = HTTPMethodType.getMethod.rawValue
        
        AF.request(urlReq).responseJSON { (response) in
            if response.error == nil {
                if let dataResponse = response.data {
                    
                    do {
                        // make sure this JSON is in the format we expect
                        if let dictData = try JSONSerialization.jsonObject(with: dataResponse, options: []) as? [String: Any] {
                            if let isSuccess = dictData["success"] as? Bool, isSuccess {
                                if let dictResult = dictData["result"] as? [String:Any] {
                                    var arrSlots = [ProviderSlots]()
                                    for dict in dictResult {
                                        var tempDict : [String:Any] = ["date": "", "slots":[]]
                                        tempDict["date"] = dict.key
                                        tempDict["slots"] = dict.value
                                        arrSlots.append(ProviderSlots.initWith(dictInfo: tempDict))
                                    }
                                    let sorted = arrSlots.sorted {$0.date ?? "" < $1.date ?? ""}
                                    DispatchQueue.main.async {
                                        self.arrAppointmentSlots = sorted
                                        complitionHandler(true)

                                    }
                                }
                            } else {
                                complitionHandler(false)
                            }
                        }
                    } catch let error as NSError {
                        print("Failed to load: \(error.localizedDescription)")
                        complitionHandler(false)
                    }
                }
            } else {
                 print("Failed to load")
                complitionHandler(false)
            }
        }
    }
}


//
//  NetworkManager.swift
//  MyDoktorUp
//
//  Created by SHREE SAHANA on 3/24/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation

let baseUrl = "https://api.doktorup.com/api/v2"


let baseImageBeforeUrl = "doktorup.com/data"
let baseImageAfterUrl = "/media/photos/"



struct ServiceURI {
    static let providerPath = "/provider/"
    static let appointmentForProviderPath = "/appointment/provider/"
    static let visitReasonPath = "/provider/visit-reason/"
    static let visitReasonPathCommon = "/category/visit-reason"
    static let insuranceCarrierPath = "/provider/insurance-carrier/"
    static let insuranceCarrierPathCommon = "/category/insurance-carrier"
    static let appointmentPath = "/appointment/"
    static let userAppointmentPath = "/user/appointment/"
    static let userLogin = "/login"
    static let Oauth_Provider = "?oauth2_provider"
    static let userSignUp = "/user"
    static let cancelAppointment = "/appointment/"
    static let ChekingAppointment = "/checkin"
    static let form = "/forms"
    static let PostInsuranceCard = "/insurance-card"
    static let PostdrivingLicense = "/driving-license"
    static let ForgotPassword = "/forgot-password"
    static let changePassword = "/change-password"

}


public enum ImageUrlType: String {
    case dlic = "dlic"
    case insc = "insc"
    
}


public enum HTTPMethodType: String {
    case postMethod = "POST"
    case getMethod = "GET"
    case putMethod = "PUT"
    case deleteMethod = "DELETE"
    case patchMethod = "PATCH"
}
struct HeaderFields {
    static let apiTokenKey = "API-Token"
    static let apiTokenValue = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJHZXR5cyIsIm5hbWUiOiJIdXkgVnUiLCJqdGkiOiIzNWE4MDc0Yi1kZmI4LTRkN2QtYmVmYi0xNzBiM2E2YTZmZDMiLCJpYXQiOjE1NzgzMzIxOTV9.6Y_lDm-6crxFzXZmEMH41g6vVCnqo6pvOkj2A8a3feM"
    
    static let contentTypeKey = "Content-Type"
    static let contentTypeValue = "application/json"
    static let AuthKey = "Authorization"
    static let bearerTokenKey = "Bearer "
    static let UserAgent = "User-Agent"
    static let Build = "Build"
    static let MobileOS = "MobileOS"
    static let MobileOSValue = "iOS"
    static let Version = "Version"
    
}

class NetworkManager {
    static func getHeaders() -> [String: String] {
        let loginToken = UserDefaultsManager().loginSessionId ?? ""
        var headers = [HeaderFields.contentTypeKey: HeaderFields.contentTypeValue]
        headers[HeaderFields.apiTokenKey] = HeaderFields.apiTokenValue
        headers[HeaderFields.AuthKey] = HeaderFields.bearerTokenKey + loginToken
        return headers as Dictionary
    }
    
    
    
    
    
}

//
//  AppointmentListObserver.swift
//  MyDoktorUp
//
//  Created by SHREE SAHANA on 4/18/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import Combine
import Alamofire

class AppointmentListObserver : ObservableObject {
    let objectWillChange = PassthroughSubject<Void,Never>()
    @Published var appointmentData = [AppointmentData]()
    var appointmentModel : [AppointmentModel] = []
    var appointmentInfoModel : AppointmentModel!
    var AppointmentsTypeCount = [String:[AppointmentModel]]()
    func getAppointmentList(ForDigitalCheking:Bool,startDate: String, endDate: String,type:String, completionHander: @escaping (Bool)->Void) {
        let serverUrl = baseUrl + ServiceURI.userAppointmentPath
        var paramUri = ""
        if endDate.isEmpty {
            paramUri = "?startdate=\(startDate)&type=\(type)"
        } else {
            paramUri = "?startdate=\(startDate)&enddate=\(endDate)&type=\(type)"
        }
        guard let serviceUrl = URL(string: serverUrl + paramUri) else {
            return
        }
        dLog("Service URL -> \(serviceUrl)")
        var urlReq = URLRequest(url: serviceUrl)
        urlReq.allHTTPHeaderFields = NetworkManager.getHeaders()
        urlReq.httpMethod = HTTPMethodType.getMethod.rawValue
        
        AF.request(urlReq).responseJSON { (response) in
            print("response list \(response)")
            if response.error == nil {
                if let dataResponse = response.data {
                    do {
                        
                        // make sure this JSON is in the format we expect
                        if let dictData = try JSONSerialization.jsonObject(with: dataResponse, options: []) as? [String: Any] {
                            if let isSuccess = dictData["success"] as? Bool, isSuccess {
                                if let arrResult = dictData["result"] as? [String:Any] {
                                    
                                    
                                    if  dictData.count > 0
                                    {
                                        self.appointmentModel.removeAll()
                                        self.appointmentData.removeAll()
                                        
                                        if let checkedin_appointments = arrResult["checkedin_appointments"] as? [String:Any]
                                        {
                                            
                                          if ForDigitalCheking == false
                                            {
                                                AppointmentType.checkedin_appointments = checkedin_appointments["title"] as? String ?? ""
                                                let Advertise  = checkedin_appointments["appointments"] as? [Any]
                                                
                                                
                                                guard let jsonData = try? JSONSerialization.data(withJSONObject: Advertise!, options: .prettyPrinted) else {
                                                    return
                                                }
                                                
                                                self.appointmentModel = try! JSONDecoder().decode([AppointmentModel].self, from: jsonData)
                                                self.AppointmentsTypeCount[AppointmentType.checkedin_appointments] = self.appointmentModel
                                                self.sortAppointment(completionHander: completionHander)
                                                
                                                self.AppointmentsTypeCount.removeAll()
                                            }
                                            
                                            
                                        }
                                        if let scheduled_appointments = arrResult["schedule_appointments"] as? [String:Any]
                                        {
                                            
                                            AppointmentType.scheduled = scheduled_appointments["title"] as? String ?? ""
                                            
                                            let Advertise  = scheduled_appointments["appointments"] as? [Any]
                                            
                                            
                                            guard let jsonData = try? JSONSerialization.data(withJSONObject: Advertise!, options: .prettyPrinted) else {
                                                return
                                            }
                                            
                                            self.appointmentModel = try! JSONDecoder().decode([AppointmentModel].self, from: jsonData)
                                            self.AppointmentsTypeCount[AppointmentType.scheduled] = self.appointmentModel
                                            self.sortAppointment(completionHander: completionHander)
                                            
                                            self.AppointmentsTypeCount.removeAll()
                                            
                                        }
                                        if let past_Appointment = arrResult["past_appointments"] as? [String:Any]
                                        {
                                            if ForDigitalCheking == false
                                            {
                                            
                                            AppointmentType.past = past_Appointment["title"] as? String ?? ""
                                            
                                            let Advertise  = past_Appointment["appointments"] as? [Any]
                                            
                                            
                                            guard let jsonData = try? JSONSerialization.data(withJSONObject: Advertise!, options: .prettyPrinted) else {
                                                return
                                            }
                                            
                                            self.appointmentModel = try! JSONDecoder().decode([AppointmentModel].self, from: jsonData)
                                            self.AppointmentsTypeCount[AppointmentType.past] = self.appointmentModel
                                            self.sortAppointment(completionHander: completionHander)
                                            
                                            self.AppointmentsTypeCount.removeAll()
                                            
                                            }
                                            
                                            
                                        }
                                        completionHander(true)

                                    }
                                    else
                                    {
                                        completionHander(true)
                                    }
                                     
                                }
                                else
                                {
                                    completionHander(true)
                                }
                            } else {
                                UserDefaultsManager().setValueFor(isSign: false)
                                print("Failure ",dictData)
                                completionHander(false)
                            }
                        }
                        else
                        {
                            completionHander(false)
                        }
                    } catch let error as NSError {
                        UserDefaultsManager().setValueFor(isSign: false)
                        print("Failed to load: \(error.localizedDescription)")
                        completionHander(false)
                    }
                }
            } else {
                print("Failure")
                UserDefaultsManager().setValueFor(isSign: false)
                completionHander(false)
            }
        }
    }
    func getCurrentDay(appointmentDate: String) -> String {
        let currentDate = Utils.getCurrentDate(isRequest: false)
        var appointmentType = AppointmentType.past
        if currentDate <= appointmentDate {
            appointmentType = AppointmentType.scheduled
        }
        return appointmentType
    }
    func getAppointmentLockTime(appointmentTime: String) -> Bool {
        let currentTime = Utils.getCurrentTime()
        var isRescheduleAppointment = true
        if currentTime == appointmentTime {
            isRescheduleAppointment = false
        }
        return isRescheduleAppointment
    }
    func sortAppointment(completionHander: @escaping (Bool)->Void) {
        
        
        
        
        for (mainkey,value) in AppointmentsTypeCount
        {
            self.appointmentData.append(AppointmentData(title: mainkey, items: value))
        }
        
        
        
        completionHander(true)
    }
    
    func GetAppointmentInfo(AppointmentId: String, completionHander: @escaping (Bool)->Void) {
        let serverUrl = baseUrl + ServiceURI.appointmentPath
        let paramUri = "\(AppointmentId)"
        
        guard let serviceUrl = URL(string: serverUrl + paramUri) else {
            return
        }
        dLog("Service URL -> \(serviceUrl)")
        var urlReq = URLRequest(url: serviceUrl)
        urlReq.allHTTPHeaderFields = NetworkManager.getHeaders()
        urlReq.httpMethod = HTTPMethodType.getMethod.rawValue
        
        AF.request(urlReq).responseJSON { (response) in
            print("response \(response)")
            if response.error == nil {
                if let dataResponse = response.data {
                    do {
                        // make sure this JSON is in the format we expect
                        if let dictData = try JSONSerialization.jsonObject(with: dataResponse, options: []) as? [String: Any] {
                            if let isSuccess = dictData["success"] as? Bool, isSuccess {
                                if let arrResult = dictData["result"] as? [String:Any] {
                                    
                                    guard let jsonData = try? JSONSerialization.data(withJSONObject: arrResult, options: .prettyPrinted) else {
                                        return
                                    }
                                    
                                    self.appointmentInfoModel = try! JSONDecoder().decode(AppointmentModel.self, from: jsonData)
                                    completionHander(true)
                                }
                                print("Success")
                            } else {
                                UserDefaultsManager().setValueFor(isSign: false)
                                print("Failure ",dictData)
                                completionHander(false)
                            }
                        }
                    } catch let error as NSError {
                        UserDefaultsManager().setValueFor(isSign: false)
                        print("Failed to load: \(error.localizedDescription)")
                        completionHander(false)
                    }
                }
            } else {
                print("Failure")
                UserDefaultsManager().setValueFor(isSign: false)
                completionHander(false)
            }
        }
    }
    
    
    func CancelAppointmentList(AppointmentId: String, completionHander: @escaping (Bool)->Void) {
        let serverUrl = baseUrl + ServiceURI.cancelAppointment
        let paramUri = "\(AppointmentId)/cancel"
        
        guard let serviceUrl = URL(string: serverUrl + paramUri) else {
            return
        }
        dLog("Service URL -> \(serviceUrl)")
        var urlReq = URLRequest(url: serviceUrl)
        urlReq.allHTTPHeaderFields = NetworkManager.getHeaders()
        urlReq.httpMethod = HTTPMethodType.putMethod.rawValue
        
        AF.request(urlReq).responseJSON { (response) in
            print("response \(response)")
            if response.error == nil {
                if let dataResponse = response.data {
                    do {
                        // make sure this JSON is in the format we expect
                        if let dictData = try JSONSerialization.jsonObject(with: dataResponse, options: []) as? [String: Any] {
                            if let isSuccess = dictData["success"] as? Bool, isSuccess {
                                completionHander(true)
                            } else {
                                completionHander(false    )
                            }
                        }
                    } catch let error as NSError {
                        completionHander(false)
                    }
                }
            } else {
                print("Failure")
                UserDefaultsManager().setValueFor(isSign: false)
                completionHander(false)
            }
        }
    }
    
    
}


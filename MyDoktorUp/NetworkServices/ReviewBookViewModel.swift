//
//  ReviewBookViewModel.swift
//  MyDoktorUp
//
//  Created by Mac on 20/02/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import Alamofire


class VisitReasonsObserver : ObservableObject{
    
    @Published var arrVisitReasons = [VisitReason]()
    
    var ud = UserDefaultsManager()
    
    func getVisitResoons(isCommonAPI: Bool, isNewPatient: Bool)
    {
        guard let strProviderID = ud.providerID else {
            return
        }
        print(strProviderID)
        var strURL = baseUrl + ServiceURI.visitReasonPath + strProviderID
        
        if isCommonAPI {
            strURL = baseUrl + ServiceURI.visitReasonPathCommon
        }
        guard let url = URL(string: strURL) else {
            return
        }
        dLog("Service URL -> \(url)")
        let finalURL = isNewPatient ? url.appending("patient_type", value: "new_patient") : url
        var urlReq = URLRequest(url: finalURL)
        urlReq.allHTTPHeaderFields = NetworkManager.getHeaders()
        urlReq.httpMethod = HTTPMethodType.getMethod.rawValue
        
        AF.request(urlReq).responseJSON { (response) in
            if response.error == nil {
                if let dataResponse = response.data {
                    
                    do {
                        // make sure this JSON is in the format we expect
                        if let dictData = try JSONSerialization.jsonObject(with: dataResponse, options: []) as? [String: Any] {
                            if let isSuccess = dictData["success"] as? Bool, isSuccess {
                                if let arrResult = dictData["result"] as? [Any] {
                                    self.arrVisitReasons.removeAll()
                                    for i in 0..<arrResult.count{
                                        if let json = arrResult[i] as? [String:Any] {
                                            self.arrVisitReasons.append(VisitReason.initWith(dictInfo: json))
                                        }
                                    }
                                } else {
                                    print("Error")
                                    self.getVisitResoons(isCommonAPI: true, isNewPatient: false)
                                }
                            } else {
                                print("Error")
                                self.getVisitResoons(isCommonAPI: true, isNewPatient: false)
                            }
                        }
                    } catch let error as NSError {
                        print("Failed to load: \(error.localizedDescription)")
                        self.getVisitResoons(isCommonAPI: true, isNewPatient: false)
                    }
                }
            } else {
                print("Error")
                self.getVisitResoons(isCommonAPI: true, isNewPatient: false)
            }
        }
    }
    
}


class InsuranceCarrierObserver : ObservableObject {
    
    @Published var arrInsuranceList = [InsuranceCarrier]()
    var ud = UserDefaultsManager()
    
    func getInsuranceList(searchText:String,isCommonAPI: Bool, complitionHandler: @escaping (Bool)->Void)
    {
        guard let strProviderID = ud.providerID else {
            return
        }
        print(strProviderID)
        var strURL = baseUrl + ServiceURI.insuranceCarrierPath + strProviderID //Added Static to check flow, replace it with strProviderID
        if isCommonAPI {
            strURL = baseUrl + ServiceURI.insuranceCarrierPathCommon
        }
        
        if searchText != ""
        {
            strURL = strURL + "?search=\(searchText)"
        }
        guard let url = URL(string: strURL) else {
            return
        }
        
        
        print("Service URL ", url)
        var urlReq = URLRequest(url: url)
        urlReq.allHTTPHeaderFields = NetworkManager.getHeaders()
        urlReq.httpMethod = HTTPMethodType.getMethod.rawValue
        
        AF.request(urlReq).responseJSON { (response) in
            if let error = response.error {
                print(error)
                complitionHandler(false)
                return
            }
            guard let dataResponse = response.data else {
                complitionHandler(false)
                return
            }
            do {
                // make sure this JSON is in the format we expect
                guard let dictData = try JSONSerialization.jsonObject(with: dataResponse, options: []) as? [String: Any] else {
                    complitionHandler(false)
                    return
                }
                guard let isSuccess = dictData["success"] as? Bool else {
                    complitionHandler(false)
                    return
                }
                if isSuccess {
                    guard let dictResult = dictData["result"] as? [Any] else {
                        ReviewBookFormObserver.getErrorMessage(dictData: dictData)
                        if isCommonAPI {
                            complitionHandler(false)
                        } else {
                            self.getInsuranceList(searchText: "", isCommonAPI: true, complitionHandler: complitionHandler)
                        }
                        return
                    }
                    if dictResult.isEmpty {
                        ReviewBookFormObserver.getErrorMessage(dictData: dictData)
                        if isCommonAPI {
                            complitionHandler(false)
                        } else {
                            self.getInsuranceList(searchText: "", isCommonAPI: true, complitionHandler: complitionHandler)
                        }
                    } else {
                        print(dictResult)
                        self.arrInsuranceList.removeAll()
                        for i in 0..<dictResult.count {
                            if let arrResult = dictResult[i] as? [String:Any] {
                                self.arrInsuranceList.append(InsuranceCarrier.initWith(dictInfo: arrResult))
                                complitionHandler(true)
                            }
                        }
                    }
                } else {
                    ReviewBookFormObserver.getErrorMessage(dictData: dictData)
                    if isCommonAPI {
                        complitionHandler(false)
                    } else {
                        self.getInsuranceList(searchText: "", isCommonAPI: true, complitionHandler: complitionHandler)
                    }
                }
            } catch let error as NSError {
                print("Failed to load: \(error.localizedDescription)")
                if isCommonAPI {
                    complitionHandler(false)
                } else {
                    self.getInsuranceList(searchText: "", isCommonAPI: true, complitionHandler: complitionHandler)
                }
            }
        }
    }
}

class ReviewBookFormObserver : ObservableObject {
    
    @Published var isFormSubmittedSuccessfully = false
    @Published var isAPICalled = false
    
    func postAppointment(dictToPost:[String:Any], complitionHandler: @escaping (Bool, _ error: Error?)->Void)
    {
        let strURL = baseUrl + ServiceURI.appointmentPath
        guard let url = URL(string: strURL) else {
            return
        }
        dLog("Service URL -> \(url)")
        var urlReq = URLRequest(url: url)
        urlReq.allHTTPHeaderFields = NetworkManager.getHeaders()
        
        urlReq.httpMethod = HTTPMethodType.putMethod.rawValue
        print("  ---> ",dictToPost)
        let data = try! JSONSerialization.data(withJSONObject: dictToPost, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        if let json = json {
            urlReq.httpBody = json.data(using: String.Encoding.utf8.rawValue)
        }
        urlReq.timeoutInterval = 180
        
        
        AF.request(urlReq).responseJSON { (response) in
            if let error = response.error {
                print(error)
                complitionHandler(false, error)
                return
            }
            guard let dataResponse = response.data else {
                complitionHandler(false, nil)
                return
            }
            do {
                // make sure this JSON is in the format we expect
                guard let dictData = try JSONSerialization.jsonObject(with: dataResponse, options: []) as? [String: Any] else {
                    complitionHandler(false, nil)
                    return
                }
                guard let isSuccess = dictData["success"] as? Bool else {
                    complitionHandler(false, nil)
                    return
                }
                if isSuccess {
                    complitionHandler(true, nil)
                } else {
                    print(dictData)
                    guard let failResult = dictData["error"] as? [String:Any],
                        let errCode = failResult["code"],
                        let errMessage = failResult["message"] else {
                            let error = NSError(domain:"", code: 202, userInfo:[NSLocalizedDescriptionKey: "Invalid value"])
                            complitionHandler(false, error as Error)
                            return
                    }
                    print(errCode)
                    let error = NSError(domain:"", code: 202, userInfo:[NSLocalizedDescriptionKey: errMessage])
                    complitionHandler(false, error as Error)
                }
            } catch let error as NSError {
                print("Failed to load: \(error.localizedDescription)")
                let error = NSError(domain:"", code: error.code, userInfo:[NSLocalizedDescriptionKey: error.localizedDescription])
                complitionHandler(false, error as Error)
            }
        }
    }
    static func getErrorMessage(dictData: [String: Any]) {
        guard let failResult = dictData["error"] as? [String:Any],
            let errCode = failResult["code"],
            let errMessage = failResult["message"] else {
                return
        }
        print("Book Appointment Error Code is ", errCode)
        print("Book Appointment Error Message is ", errMessage)
    }
}

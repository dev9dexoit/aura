//
//  UserDefaultObserver.swift
//  MyDoktorUp
//
//  Created by Mac on 14/02/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation

class UserDefaultsManager: ObservableObject {
    @Published var providerID = UserDefaults.standard.value(forKey: UserDefault.udProviderId) as? String {
        didSet { UserDefaults.standard.set(self.providerID, forKey: UserDefault.udProviderId) }
    }
    
    @Published var doctorInfo = UserDefaults.standard.value(forKey: UserDefault.doctorInfo) as? String {
        didSet { UserDefaults.standard.set(self.doctorInfo, forKey: UserDefault.doctorInfo) }
    }
    
    @Published var userFullName = UserDefaults.standard.value(forKey: UserDefault.userFullName) as? String {
        didSet { UserDefaults.standard.set(self.userFullName, forKey: UserDefault.userFullName) }
    }
    
    @Published var userEmail = UserDefaults.standard.value(forKey: UserDefault.userEmail) as? String {
        didSet { UserDefaults.standard.set(self.userEmail, forKey: UserDefault.userEmail) }
    }
    
    @Published var userPhoneNumber = UserDefaults.standard.value(forKey: UserDefault.userPhoneNumber) as? String {
           didSet { UserDefaults.standard.set(self.userPhoneNumber , forKey: UserDefault.userPhoneNumber) }
       }
    
    @Published var userdob = UserDefaults.standard.value(forKey: UserDefault.userdob) as? String {
        didSet { UserDefaults.standard.set(self.userdob , forKey: UserDefault.userdob) }
    }
    @Published var userzip = UserDefaults.standard.value(forKey: UserDefault.userzip) as? String {
        didSet { UserDefaults.standard.set(self.userzip , forKey: UserDefault.userzip) }
    }
    
    @Published var usercity = UserDefaults.standard.value(forKey: UserDefault.usercity) as? String {
        didSet { UserDefaults.standard.set(self.usercity , forKey: UserDefault.usercity) }
    }
    
    @Published var userstate = UserDefaults.standard.value(forKey: UserDefault.userstate) as? String {
           didSet { UserDefaults.standard.set(self.userstate , forKey: UserDefault.userstate) }
       }
    @Published var useraddress1 = UserDefaults.standard.value(forKey: UserDefault.useraddress1) as? String {
        didSet { UserDefaults.standard.set(self.useraddress1 , forKey: UserDefault.useraddress1) }
    }
    @Published var useraddress2 = UserDefaults.standard.value(forKey: UserDefault.useraddress2) as? String {
        didSet { UserDefaults.standard.set(self.useraddress2 , forKey: UserDefault.useraddress2) }
    }
    
    @Published var user_Insu_Front = UserDefaults.standard.value(forKey: UserDefault.Insurance.FrontImage) as? String {
        didSet { UserDefaults.standard.set(self.user_Insu_Front , forKey: UserDefault.Insurance.FrontImage) }
    }
    
    @Published var user_Insu_Back = UserDefaults.standard.value(forKey: UserDefault.Insurance.backImage) as? String {
        didSet { UserDefaults.standard.set(self.user_Insu_Front , forKey: UserDefault.Insurance.backImage) }
    }
    
    
    @Published var user_type = UserDefaults.standard.value(forKey: UserDefault.user_type) as? String {
        didSet { UserDefaults.standard.set(self.user_type , forKey: UserDefault.user_type) }
    }
    
    @Published var user_is_Active = UserDefaults.standard.value(forKey: UserDefault.is_Active) as? String {
        didSet { UserDefaults.standard.set(self.user_is_Active , forKey: UserDefault.is_Active) }
    }
    
    @Published var user_last_login = UserDefaults.standard.value(forKey: UserDefault.last_login) as? String {
        didSet { UserDefaults.standard.set(self.user_last_login , forKey: UserDefault.last_login) }
    }
    
    @Published var user_created_at = UserDefaults.standard.value(forKey: UserDefault.created_at) as? String {
        didSet { UserDefaults.standard.set(self.user_created_at , forKey: UserDefault.created_at) }
    }
    
    @Published var user_update_at = UserDefaults.standard.value(forKey: UserDefault.update_at) as? String {
        didSet { UserDefaults.standard.set(self.user_update_at , forKey: UserDefault.update_at) }
    }
    
    @Published var user_ethnicity = UserDefaults.standard.value(forKey: UserDefault.user_ethnicity) as? String {
        didSet { UserDefaults.standard.set(self.user_ethnicity , forKey: UserDefault.user_ethnicity) }
    }
    
    @Published var user_gender = UserDefaults.standard.value(forKey: UserDefault.user_gender) as? String {
        didSet { UserDefaults.standard.set(self.user_gender , forKey: UserDefault.user_gender) }
    }
    
    @Published var user_insurance = UserDefaults.standard.value(forKey: UserDefault.user_insurance) as? String {
        didSet { UserDefaults.standard.set(self.user_insurance , forKey: UserDefault.user_insurance) }
    }
    
    
    @Published var user_language = UserDefaults.standard.value(forKey: UserDefault.user_language) as? String {
        didSet { UserDefaults.standard.set(self.user_language , forKey: UserDefault.user_language) }
    }
    
    @Published var user_marital_status = UserDefaults.standard.value(forKey: UserDefault.user_marital_status) as? String {
        didSet { UserDefaults.standard.set(self.user_marital_status , forKey: UserDefault.user_marital_status) }
    }
    
    @Published var user_martial = UserDefaults.standard.value(forKey: UserDefault.user_martial) as? String {
        didSet { UserDefaults.standard.set(self.user_martial , forKey: UserDefault.user_martial) }
    }
        
    @Published var user_race = UserDefaults.standard.value(forKey: UserDefault.user_race) as? String {
        didSet { UserDefaults.standard.set(self.user_race , forKey: UserDefault.user_race) }
    }
        
    func setValueFor(provider: Provider) {
        guard let providerId = provider.id else {
            return
        }
        self.providerID = providerId
        UserDefaults.standard.set(providerId, forKey: UserDefault.udProviderId)
        self.doctorInfo = Utils.getProviderFullname(provider)
    }
    
    @Published var isSign = UserDefaults.standard.value(forKey: UserDefault.loggedInUser) as? Bool {
        didSet { UserDefaults.standard.set(self.isSign, forKey: UserDefault.loggedInUser) }
    }
    func setValueFor(isSign: Bool) {
        self.isSign = isSign
        UserDefaults.standard.set(isSign, forKey: UserDefault.loggedInUser)
    }
    func setSelectedDocktor(Id: String) {
        
        UserDefaults.standard.set(Id, forKey: UserDefault.SelectedDoctor)
    }
    @Published var loginSessionId = UserDefaults.standard.value(forKey: UserDefault.userSessionId) as? String {
        didSet { UserDefaults.standard.set(self.loginSessionId, forKey: UserDefault.userSessionId) }
    }
    
    
    @Published var loginType = UserDefaults.standard.value(forKey: UserDefault.loginType) as? String {
        didSet { UserDefaults.standard.set(self.loginType, forKey: UserDefault.loginType) }
    }
    
    @Published var loginUserId = UserDefaults.standard.value(forKey: UserDefault.userId) as? String {
        didSet { UserDefaults.standard.set(self.loginUserId, forKey: UserDefault.userId) }
    }
    
    @Published var doctorFullName = UserDefaults.standard.value(forKey: UserDefault.doctorFullName) as? String {
        didSet { UserDefaults.standard.set(self.doctorFullName, forKey: UserDefault.doctorFullName) }
    }
    
    
    func setValueFor(user: User) {
        guard let sessionId = user.session_id else {
            return
        }
      
        guard let loginEmail = user.email else {
            return
        }
        
        guard let loginUesrID = user.id else {
            return
        }

        self.loginSessionId = sessionId
        self.userEmail = loginEmail
        self.loginUserId = loginUesrID

        UserDefaults.standard.set(sessionId, forKey: UserDefault.userSessionId)
        self.userFullName = Utils.getUserFullname(user)
        self.userEmail = user.email ?? ""
    }
}

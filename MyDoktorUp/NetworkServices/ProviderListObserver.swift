//
//  ProviderListObserver.swift
//  MyDoktorUp
//
//  Created by Mac on 13/02/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import Alamofire

class ProviderListObserver : ObservableObject{
    
    @Published var arrProviders = [Provider]()
    
    func getProvidersByPhone(strPhone: String, complitionHandler: @escaping (Bool)->Void)
    {
        self.arrProviders.removeAll()
        let strURL = baseUrl + ServiceURI.providerPath
        guard let url = URL(string: strURL) else {
            return
        }
       
        let finalURL = url.appending("phone", value: strPhone)
        dLog("finalURL ---> \(finalURL)")
        var urlReq = URLRequest(url: finalURL)
        urlReq.allHTTPHeaderFields = NetworkManager.getHeaders()
        urlReq.httpMethod = HTTPMethodType.getMethod.rawValue
        
        AF.request(urlReq).responseJSON { (response) in
            if response.error == nil {
                if let dataResponse = response.data {
                    
                    do {
                        // make sure this JSON is in the format we expect
                        if let dictData = try JSONSerialization.jsonObject(with: dataResponse, options: []) as? [String: Any] {
                            if let isSuccess = dictData["success"] as? Bool, isSuccess {
                                if let arrResult = dictData["result"] as? [Any] {
                                    for i in 0..<arrResult.count{
                                        if let dictResult = arrResult[i] as? [String:Any] {
                                            self.arrProviders.append(Provider.initWith(dictInfo: dictResult))
                                        }
                                    }
                                }
                                complitionHandler(true)
                            } else {
                                complitionHandler(false)
                            }
                        }
                    } catch let error as NSError {
                        dLog("Error ---> \(error.localizedDescription)")
                        complitionHandler(false)
                    }
                }
            } else {
                complitionHandler(false)
            }
        }
    }
}


extension URL {

    func appending(_ queryItem: String, value: String?) -> URL {

        guard var urlComponents = URLComponents(string: absoluteString) else { return absoluteURL }

        // Create array of existing query items
        var queryItems: [URLQueryItem] = urlComponents.queryItems ??  []

        // Create query item
        let queryItem = URLQueryItem(name: queryItem, value: value)

        // Append the new query item in the existing query items array
        queryItems.append(queryItem)

        // Append updated query items array in the url component object
        urlComponents.queryItems = queryItems

        // Returns the url from new url components
        return urlComponents.url!
    }
}

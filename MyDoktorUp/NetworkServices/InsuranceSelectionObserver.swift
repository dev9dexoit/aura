//
//  InsuranceSelectionObserver.swift
//  MyDoktorUp
//
//  Created by Mac on 17/04/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
class InsuranceSelectionObserver: ObservableObject {

    @Published var insuranceName: String = ""
    
    func setInsuranceName(name: String) {
        self.insuranceName = name
    }
}

//
//  SignInObserver.swift
//  MyDoktorUp
//
//  Created by SHREE SAHANA on 4/19/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import Combine
import Alamofire


class SignUpObserver : ObservableObject {
    let objectWillChange = PassthroughSubject<Void,Never>()
    @Published var userInfo = User()
    func getRequestBody(email:String,first_name:String,last_name:String,username: String, password: String) -> [String: String] {
        var requestBody: [String: String] = [:]
        requestBody["user_name"] = username
        requestBody["email"] = email
        requestBody["password"] = password
        requestBody["first_name"] = first_name
        requestBody["last_name"] = last_name
        print("Request Body  \(requestBody)")
        return requestBody
    }

    
    
    func signUpViaEmail(email:String,firstname:String,lastname:String,username: String, password: String, completionHander: @escaping ([String: Any])->Void) {
        let serverUrl = baseUrl + ServiceURI.userSignUp
        guard let serviceUrl = URL(string: serverUrl) else {
            return
        }
        dLog("Service URL -> \(serviceUrl)")
        var urlReq = URLRequest(url: serviceUrl)
        urlReq.allHTTPHeaderFields = NetworkManager.getHeaders()
        urlReq.httpMethod = HTTPMethodType.postMethod.rawValue
        let requestBodyDict = self.getRequestBody(email: email, first_name: firstname, last_name: lastname, username: username, password: password)
        print("requestBodyDict \(requestBodyDict)")
        let data = try! JSONSerialization.data(withJSONObject: requestBodyDict, options: JSONSerialization.WritingOptions.prettyPrinted)
        let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        if let json = json {
            urlReq.httpBody = json.data(using: String.Encoding.utf8.rawValue)
        }
        print("urlReq \(urlReq)")
        AF.request(urlReq).responseJSON { (response) in
            if response.error == nil {
                if let dataResponse = response.data {
                    do {
                        // make sure this JSON is in the format we expect
                        if let dictData = try JSONSerialization.jsonObject(with: dataResponse, options: []) as? [String: Any] {
                            if let isSuccess = dictData["success"] as? Bool, isSuccess {
                                if let arrResult = dictData["result"] {
                                    print(arrResult)
                                    guard let jsonData = try? JSONSerialization.data(withJSONObject: arrResult, options: .prettyPrinted) else {
                                        return
                                    }
                                    self.userInfo = try! JSONDecoder().decode(User.self, from: jsonData)
//                                    self.userInfo.type =  LoginTypes.Email
                                    
                                    
                                    
                                    
                                    UserDefaultsManager().setValueFor(isSign: true)
                                    UserDefaultsManager().setValueFor(user: self.userInfo)
                                    print(self.userInfo)
                                }
                                print("Success")
                                completionHander(dictData)
                            } else {
                                completionHander(dictData)
                            }
                        }
                    } catch let error as NSError {
                        print("Failed to load: \(error.localizedDescription)")
                        
                    }
                }
            } else {
                print("Failure")
                
            }
        }
    }
    
}

//
//  ChekingAppointment_Observer.swift
//  MyDoktorUp
//
//  Created by MACBOOK PRO on 17/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//


import Foundation
import Combine
import Alamofire

class ChekingAppointment_Observer : ObservableObject {
    
    
    
    
    func setChekingAppoitmentStep(Checkingparamters:[Any],Infoparamters:[String:String],id:String,type:String, completionHander: @escaping (Bool)->Void) {
        let serverUrl = baseUrl + ServiceURI.appointmentPath
        var paramUri = ""
        paramUri = "\(id)\(ServiceURI.ChekingAppointment)?type=\(type)"
        guard let serviceUrl = URL(string: serverUrl + paramUri) else {
            return
        }
        
        var urlReq = URLRequest(url: serviceUrl)
        urlReq.allHTTPHeaderFields = NetworkManager.getHeaders()
        urlReq.httpMethod = HTTPMethodType.putMethod.rawValue
        
        
        if type == "info"
        {

            let data = try! JSONSerialization.data(withJSONObject: Infoparamters, options: JSONSerialization.WritingOptions.prettyPrinted)
            let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
            if let json = json {
                urlReq.httpBody = json.data(using: String.Encoding.utf8.rawValue)
            }
        }
        else if type == "consent"
        {

            let data = try! JSONSerialization.data(withJSONObject: Checkingparamters, options: JSONSerialization.WritingOptions.prettyPrinted)
            let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
            if let json = json {
                urlReq.httpBody = json.data(using: String.Encoding.utf8.rawValue)
            }
        }
        
        
        
        
        AF.request(urlReq).responseJSON { (response) in
            print("response \(response)")
            if response.error == nil {
                if let dataResponse = response.data {
                    do {
                        // make sure this JSON is in the format we expect
                        if let dictData = try JSONSerialization.jsonObject(with: dataResponse, options: []) as? [String: Any] {
                            if let isSuccess = dictData["success"] as? Bool, isSuccess {
                               
                                completionHander(true)
                                print("Success")
                            } else {
                                print("Failure ",dictData)
                                completionHander(false)
                            }
                        }
                    } catch let error as NSError {
                        
                        completionHander(false)
                    }
                }
            } else {
                
                
                completionHander(false)
            }
        }
    }
    
    func postInsurance(Frontimage:UIImage,Backimage:UIImage,completionHander: @escaping (Bool)->Void)
    {
        let frontimage = Frontimage.jpegData(compressionQuality: 0.2)!
        let backimage = Backimage.jpegData(compressionQuality: 0.2)!
        
        
        let serverUrl = baseUrl + ServiceURI.form + ServiceURI.PostInsuranceCard
        
        
        guard let serviceUrl = URL(string: serverUrl) else {
            return
        }
        
        var urlReq = URLRequest(url: serviceUrl)
        urlReq.httpMethod = HTTPMethodType.postMethod.rawValue
        urlReq.allHTTPHeaderFields = NetworkManager.getHeaders()
        
        AF.upload(multipartFormData: { multiPart in
            
            multiPart.append(frontimage, withName: "front_ic" , fileName: "front_ic.jpeg", mimeType: "image/jpeg")
            multiPart.append(backimage, withName: "back_ic" , fileName: "back_ic.jpeg", mimeType: "image/jpeg")
            
        }, with: urlReq)
            
            .responseJSON(completionHandler: { data in
                print("Data insurance \(data)")
                //Do what ever you want to do with response
            })
        
    }
    
    
    func postDrivingLicense(Frontimage:UIImage,Backimage:UIImage,completionHander: @escaping (Bool)->Void)
    {
        let frontimage = Frontimage.jpegData(compressionQuality: 0.2)!
        let backimage = Backimage.jpegData(compressionQuality: 0.2)!
        
        let serverUrl = baseUrl + ServiceURI.form + ServiceURI.PostdrivingLicense
        
        
        guard let serviceUrl = URL(string: serverUrl) else {
            return
        }
        
        var urlReq = URLRequest(url: serviceUrl)
        urlReq.httpMethod = HTTPMethodType.postMethod.rawValue
        urlReq.allHTTPHeaderFields = NetworkManager.getHeaders()
        
        
        AF.upload(multipartFormData: { multiPart in
            
            
            multiPart.append(frontimage, withName: "front_ic" , fileName: "front_ic.jpeg", mimeType: "image/jpeg")
            multiPart.append(backimage, withName: "back_ic" , fileName: "back_ic.jpeg", mimeType: "image/jpeg")
            
        }, with: urlReq)
            
            .responseJSON(completionHandler: { data in
                print(" driving Data \(data)")
                //Do what ever you want to do with response
            })
        
    }
    
}

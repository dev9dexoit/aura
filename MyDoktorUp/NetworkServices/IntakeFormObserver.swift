//
//  IntakeFormObserver.swift
//  MyDoktorUp
//
//  Created by Mac on 30/03/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation


class IntakeFormObserver: ObservableObject {
    
    @Published var intakeForm: IntakeFormModel?
    
    func parceJsonData() {
        do {
            
            if let file = Bundle.main.url(forResource: "intakeform", withExtension: "json") {
                
                let path = Bundle.main.path(forResource: "intakeform", ofType: "json")
                let string = try String(contentsOfFile: path!, encoding: String.Encoding.utf8)
                print(string)
                intakeForm = try JSONDecoder().decode(IntakeFormModel.self, from: try Data(contentsOf: file))
            }
            
            print(self.intakeForm!)
             
        } catch let error as NSError {
            print("Failed to load: \(error.localizedDescription)")
        }
    }
}

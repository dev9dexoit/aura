//
//  ChangePasswordObserver.swift
//  MyDoktorUp
//
//  Created by Mac on 31/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import Alamofire

class ChangePasswordObserver : ObservableObject {
    
    @Published var isChangedPassword = false
    @Published var isAPICalled = false
    
    func changePassword(dictToPost: [String: Any], complitionHandler: @escaping (Bool, _ error: Error?) -> Void) {
        let strURL = baseUrl + ServiceURI.changePassword
        guard let url = URL(string: strURL) else {
            return
        }
        dLog("Service URL -> \(url)")
        var urlReq = URLRequest(url: url)
        urlReq.allHTTPHeaderFields = NetworkManager.getHeaders()
        
        urlReq.httpMethod = HTTPMethodType.putMethod.rawValue
        print("  ---> ",dictToPost)
        let data = try! JSONSerialization.data(withJSONObject: dictToPost, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        if let json = json {
            urlReq.httpBody = json.data(using: String.Encoding.utf8.rawValue)
        }
        urlReq.timeoutInterval = 180
        
        
        AF.request(urlReq).responseJSON { (response) in
            if let error = response.error {
                print(error)
                complitionHandler(false, error)
                return
            }
            guard let dataResponse = response.data else {
                complitionHandler(false, nil)
                return
            }
            do {
                // make sure this JSON is in the format we expect
                guard let dictData = try JSONSerialization.jsonObject(with: dataResponse, options: []) as? [String: Any] else {
                    complitionHandler(false, nil)
                    return
                }
                guard let isSuccess = dictData["success"] as? Bool else {
                    complitionHandler(false, nil)
                    return
                }
                if isSuccess {
                    complitionHandler(true, nil)
                } else {
                    print(dictData)
                    guard let failResult = dictData["error"] as? [String:Any],
                        let errCode = failResult["code"],
                        let errMessage = failResult["message"] else {
                            let error = NSError(domain:"", code: 202, userInfo:[NSLocalizedDescriptionKey: "Invalid value"])
                            complitionHandler(false, error as Error)
                            return
                    }
                    print(errCode)
                    let error = NSError(domain:"", code: 202, userInfo:[NSLocalizedDescriptionKey: errMessage])
                    complitionHandler(false, error as Error)
                }
            } catch let error as NSError {
                print("Failed to load: \(error.localizedDescription)")
                let error = NSError(domain:"", code: error.code, userInfo:[NSLocalizedDescriptionKey: error.localizedDescription])
                complitionHandler(false, error as Error)
            }
        }
    }
    static func getErrorMessage(dictData: [String: Any]) {
        guard let failResult = dictData["error"] as? [String:Any],
            let errCode = failResult["code"],
            let errMessage = failResult["message"] else {
                return
        }
        print("Book Appointment Error Code is ", errCode)
        print("Book Appointment Error Message is ", errMessage)
    }
}

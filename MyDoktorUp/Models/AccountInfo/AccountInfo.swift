//
//  AccountInfo.swift
//  MyDoktorUp
//
//  Created by Mac on 18/06/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation

struct UpdateAccountInfo {
    var profile_attributes: ProfileAttributes
    var special_attributes: SpecialAttributes
    var practice_attributes: PracticeAttributes
    var user_booking_forself: [UserBookingForself]
}

struct ProfileAttributes {
    var last_name: String
    var first_name: String
    var address1: String
    var address2: String
    var dob: String
    var zip: String
    var city: String
    var state: String
}

struct SpecialAttributes {
    var gender: String
    var marital_status: String
    var language: String
    var race: String
    var ethnicity: String
}

struct PracticeAttributes {
    var insurance: String
}

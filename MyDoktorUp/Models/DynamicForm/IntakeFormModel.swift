//
//  IntakeFormModel.swift
//  MyDoktorUp
//
//  Created by Mac on 30/03/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation


import Foundation

class IntakeFormModel: Codable, Identifiable {
    var datatype, providerid: String
    var providerformdata: [ProviderFormData]
    
    init(datatype: String, providerid: String, providerformdata: [ProviderFormData]) {
        self.datatype = datatype
        self.providerid = providerid
        self.providerformdata = providerformdata
    }
}

class ProviderFormData: Codable, Identifiable {
    
    var formname, formdescription: String
    var formcontent: [FormContent]
    
    init(formname: String, formdescription: String, formcontent: [FormContent]) {
        self.formname = formname
        self.formdescription = formdescription
        self.formcontent = formcontent
    }
}

class FormContent: Codable, Identifiable {
    var helptext: String?
    var fielddata: FidelData
    var minage: Int?
    var maxage: Int?
    var populatedfrom: String
    var name: String
    var populated: String
    var questions: [QuestionN]
    var portalformsectionid: Int
    var ageunit: String
    var empty: Bool
    var type: String
    var complete: Bool

    init(helptext: String?, fielddata: FidelData, minage: Int?, maxage: Int?, populatedfrom: String, name: String, populated: String, questions: [QuestionN], portalformsectionid: Int, ageunit: String, empty: Bool, type: String, complete: Bool) {
        self.helptext = helptext
        self.fielddata = fielddata
        self.minage = minage
        self.maxage = maxage
        self.populatedfrom = populatedfrom
        self.name = name
        self.populated = populated
        self.questions = questions
        self.portalformsectionid = portalformsectionid
        self.ageunit = ageunit
        self.empty = empty
        self.type = type
        self.complete = complete
    }
    
}

class FidelData: Codable {
    
    var defaultfielddata: DefaultFieldData?
    var administered: AdministeredN?
    var onset: Bool?
    var deactivateddate: DeactivatedDate?
    var allergyreactionid: AllergyReactionId?
    var reaction: Bool?
    var allergyid: AllergyId?
    var onsetdate: OnSetDate?
    var allergyconcepttype: AllergyConceptType?
    var severityid: SeverityId?
    var allergycode: AllergyCode?
    
    init(defaultfielddata: DefaultFieldData?, administered: AdministeredN?, onset: Bool?, deactivateddate: DeactivatedDate?, allergyreactionid: AllergyReactionId?, reaction: Bool?, allergyid: AllergyId?, onsetdate: OnSetDate?, allergyconcepttype: AllergyConceptType?, severityid: SeverityId?, allergycode: AllergyCode?) {
        
        self.defaultfielddata = defaultfielddata
        self.administered = administered
        self.onset = onset
        self.deactivateddate = deactivateddate
        self.allergyreactionid = allergyreactionid
        self.reaction = reaction
        self.allergyid = allergyid
        self.onsetdate = onsetdate
        self.allergyconcepttype = allergyconcepttype
        self.severityid = severityid
        self.allergycode = allergycode
    }
}

class DefaultFieldData: Codable {
    var inputtype: String?
}

class AdministeredN: Codable {
    var required: Int?
    var inputtype: String?
    var list: Int?
}

class DeactivatedDate: Codable {
    var inputtype: String?
}

class AllergyReactionId: Codable {
    var inputtype: String?
    var dropdownvalues: [[String]]?
    var list: Int?
}

class AllergyId: Codable {
    
    var required: Int?
    var inputtype: String?
    
    init(required: Int?, inputtype: String?) {
        self.required = required
        self.inputtype = inputtype
    }
}

class OnSetDate: Codable {
    
    var inputtype: String?
    
    init(inputtype: String?) {
        self.inputtype = inputtype
    }
}

class AllergyConceptType: Codable {
    
    var inputtype: String?
    var dropdownvalues: [[String]]?
    
    init(inputtype: String?, dropdownvalues:[[String]]?) {
        self.inputtype = inputtype
        self.dropdownvalues = dropdownvalues
    }
}

class SeverityId: Codable {
    
    var inputtype: String?
    var dropdownvalues: [DropdownServiceId]?
    var list: Int?
    
    init(inputtype: String?, dropdownvalues: [DropdownServiceId]?, list: Int?) {
        self.inputtype = inputtype
        self.dropdownvalues = dropdownvalues
        self.list = list
    }
}

class AllergyCode: Codable {
    
    var inputtype: String?
    
    init(inputtype: String?) {
        self.inputtype = inputtype
    }
}

class DropdownServiceId: Codable {
    var parentcode, ordering, id, snomedcode, description: String?
    
    init(parentcode: String?, ordering: String?, id: String?, snomedcode: String?, description: String?) {
        self.parentcode = parentcode
        self.ordering = ordering
        self.id = id
        self.snomedcode = snomedcode
        self.description = description
    }
}


class QuestionN: Codable, Identifiable {
    
    var fields: Fields?
    var portalformquestionid, text, cvx: String?
    var inputtype, clinicalelementid: String?
    var dropdownvalues: [[String]]?
    
    init(fields: Fields?, portalformquestionid: String?, text: String?, cvx: String?, inputtype: String?, clinicalelementid: String?, dropdownvalues:[[String]]?) {
        self.fields = fields
        self.portalformquestionid = portalformquestionid
        self.text = text
        self.cvx = cvx
        self.inputtype = inputtype
        self.clinicalelementid = clinicalelementid
        self.dropdownvalues = dropdownvalues
    }
}

class Fields: Codable {
    
    var defaultField: String?
    
    init(defaultField: String?) {
        self.defaultField = defaultField
    }
}

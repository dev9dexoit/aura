//
//  ProviderSlots.swift
//  MyDoktorUp
//
//  Created by Mac on 18/02/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation

struct ProviderSlots: Identifiable {
    
    public var id = UUID()
    public var date: String?
    public var slots: [AppointmentInfo] = []
    
    static func initWith(dictInfo: [String:Any]) -> ProviderSlots {
        
        var providerSlotObj = ProviderSlots()
        
        if let date = dictInfo["date"] as? String {
            providerSlotObj.date = date
        }
        if let arrSlots = dictInfo["slots"] as? [Any] {
            for case let dictSlot as [String:Any] in arrSlots {
                providerSlotObj.slots.append(AppointmentInfo.initWith(dictInfo: dictSlot))
            }
        }
        
        return providerSlotObj
    }
}


struct AppointmentInfo: Identifiable {
    
    public var id = UUID()
    public var appointment_id: String?
    public var appointment_date: String?
    public var appointment_time: String?
    public var appointment_type_id: String?
    public var department_id: String?
    public var duration: String?
    public var provider_id: String?
    
    static func initWith(dictInfo:[String:Any]) -> AppointmentInfo {
        var appointmentObj = AppointmentInfo()
        
        if let appointment_id = dictInfo["appointment_id"] as? String {
            appointmentObj.appointment_id = appointment_id
        }
        if let appointment_date = dictInfo["appointment_date"] as? String {
            appointmentObj.appointment_date = appointment_date
        }
        if let appointment_time = dictInfo["appointment_time"] as? String {
            appointmentObj.appointment_time = appointment_time
        }
        if let appointment_type_id = dictInfo["appointment_type_id"] as? String {
            appointmentObj.appointment_type_id = appointment_type_id
        }
        if let department_id = dictInfo["department_id"] as? String {
            appointmentObj.department_id = department_id
        }
        if let duration = dictInfo["duration"] as? String {
            appointmentObj.duration = duration
        }
        if let provider_id = dictInfo["provider_id"] as? String {
            appointmentObj.provider_id = provider_id
        }
        return appointmentObj
    }
}

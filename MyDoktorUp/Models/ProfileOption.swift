//
//  ProfileOption.swift
//  MyDoktorUp
//
//  Created by Mac on 05/02/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI

struct ProfileOption: Hashable, Codable, Identifiable {
    
    var id: Int
    var title: String
}

//
//  Provider.swift
//  MyDoktorUp
//
//  Created by Mac on 13/02/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation


struct Provider: Identifiable {
    public var npi: String?
    public var id: String?
    public var first_name: String?
    public var last_name: String?
    public var zip: String?
    public var address1: String?
    public var address2: String?
    public var city: String?
    public var state: String?
    public var credential: String?
    public var taxonomy_code: String?
    public var latitude: String?
    public var longitude: String?
    public var mailing_phone: String?
    public var practice_phone: String?
    public var rating: Rating?
    public var reviews : Review?
    public var photos: [Photos]?
    public var insurance_carriers:String?

    public var professional_statement: String?
    public var taxonomy_name : String?
    public var distance : String?
    public var virtual : Bool?

    
    
    static func initWith(dictInfo:[String:Any]) -> Provider {
        var providerObj = Provider()
        if let npi = dictInfo["npi"] as? String {
            providerObj.npi = npi
        }
        if let id = dictInfo["id"] as? String {
            providerObj.id = id
        }
        providerObj.first_name = dictInfo["first_name"] as? String ?? ""
        providerObj.last_name = dictInfo["last_name"] as? String ?? ""
        
        if let zip = dictInfo["zip"] as? String {
            providerObj.zip = zip
        }

        providerObj.address1 = dictInfo["address1"] as? String ?? "-"
        providerObj.address2 = dictInfo["address2"] as? String ?? ""
        
        if let city = dictInfo["city"] as? String {
            providerObj.city = city
        }
        if let state = dictInfo["state"] as? String {
            providerObj.state = state
        }
        if let credential = dictInfo["credential"] as? String {
            providerObj.credential = credential
        }
        if let taxonomy_code = dictInfo["taxonomy_code"] as? String {
            providerObj.taxonomy_code = taxonomy_code
        }
        if let latitude = dictInfo["latitude"] as? String {
            providerObj.latitude = latitude
        }
        if let longitude = dictInfo["longitude"] as? String {
            providerObj.longitude = longitude
        }
        
        providerObj.mailing_phone = dictInfo["mailing_phone"] as? String ?? "000-000-0000"
        providerObj.practice_phone = dictInfo["practice_phone"] as? String ?? "000-000-0000"
        
        if let rating = dictInfo["rating"] as? [String:Any] {
            providerObj.rating = Rating.initWith(dictInfo: rating)
        }
        if let reviews = dictInfo["reviews"] as? [String:Any] {
            providerObj.reviews = Review.initWith(dictInfo: reviews)
        }
        if let photos = dictInfo["photos"] as? [Any] {
            providerObj.photos = Photos.initWith(arrData: photos)
        }
        providerObj.insurance_carriers = dictInfo["insurance_carriers"] as? String ?? "-"

        providerObj.professional_statement = dictInfo["professional_statement"] as? String ?? ""
        providerObj.virtual = dictInfo["virtual"] as? Bool ?? false

        return providerObj
    }
}

struct Rating: Identifiable {
    public var id: String?
    public var overall: Double?
    public var wait_times: Double?
    public var bedside_manner: Double?
    public var strOverall:String?
    
    static func initWith(dictInfo:[String:Any]) -> Rating {
        var ratingObj = Rating()
        if let id = dictInfo["id"] as? String {
            ratingObj.id = id
        }
        if let overall = dictInfo["overall"] as? Double {
            ratingObj.overall = overall
            ratingObj.strOverall = "\(overall)"
        }
        if let wait_times = dictInfo["wait_times"] as? Double {
            ratingObj.wait_times = wait_times
        }
        if let bedside_manner = dictInfo["bedside_manner"] as? Double {
            ratingObj.bedside_manner = bedside_manner
        }
        return ratingObj
    }
}

struct Review: Identifiable {
    public var id: String?
    public var total: Int?
    public var totalReview: String?
    
    static func initWith(dictInfo:[String:Any]) -> Review {
        var reviewObj = Review()
        if let id = dictInfo["id"] as? String {
            reviewObj.id = id
        }
        if let total = dictInfo["total"] as? Int {
            reviewObj.total = total
            
            if total > 0 {
                if total > 1 {
                    reviewObj.totalReview = "\(total) Reviews"
                } else {
                    reviewObj.totalReview = "\(total) Review"
                }
            } else {
                reviewObj.totalReview = "0 Review"
            }
        }
        
        return reviewObj
    }
}


struct Photos: Identifiable {
    public var id: Int?
    public var photo_url: String?
    
    static func initWith(arrData:[Any]) -> [Photos] {
        let photoObj = Photos()
        return [photoObj]
    }
}


//
//  InsuranceListView.swift
//  MyDoktorUp
//
//  Created by Mac on 16/04/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI

protocol InsuranceDelegate {
    func didSelectInsurance(index: Int)
}

struct InsuranceListView: View {
    @ObservedObject var observedInsurance = InsuranceCarrierObserver()
    
    @Environment(\.presentationMode) var presentationMode
    
    @State private var searchText : String = ""
    @State var insuranceNames: [String] = []
    
    @State var insuranceDelegate: InsuranceDelegate?
    @State var isShowing = false
    
    
    var body: some View {
        LoadingView(isShowing: .constant(isShowing)) {
            VStack {
                SearchBar(text: self.$searchText, placeholder: "Search Insurance",onTextChanged:self.searchinsuranceName)
                List{
                    ForEach(self.insuranceNames.filter {
                        self.searchText.isEmpty ? true : $0.lowercased().contains(self.searchText.lowercased())
                    }, id: \.self) { insuranceName in
                        Button(action: {
                            if self.insuranceDelegate != nil {
                                self.insuranceDelegate!.didSelectInsurance(index: self.insuranceNames.firstIndex(of: insuranceName)!)
                                self.presentationMode.wrappedValue.dismiss()
                            }
                        }) {
                            Text(insuranceName)
                        }
                    }
                }
            }
        }
    }
    func searchinsuranceName(for searchText: String) {
        if !searchText.isEmpty {
            self.isShowing = true
            self.observedInsurance.getInsuranceList(searchText:searchText,isCommonAPI: true, complitionHandler: { (isSuccess) in
                if isSuccess {
                    self.isShowing = false
                    self.insuranceNames.removeAll()
                    for case let insurance as InsuranceCarrier in self.observedInsurance.arrInsuranceList {
                        self.insuranceNames.append(insurance.name ?? "")
                    }
                }else
                {
                    self.isShowing = false
                    
                }
            })
        }
        
    }
    
}

struct InsuranceListView_Previews: PreviewProvider {
    static var previews: some View {
        InsuranceListView()
    }
}




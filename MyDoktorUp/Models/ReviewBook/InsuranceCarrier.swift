//
//  InsuranceCarrier.swift
//  MyDoktorUp
//
//  Created by Mac on 20/02/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation

struct InsuranceCarrier:Identifiable {
    
    public var id: Int?
    public var name: String?
    
    static func initWith(dictInfo:[String:Any]) -> InsuranceCarrier {
        var insuranceCarrierObj = InsuranceCarrier()
        
        if let id = dictInfo["id"] as? Int {
            insuranceCarrierObj.id = id
        }
        if let name = dictInfo["name"] as? String {
            insuranceCarrierObj.name = name
        }
        
        return insuranceCarrierObj
    }
}

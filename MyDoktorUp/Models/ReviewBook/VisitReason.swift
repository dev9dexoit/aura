//
//  VisitReason.swift
//  MyDoktorUp
//
//  Created by Mac on 20/02/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation


struct VisitReason:Identifiable {
    
    public var id = UUID()
    public var UIVisitReason: String?
    public var reasonid: Int?
    public var name: String?
    public var duration: Int?
    public var patientdisplayname: String?
    public var appointmenttypeid: Int?
    
    static func initWith(dictInfo:[String:Any]) -> VisitReason {
        var visitReasonObj = VisitReason()
        
        if let strUIVisitReason = dictInfo["UIVisitReason"] as? String {
            visitReasonObj.UIVisitReason = strUIVisitReason
        }
        if let reasonid = dictInfo["reasonid"] as? Int {
            visitReasonObj.reasonid = reasonid
        }
        if let name = dictInfo["name"] as? String {
            visitReasonObj.name = name
        }
        if let duration = dictInfo["duration"] as? Int {
            visitReasonObj.duration = duration
        }
        if let patientdisplayname = dictInfo["patientdisplayname"] as? String {
            visitReasonObj.patientdisplayname = patientdisplayname
        }
        if let appointmenttypeid = dictInfo["appointmenttypeid"] as? Int {
            visitReasonObj.appointmenttypeid = appointmenttypeid
        }
        
        return visitReasonObj
    }
}

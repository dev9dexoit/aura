//
//  Appointments.swift
//  MyDoktorUp
//
//  Created by SHREE SAHANA on 4/19/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation

struct AppointmentModel: Hashable, Codable {
   
    var appointment_id: String?
    var appointment_date: String?
    var appointment_time: String?
    var department_id: String?
    var duration: String?
    var appointment_type_id: String?
    var provider_id: String?
    var visit_reason: String?
    var created_at: String?
    var status: String?
    var provider_first_name : String?
    var provider_last_name : String?
    var provider_address1 : String?
    var provider_address2 : String?
    var provider_city : String?
    var provider_state : String?
    var provider_phone : String?
    var taxonomy_name : String?
    var distance : String?
    var visit_note : String?
    var appointment_join:String?
    var booking_forself : String?
    var patient_name: String?
    var patient_address1: String?
    var patient_address2: String?
    var patient_city_state_zip: String?
    var patient_dob: String?
    var patient_gender: String?
    var patient_email: String?
    var insurance: String?
    var patient_phone: String?
    var checkin_info: String?
    var checkin_consent: String?
    var checkin_complete: String?
          
       
}
struct AppointmentData: Identifiable, Codable {
    var id = UUID()
    var title: String
    var items: [AppointmentModel]
    init(title: String, items: [AppointmentModel]) {
        self.title = title
        self.items = items
    }

}

//
//  User.swift
//  MyDoktorUp
//
//  Created by SHREE SAHANA on 4/19/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation

struct User: Codable {
    var id: String?
    var patient_id: String?
    var email: String?
    var user_type: String?
    var session_id: String?
    var first_name: String?
    var last_name: String?
}

struct User_Insurance_card {
    var back : String?
    var front : String?
    
    
    static func initWith(dictInfo:[String:Any]) -> User_Insurance_card {
        
        var userInsurance = User_Insurance_card()
        if let back = dictInfo["back"] as? String {
            userInsurance.back =  back
        }
        if let front = dictInfo["front"] as? String {
            userInsurance.front = front
        }
        return userInsurance
    }
    
}



struct User_Special_Attributes {

    var race: String?
    var gender: String?
    var martial: String?
    var language: String?
    var ethnicity: String?
    var insurance: String?
    var marital_status: String?
    
    static func initWith(dictInfo:[String:Any]) -> User_Special_Attributes {
        
        var user_Special_Attributes = User_Special_Attributes()

        
        if let race = dictInfo["race"] as? String {
            user_Special_Attributes.race =  race
        }
        if let gender = dictInfo["gender"] as? String {
            user_Special_Attributes.gender = gender
        }
        if let martial = dictInfo["martial"] as? String {
            user_Special_Attributes.martial =  martial
        }
        if let language = dictInfo["language"] as? String {
            user_Special_Attributes.language = language
        }
        if let ethnicity = dictInfo["ethnicity"] as? String {
            user_Special_Attributes.ethnicity =  ethnicity
        }
        if let insurance = dictInfo["insurance"] as? String {
            user_Special_Attributes.insurance = insurance
        }
        if let marital_status = dictInfo["marital_status"] as? String {
            user_Special_Attributes.marital_status = marital_status
               }
        
        
        
        return user_Special_Attributes
    }
    
}

struct User_System_Attribute {

var user_type : String?
var is_Active : String?
var last_login : String?
var created_at : String?
var update_at : String?

    
    
    static func initWith(dictInfo:[String:Any]) -> User_System_Attribute {
        var user_System_Attribute = User_System_Attribute()
        
        
                if let user_type = dictInfo["user_type"] as? String {
                      user_System_Attribute.user_type =  user_type
                  }
                  if let is_Active = dictInfo["is_Active"] as? String {
                      user_System_Attribute.is_Active = is_Active
                  }
              
                  if let last_login = dictInfo["last_login"] as? String {
                      user_System_Attribute.last_login =  last_login
                  }
                  if let created_at = dictInfo["created_at"] as? String {
                      user_System_Attribute.created_at =  created_at
                  }
                  if let update_at = dictInfo["update_at"] as? String {
                      user_System_Attribute.update_at =  update_at
                  }
        
              
        
        return user_System_Attribute

    }
}




struct UserProfileInfo {
    
    var dob : String?
    var zip : String?
    var city : String?
    var phone : String?
    var state : String?
    var address1 : String?
    var address2 : String?
    var last_name : String?
    var first_name : String?
//    var Insurance:User_Insurance_card
    
    static func initWith(dictInfo:[String:Any]) -> UserProfileInfo {
            var userProfileInfo = UserProfileInfo()
    
            if let dob = dictInfo["dob"] as? String {
                userProfileInfo.dob =  dob
            }
            if let zip = dictInfo["zip"] as? String {
                userProfileInfo.zip = zip
            }
        
            if let city = dictInfo["city"] as? String {
                userProfileInfo.city =  city
            }
            if let phone = dictInfo["phone"] as? String {
                userProfileInfo.phone =  phone
            }
            if let state = dictInfo["state"] as? String {
                userProfileInfo.state =  state
            }
            if let address1 = dictInfo["address1"] as? String {
                userProfileInfo.address1 =  address1
            }
            if let address2 = dictInfo["address2"] as? String {
                userProfileInfo.address2 =  address2
            }
            if let last_name = dictInfo["last_name"] as? String {
                       userProfileInfo.last_name =  last_name
            }
            if let first_name = dictInfo["first_name"] as? String {
                       userProfileInfo.first_name =  first_name
            }
        
        if let UserInsurance = dictInfo["insurance_card"] as? [String:Any ]
        {
            let userInformation = User_Insurance_card.initWith(dictInfo: UserInsurance)
            UserDefaultsManager().user_Insu_Front = userInformation.front
            UserDefaultsManager().user_Insu_Back = userInformation.back

        }
        
        return userProfileInfo
    
    }
    
}


struct UserBookingFor {
    var patient_id: Int?
    var relationship: String?
    var simple_name: String?
    var dob: String?
    var gender: String?
    var user_allowed_to_book_forself: [String]?
        
    static func initWith(dictInfo:[String:Any]) -> UserBookingFor {
        var userBookingFor = UserBookingFor()
        if let id = dictInfo["patient_id"] as? Int {
            userBookingFor.patient_id = id
        }
        if let relationship = dictInfo["relationship"] as? String {
            userBookingFor.relationship = relationship
        }
        if let simple_name = dictInfo["simple_name"] as? String {
            userBookingFor.simple_name = simple_name
        }
        if let dob = dictInfo["dob"] as? String {
            userBookingFor.dob = dob
        }
        if let gender = dictInfo["gender"] as? String {
            userBookingFor.gender = gender
        }
        if let user_allowed_to_book_forself = dictInfo["user_allowed_to_book_forself"] as? [String] {
            userBookingFor.user_allowed_to_book_forself = user_allowed_to_book_forself
        }
        
        return userBookingFor
    }
}

struct PatientRelation: Hashable, Codable, Identifiable {
    public var id = UUID()
    public var relationDesignation: String
}

struct UserProfileAttributes: Hashable, Codable, Identifiable {
    public var id = UUID()
    public var zip: String?
    public var city: String?
    public var state: String?
    public var address1: String?
    public var address2: String?
    public var last_name: String?
    public var first_name: String?
    public var gender: String?
    public var phone: String?
    public var dob: String?
    
    static func initWith(dictInfo:[String:Any]) -> UserProfileAttributes {
        var userProfileAttributesObj = UserProfileAttributes()
        
        if let zip = dictInfo["zip"] as? String {
            userProfileAttributesObj.zip = zip
        }
        if let city = dictInfo["city"] as? String {
            userProfileAttributesObj.city = city
        }
        if let state = dictInfo["state"] as? String {
            userProfileAttributesObj.state = state
        }
        if let address1 = dictInfo["address1"] as? String {
            userProfileAttributesObj.address1 = address1
        }
        if let address2 = dictInfo["address2"] as? String {
            userProfileAttributesObj.address2 = address2
        }
        if let last_name = dictInfo["last_name"] as? String {
            userProfileAttributesObj.last_name = last_name
        }
        if let first_name = dictInfo["first_name"] as? String {
            userProfileAttributesObj.first_name = first_name
        }
        if let gender = dictInfo["gender"] as? String {
            userProfileAttributesObj.gender = gender
        }
        if let phoneNo = dictInfo["phone"] as? String {
            userProfileAttributesObj.phone = phoneNo
        }
        if let dob = dictInfo["dob"] as? String {
            userProfileAttributesObj.dob = dob
        }
        
        return userProfileAttributesObj
    }
    
}

struct UserSpecialAttributes: Hashable, Codable, Identifiable {
    public var id = UUID()
    public var dob: String?
    public var race: String?
    public var gender: String?
    public var language: String?
    public var ethnicity: String?
    public var marital_status: String?
    
    static func initWith(dictInfo:[String:Any]) -> UserSpecialAttributes {
        var userSpecialAttributesObj = UserSpecialAttributes()
        
        if let dob = dictInfo["dob"] as? String {
            userSpecialAttributesObj.dob = dob
        }
        if let race = dictInfo["race"] as? String {
            userSpecialAttributesObj.race = race
        }
        if let gender = dictInfo["gender"] as? String {
            userSpecialAttributesObj.gender = gender
        }
        if let language = dictInfo["language"] as? String {
            userSpecialAttributesObj.language = language
        }
        if let ethnicity = dictInfo["ethnicity"] as? String {
            userSpecialAttributesObj.ethnicity = ethnicity
        }
        if let maritalStatus = dictInfo["marital_status"] as? String {
            userSpecialAttributesObj.marital_status = maritalStatus
        }
            
        return userSpecialAttributesObj
    }
}

struct UserBookingForself: Hashable, Codable, Identifiable {
    public var id = UUID()
    public var patient_id: Int
    public var relationship: String
    public var simple_name: String
    public var dob: String
    public var gender: String
    public var user_allowed_to_book_forself: [String]
}

struct SocialUser {
    static var Email:String = ""
    static var familyName:String = ""
    static var name:String = ""
    static var givenName:String = ""
    
}




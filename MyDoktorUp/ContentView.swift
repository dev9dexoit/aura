//
//  ContentView.swift
//  MyDoktorUp
//
//  Created by Mac on 05/02/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI


class AppLogInViewState: ObservableObject {
    @Published var moveToLoginView: Bool = false
}



struct ContentView: View {
    @Binding var isNavigationBarHidden: Bool
    @EnvironmentObject var model: NavigationIsActiveModel
    @EnvironmentObject var appState: AppState
    @State var selection = 0
    @ObservedObject var userDefaultsManager = UserDefaultsManager()
    var selectedProvider = Provider()
    var body: some View {
      
        VStack {
            
            TabView(selection: $selection) {
            
            DoctorProfileView(isNavigationBarHidden: self.$isNavigationBarHidden, selectedProvider: self.selectedProvider).tabItem({
             if selection == 0 {
                    Image(TabImagesSelected.tabItem0)
                } else {
                    Image(TabImages.tabItem0)
                }
                
                Text(TabTitles.tabItem0)
            }).tag(0)
            .navigationBarTitle("Home", displayMode: .inline)
            if UserDefaultsManager().isSign ?? false {
                CheckInView().tabItem({
                    if selection == 1 {
                        Image(TabImagesSelected.tabItem1)
                    } else {
                        Image(TabImages.tabItem1)
                    }
                    Text(TabTitles.tabItem1)
                }).tag(1)
                .navigationBarTitle("Check-In", displayMode: .inline)
            } else {
                LoginForCheckIn(SelectedTab: 1).tabItem({
                    if selection == 1 {
                        Image(TabImagesSelected.tabItem1)
                    } else {
                        Image(TabImages.tabItem1)
                    }
                    Text(TabTitles.tabItem1)
                }).tag(1)
                .navigationBarTitle("Check-In", displayMode: .inline)
            }
            if UserDefaultsManager().isSign ?? false {
             
                AppointmentsView(appointments: AppointmentListObserver()).tabItem({
                    if selection == 2 {
                        Image(TabImagesSelected.tabItem2)
                    } else {
                        Image(TabImages.tabItem2)
                    }
                    Text(TabTitles.tabItem2)
                }).tag(2)
                .navigationBarTitle("Appointments", displayMode: .inline)
            } else {
                LoginForAppointment(SelectedTab: 2).tabItem({
                    if selection == 2 {
                        Image(TabImagesSelected.tabItem2)
                    } else {
                        Image(TabImages.tabItem2)
                    }
                    Text(TabTitles.tabItem2)
                }).tag(2)
                .navigationBarTitle("Appointments", displayMode: .inline)
            }
            
            if UserDefaultsManager().isSign ?? false {
                IntakePersonalInfoView(rootIsActive: .constant(true)).tabItem({
                    if selection == 3 {
                        Image(TabImagesSelected.tabItem3)
                    } else {
                        Image(TabImages.tabItem3)
                    }
                    Text(TabTitles.tabItem3)
                }).tag(3)
                .navigationBarTitle("Intake Forms", displayMode: .inline)
            } else {
                LoginForIntakeForms(SelectedTab: 3).tabItem({
                    if selection == 3 {
                        Image(TabImagesSelected.tabItem3)
                    } else {
                        Image(TabImages.tabItem3)
                    }
                    Text(TabTitles.tabItem3)
                }).tag(3)
                .navigationBarTitle("Intake Forms", displayMode: .inline)
            }
            
            if UserDefaultsManager().isSign ?? false {
                SettingsView().tabItem({
                    if selection == 4 {
                        Image(TabImagesSelected.tabItem4)
                    } else {
                        Image(TabImages.tabItem4)
                    }
                    Text(TabTitles.tabItem4)
                }).tag(4)
                .navigationBarTitle("Settings", displayMode: .inline)
            } else {
                LoginForSetting(SelectedTab: 4).tabItem({
                    if selection == 4 {
                        Image(TabImagesSelected.tabItem4)
                    } else {
                        Image(TabImages.tabItem4)
                    }
                    Text(TabTitles.tabItem4)
                }).tag(4)
                .navigationBarTitle("Settings", displayMode: .inline)
            }
            
                }.accentColor(kPrimaryRegularColor)
            }
        
            .onAppear() {
                
                    self.userDefaultsManager.setValueFor(provider: self.selectedProvider)
        }
        .edgesIgnoringSafeArea(.top)
        .hideNavigationBar()
        .navigationBarBackButtonHidden(true)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(isNavigationBarHidden: .constant(true))
    }
}

struct MyTabBarView: View {
    @Binding var isNavigationBarHidden: Bool
    @EnvironmentObject var model: NavigationIsActiveModel
    @EnvironmentObject var appState: AppState
    @State var selection = 0
    @ObservedObject var userDefaultsManager = UserDefaultsManager()
    var selectedProvider = Provider()
    var body: some View {
      
        VStack {
            TabView(selection: $selection) {
                DoctorProfileView(isNavigationBarHidden: self.$isNavigationBarHidden, selectedProvider: self.selectedProvider).tabItem({
                    if selection == 0 {
                        Image(TabImagesSelected.tabItem0)
                    } else {
                        Image(TabImages.tabItem0)
                    }
                    Text(TabTitles.tabItem0)
                }).tag(0)
                .navigationBarTitle("Home", displayMode: .inline)
                if UserDefaultsManager().isSign ?? false {
                    CheckInView().tabItem({
                        if selection == 1 {
                            Image(TabImagesSelected.tabItem1)
                        } else {
                            Image(TabImages.tabItem1)
                        }
                        Text(TabTitles.tabItem1)
                    }).tag(1)
                    .navigationBarTitle("Check-In", displayMode: .inline)
                } else {
                    LoginForCheckIn(SelectedTab: 1).tabItem({
                        if selection == 1 {
                            Image(TabImagesSelected.tabItem1)
                        } else {
                            Image(TabImages.tabItem1)
                        }
                        Text(TabTitles.tabItem1)
                    }).tag(1)
                    .navigationBarTitle("Check-In", displayMode: .inline)
                }
                if UserDefaultsManager().isSign ?? false {
                 
                    AppointmentsView(appointments: AppointmentListObserver()).tabItem({
                        if selection == 2 {
                            Image(TabImagesSelected.tabItem2)
                        } else {
                            Image(TabImages.tabItem2)
                        }
                        Text(TabTitles.tabItem2)
                    }).tag(2)
                    .navigationBarTitle("Appointments", displayMode: .inline)
                } else {
                    LoginForAppointment(SelectedTab: 2).tabItem({
                        if selection == 2 {
                            Image(TabImagesSelected.tabItem2)
                        } else {
                            Image(TabImages.tabItem2)
                        }
                        Text(TabTitles.tabItem2)
                    }).tag(2)
                    .navigationBarTitle("Appointments", displayMode: .inline)
                }
                
                if UserDefaultsManager().isSign ?? false {
                    IntakePersonalInfoView(rootIsActive: .constant(true)).tabItem({
                        if selection == 3 {
                            Image(TabImagesSelected.tabItem3)
                        } else {
                            Image(TabImages.tabItem3)
                        }
                        Text(TabTitles.tabItem3)
                    }).tag(3)
                    .navigationBarTitle("Intake Forms", displayMode: .inline)
                } else {
                    LoginForIntakeForms(SelectedTab: 3).tabItem({
                        if selection == 3 {
                            Image(TabImagesSelected.tabItem3)
                        } else {
                            Image(TabImages.tabItem3)
                        }
                        Text(TabTitles.tabItem3)
                    }).tag(3)
                    .navigationBarTitle("Intake Forms", displayMode: .inline)
                }
                
                if UserDefaultsManager().isSign ?? false {
                    SettingsView().tabItem({
                        if selection == 4 {
                            Image(TabImagesSelected.tabItem4)
                        } else {
                            Image(TabImages.tabItem4)
                        }
                        Text(TabTitles.tabItem4)
                    }).tag(4)
                    .navigationBarTitle("Settings", displayMode: .inline)
                } else {
                    LoginForSetting(SelectedTab: 4).tabItem({
                        if selection == 4 {
                            Image(TabImagesSelected.tabItem4)
                        } else {
                            Image(TabImages.tabItem4)
                        }
                        Text(TabTitles.tabItem4)
                    }).tag(4)
                    .navigationBarTitle("Settings", displayMode: .inline)
                }
            }.accentColor(kPrimaryRegularColor)
        }.onAppear() {
                self.userDefaultsManager.setValueFor(provider: self.selectedProvider)

        }
        .hideNavigationBar()
        .navigationBarBackButtonHidden(true)
    }
}
struct MyTabBarView_Previews: PreviewProvider {
    static var previews: some View {
        MyTabBarView(isNavigationBarHidden: .constant(true))
    }
}

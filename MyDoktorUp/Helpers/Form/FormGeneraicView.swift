//
//  FormGeneraicView.swift
//  MyDoktorUp
//
//  Created by Mac on 30/03/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI

struct FormGeneraicView: View {
    
    var formData: ProviderFormData?
    
    var body: some View {
        Group() {
            if formData != nil {
                Form() {
                    List(formData!.formcontent) { content in
                        NavigationLink(destination: FormContentView(questions: content.questions, formContent: content, titleForNavigation: content.name)) {
                            Text(content.name)
                        }
                    }
                }
            } else {
                Text("No data found")
            }
        }
    }
}

struct FormGeneraicView_Previews: PreviewProvider {
    static var previews: some View {
        FormGeneraicView()
    }
}

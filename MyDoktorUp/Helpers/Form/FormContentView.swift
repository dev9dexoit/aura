//
//  FormContentView.swift
//  MyDoktorUp
//
//  Created by Mac on 30/03/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI

struct FormContentView: View {
    
    var questions: [QuestionN]?
    var formContent: FormContent?
    var titleForNavigation = ""
    @State var type: String = ""
    
    var body: some View {
        
        Group {
            if self.type == "CHECKBOX" {
                List(questions!) { question in
                    
                    CheckView(isChecked: false, checked: .constant(false), title: question.text!)
                    
                }
            } else if self.type == "CUSTOM" {
                List(questions!) { question in
                    
                    if question.inputtype == "YESNO" {
                        ToggleView(question.text!)
                    }
                    
                    else if question.inputtype == "DROPDOWN" {
                        DropDownView(question.text!, dropdownValues: question.dropdownvalues!)
                    }
                    
                    else if question.inputtype == "FREETEXT" {
                        FreeTextView(question.text!)
                    }
                    
                    else if question.inputtype == "NUMERIC" {
                        NumericTextView(question.text!)
                    }
                    
                    else if question.inputtype == "DATE" {
                        DatePickerView(title: question.text!)
                    }
                }
            }
        }.navigationBarTitle(self.titleForNavigation)
        
        .onAppear {
            if self.formContent?.fielddata.defaultfielddata?.inputtype
                != nil {
                print(self.formContent!.fielddata.defaultfielddata!.inputtype!)
                self.type = self.formContent!.fielddata.defaultfielddata!.inputtype!
            }
        }
    }
}

struct FormContentView_Previews: PreviewProvider {
    static var previews: some View {
        FormContentView()
    }
}

//MARK:- Custom Views
struct ToggleView : View {
    @State private var state: Bool = false
    private let name: String

    init(_ name: String) {
        self.name = name
    }

    var body: some View {
        HStack {
            Toggle(isOn: self.$state) {
                Text(name)
                Spacer()
            }
        }
    }
}

struct DropDownView : View {
    @State private var selectedOption: Int = 0
    private let name: String
    private let dropdownValues: [[String]]

    init(_ name: String, dropdownValues: [[String]]) {
        self.name = name
        self.dropdownValues = dropdownValues
    }

    var body: some View {
        HStack {
            Picker(selection: self.$selectedOption, label: Text(name)) {
                ForEach(0 ..< dropdownValues.count) {
                    Text(self.dropdownValues[$0][0])
                }
            }
        }
    }
}

struct FreeTextView: View {
    @State private var text: String = ""
    private let name: String
    
    init(_ name: String) {
        self.name = name
    }
    
    var body: some View {
        TextField(self.name, text: $text)
    }
}

struct CheckView: View {
    @State var isChecked:Bool = false
    @Binding var checked:Bool
    var title:String
    func toggle( ){
        isChecked = !isChecked
    }
    
    var body: some View {
        
        Button(action: {
            self.toggle()
            self.checked = self.isChecked
        }) {
            HStack{
                           Image(systemName: isChecked ? "checkmark.square": "square")
                           Text(title)
                       }
        }
      
    }
}


struct NumericTextView: View {
    @State private var text: String = ""
    private let name: String
    
    init(_ name: String) {
        self.name = name
    }
    
    var body: some View {
        TextField(self.name, text: $text)
            .keyboardType(.numberPad)
    }
}

struct DatePickerView: View {
    @State private var selectedDate: Date = Date()
    var title: String
    
    var body: some View {
        
        VStack {
            DatePicker(selection: $selectedDate, in: ...Date(), displayedComponents: .date) {
                HStack {
                    Text(title)
                }
            }
        }
    }
}

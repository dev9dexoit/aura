//
//  LocalNotification.swift
//  MyDoktorUp
//
//  Created by RMV on 27/06/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import UserNotifications

struct Notification {
    var id:String
    var title:String
    var datetime:DateComponents
    var subTitle:String
}

var notifications = [Notification]()

let manager = LocalNotificationManager()

class LocalNotificationManager
{
    var notifications = [Notification]()

    
    func listScheduledNotifications()
    {
        UNUserNotificationCenter.current().getPendingNotificationRequests { notifications in

            for notification in notifications {
                print(notification)
            }
        }
    }
    
    func schedule()
    {
        UNUserNotificationCenter.current().getNotificationSettings { settings in

            switch settings.authorizationStatus {
            case .notDetermined:
               requestAuthorizationNotification()
            case .authorized, .provisional:
                scheduleNotifications()
            default:
                break // Do nothing
            }
        }
    }
}
func requestAuthorizationNotification()
{
    UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { granted, error in

        if granted == true && error == nil {
           scheduleNotifications()
        }
    }
}

 func scheduleNotifications()
{
    for notification in notifications
    {
        
        let content      = UNMutableNotificationContent()
        content.title    = notification.title
        content.sound    = .default
//        content.subtitle = notification.subTitle
        content.body = notification.subTitle
        let trigger = UNCalendarNotificationTrigger(dateMatching: notification.datetime, repeats: false)

        let request = UNNotificationRequest(identifier: notification.id, content: content, trigger: trigger)

        UNUserNotificationCenter.current().add(request) { error in

            guard error == nil else { return }

        }
    }
}

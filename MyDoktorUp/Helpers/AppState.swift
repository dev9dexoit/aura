//
//  AppState.swift
//  MyDoktorUp
//
//  Created by RMV on 04/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//


import SwiftUI
import Combine

class AppState: ObservableObject {
    func reloadDashboard(selectedTab:Int,selectedProvider:Provider) {
        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.reloadDashboard(selectedTab: selectedTab,selectedProvider:selectedProvider)
    }
}

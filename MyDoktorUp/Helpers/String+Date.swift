//
//  String+Date.swift
//  MyDoktorUp
//
//  Created by SHREE SAHANA on 3/29/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation

extension String {
    func checkToday(inputDate: String, inputFormat: String) -> Date {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = inputFormat
        guard let datee = dateFormatterGet.date(from: inputDate) else { return Date() }
        return datee
    }
    func convertDateFormat(inputDate: String, inputFormat: String, format: String) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = inputFormat
        let dateFormatterPrint = DateFormatter()
        let datee = dateFormatterGet.date(from: inputDate)
        if Calendar.current.isDateInToday(datee ?? Date()) {
            return checkTodayforBookAppointment(format: format, datee: datee, dateFormatterPrint: dateFormatterPrint)
        } else {
            dateFormatterPrint.dateFormat = format
            return dateFormatterPrint.string(from: datee ?? Date())
        }
    }
    func checkTodayforBookAppointment(format: String, datee: Date?, dateFormatterPrint: DateFormatter) -> String {
        dateFormatterPrint.dateFormat = format
        if format.contains("EEEE") {
            let start = String.Index(utf16Offset: 4, in: "")//String.Index(encodedOffset: 4)
            let end = String.Index(utf16Offset:format.count, in: "") //String.Index(encodedOffset: format.count)
            let nformat = String(format[start..<end])
            print(nformat)
            if nformat.isEmpty {
                return dateFormatterPrint.string(from: datee ?? Date())
            } else {
                dateFormatterPrint.dateFormat = String(nformat)
                return "Today" + dateFormatterPrint.string(from: datee ?? Date())
            }
        } else {
            return dateFormatterPrint.string(from: datee ?? Date())
        }
    }
    
    func isValid() -> Bool {
        let emailRegEx = "(?:[a-zA-Z0-9!#$%\\&‘*+/=?\\^_`{|}~-]+(?:\\.[a-zA-Z0-9!#$%\\&'*+/=?\\^_`{|}" +
            "~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\" +
            "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-" +
            "z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5" +
            "]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-" +
            "9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21" +
            "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        
        let emailTest = NSPredicate(format:"SELF MATCHES[c] %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    func formatePhoneNumber() -> String {
        var newText = ""
        for (index, character) in self.enumerated() {
            if index != 0 && index != 9 && index % 3 == 0 {
                newText.append("-")
            }
            newText.append(String(character))
        }
        return newText
    }
    func capitalizingFirstLetter() -> String {
        let first = String(self.prefix(1)).capitalized
        let other = String(self.dropFirst())
        return first + other
    }

    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}

//MARK:-
extension Date {
    func getCurrentDateAsFormattedString() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        return formatter.string(from: self)
    }
}

//
//  DoktorUpNavigation.swift
//  MyDoktorUp
//
//  Created by SHREE SAHANA on 6/17/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI
import Combine

class DoktorUpNavigation {
    public func link<Destination: View>(destination: Destination, action: Binding<Bool>) -> some View {
        return NavigationLink(destination: destination, isActive: action) {
            HStack {
                EmptyView()
            }
        }
    }
}

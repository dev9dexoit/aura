//
//  Utils.swift
//  MyDoktorUp
//
//  Created by SHREE SAHANA on 4/9/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import UIKit
import EventKit
public func dLog(_ message: @autoclosure () -> String, filename: String = #file, function: String = #function, line: Int = #line) {
#if DEBUG
    print("[\(URL(string: filename)?.lastPathComponent ?? filename):\(line)]", "\(function)", message(), separator: " - ")
#else
#endif
}
public func aLog(_ message: @autoclosure () -> String, filename: String = #file, function: String = #function, line: Int = #line) {
    print("[\(URL(string: filename)?.lastPathComponent ?? filename):\(line)]", "\(function)", message(), separator: " - ")
}
class Utils {
    static func getProviderFullname(_ provider: Provider) -> String {
        let firstname = provider.first_name?.capitalized ?? ""
        let lastname = provider.last_name?.capitalized ?? ""
        let drSignature = provider.credential?.uppercased() ?? ""
        let providerFullName = "Dr. " + firstname + " " + lastname + ", " + drSignature
        
        UserDefaultsManager().doctorFullName = providerFullName
                
        return providerFullName
    }
    static func getCurrentDate(isRequest: Bool) -> String {
        let date = Date()
        let calendar = Calendar.current
        print(calendar.startOfDay(for: date))
        print(calendar.dateComponents([.year, .month, .day, .hour, .minute], from: date))
        let components = calendar.dateComponents([.year, .month, .day, .hour, .minute], from: date)
        print("All Components : \(components)")
        var currentDate = String.init(format: "%02d/%02d/%02d", components.month ?? 0, components.day ?? 0, components.year ?? 0)
        if isRequest {
            currentDate = String.init(format: "%02d-%02d-%02d",components.year ?? 0 , components.month ?? 0, components.day ?? 0)
        }
        return currentDate
    }
    static func getLast30DaysFromToday(isRequest: Bool) -> String {
        let toDate = Date()
        let calendar = Calendar.current
        let last30Days = calendar.date(byAdding: .day, value: -30, to: toDate)
        let components = calendar.dateComponents([.year, .month, .day, .hour, .minute], from: last30Days ?? Date())
        print("last30Days : \(last30Days ?? Date())")
        var currentDate = String.init(format: "%02d/%02d/%02d", components.month ?? 0, components.day ?? 0, components.year ?? 0)
        if isRequest {
            currentDate = String.init(format: "%02d-%02d-%02d",components.year ?? 0 , components.month ?? 0, components.day ?? 0)
        }
        return currentDate
    }
    
    static func getTodayWith20DaysFromToday(isRequest: Bool) -> String {
        let toDate = Date()
        let calendar = Calendar.current
        let last30Days = calendar.date(byAdding: .day, value: 20, to: toDate)
        let components = calendar.dateComponents([.year, .month, .day, .hour, .minute], from: last30Days ?? Date())
        print("last30Days : \(last30Days ?? Date())")
        var currentDate = String.init(format: "%02d/%02d/%02d", components.month ?? 0, components.day ?? 0, components.year ?? 0)
        if isRequest {
            currentDate = String.init(format: "%02d-%02d-%02d",components.year ?? 0 , components.month ?? 0, components.day ?? 0)
        }
        return currentDate
    }
    
     
    static func getCurrentTime() -> String {
        let currentDateTime = Date()
        let calendar = NSCalendar.current
        let components = calendar.dateComponents([.year, .month, .day, .hour, .minute], from: currentDateTime)
        let currentTime = String.init(format: "%02d:%02d", components.hour ?? 0, components.minute ?? 0)
        return currentTime
    }
    
     static func findDateDiffInMinute(time1Str: String, time2Str: String,formate:String) -> Int {
        let timeformatter = DateFormatter()
        timeformatter.dateFormat = formate

        guard let time1 = timeformatter.date(from: time1Str),
        let time2 = timeformatter.date(from: time2Str) else { return 0 }

  
        
        //You can directly use from here if you have two dates

        let interval = time2.timeIntervalSince(time1)
        let minute = interval.truncatingRemainder(dividingBy: 3600) / 60
        print("minute \(minute)")
        return Int(minute)
    }
    
    static func findDateDiffInHours(time1Str: String, time2Str: String,formate:String) -> Int {
        let timeformatter = DateFormatter()
        timeformatter.dateFormat = formate

        guard let time1 = timeformatter.date(from: time1Str),
            let time2 = timeformatter.date(from: time2Str) else { return 0 }

        //You can directly use from here if you have two dates

        let interval = time2.timeIntervalSince(time1)
        let hour = interval / 3600;
        print("hour \(hour)")
        return Int(hour)
    }
    
    static func SYSTEM_VERSION_LESS_THAN(version: String) -> Bool {
        return UIDevice.current.systemVersion.compare(version, options: .numeric) == .orderedAscending
    }
    
    
    static func AddPointmentInCalender(_ AppointmentInfo: AppointmentInfo,provider:Provider) -> String{
       let eventStore = EKEventStore()
        
        
        switch EKEventStore.authorizationStatus(for: .event) {
           case .authorized:
            return self.insertEvent(store: eventStore, AppointmentInfo: AppointmentInfo,provider:provider)
               case .denied:
                return "Access denied"
                   
               case .notDetermined:
               // 3
                var status = ""
                   eventStore.requestAccess(to: .event, completion:
                     {  (granted: Bool, error: Error?) -> Void in
                         if granted {
                            
                            status = self.insertEvent(store: eventStore, AppointmentInfo: AppointmentInfo, provider: provider)
                            
                            
                            
                            
                         } else {
                               status = "Access denied"
                         }
                   })
            return status
                   default:
                       return "Access denied"
           }
    }
    
   
    
    static func eventAlreadyExists(eventStore: EKEventStore,event eventToAdd: EKEvent) -> Bool {
         let predicate = eventStore.predicateForEvents(withStart: eventToAdd.startDate, end: eventToAdd.endDate, calendars: nil)
         let existingEvents = eventStore.events(matching: predicate)
         
         let eventAlreadyExists = existingEvents.contains { (event) -> Bool in
             return eventToAdd.title == event.title && event.startDate == eventToAdd.startDate && event.endDate == eventToAdd.endDate
         }
         return eventAlreadyExists
     }
    
    static func insertEvent(store: EKEventStore,AppointmentInfo:AppointmentInfo,provider:Provider) -> String {
        // 1
        
        let StartDateAndTime = "\(AppointmentInfo.appointment_date ?? "")-\(AppointmentInfo.appointment_time ?? "")"
        
        let dateformator = DateFormatter()
        dateformator.dateFormat = "MM-dd-yyyy-HH:mm"
    
    
        let startDate = dateformator.date(from: "\(StartDateAndTime)")
        
        let calendar = Calendar.current
        let endDate = calendar.date(byAdding: .minute, value: Int("\(AppointmentInfo.duration ?? "0")") ?? 0, to: startDate!)

        
        
        let event = EKEvent(eventStore: store)
        event.calendar = store.defaultCalendarForNewEvents
        event.title = "Appointment with Dr. \(provider.first_name ?? "") \(provider.last_name ?? "") (MD - \(provider.taxonomy_name ?? "")"
        event.location = "\(provider.address1 ?? ""),\(provider.city ?? ""),\(provider.state ?? ""),\(provider.zip ?? "")"
        event.startDate = startDate
        event.endDate = endDate
        if !eventAlreadyExists(eventStore:store,event: event) {
        do {
            try store.save(event, span: .thisEvent)
            return "Success"
        }
        catch {
           print("Error saving event in calendar")
            return "Fail"
            }
        }
        else
        {
            return "AlreadyExists"
        }
        
        
        // 5
        
        }
    
    static func getUserFullname(_ user: User) -> String {
        let firstname = user.first_name ?? ""
        let lastname = user.last_name ?? ""
        return firstname + " " + lastname
    }
    
}




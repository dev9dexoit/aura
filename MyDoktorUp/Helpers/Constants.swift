//
//  Constants.swift
//  MyDoktorUp
//
//  Created by Mac on 05/02/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import UIKit
import SwiftUI



var LogInTitleText = [1:"Less wait, more face-time\nwith the doctor.",2:"See past & future\nappointments.",3:"Less wait, more face-time\nwith the doctor.",4:"Personalize your app."]


struct LoginTypes {
    static let Email = "Email"
    static let Google = "google"
    static let Apple = "apple"
    
}
var MedicalFormJsonObject = NSArray()


func SetModelObject(Number:String,FormName:String,formNumber:Int) -> [String:String] {
    
    let dateformate = DateFormatter()
    dateformate.dateFormat = "YYYY-MM-dd HH:mm"
    
    
    let consentDate = dateformate.string(from: Date())
    
    var model = [String:String]()
                                        model.updateValue(FormName, forKey: "form\(formNumber)_name")
                                        model.updateValue(UserDefaultsManager().userFullName ?? "", forKey: "consent_by")
                                        model.updateValue("Self", forKey: "relationship")
                                        model.updateValue(Number, forKey: "phone")
                                        model.updateValue(consentDate , forKey: "consent")
                                        model.updateValue("\(UUID())" , forKey: "device")
                                        model.updateValue("", forKey: "signature_image")
    print("model \(model)")
    return model
    //
}

class NavigationIsActiveModel: ObservableObject {
    @Published var pushed = false
}



struct TabTitles {
    static let tabItem0 = "Home"
    static let tabItem1 = "Check-In"
    static let tabItem2 = "Appointments"
    static let tabItem3 = "Intake Forms"
    static let tabItem4 = "Settings"
}

struct TabImages {
    static let tabItem0 = "ic_home"
    static let tabItem1 = "ic_check_in"
    static let tabItem2 = "ic_appointments"
    static let tabItem3 = "ic_intake_form"
    static let tabItem4 = "ic_settings"
}

struct TabImagesSelected {
    static let tabItem0 = "ic_sel_home"
    static let tabItem1 = "ic_sel_check_in"
    static let tabItem2 = "ic_sel_appointments"
    static let tabItem3 = "ic_sel_intake_form"
    static let tabItem4 = "ic_sel_settings"
}


struct UserDefault {
    static let udProviderId = "provider_id"
    static let userSessionId = "loginSessionId"
    static let loggedInUser = "isLoggedInUser"
    static let SelectedDoctor = "SelectedDoctor"
    static let doctorInfo = "doctorInfo"
    static let loginType = "loginType"
    static let userFullName = "userFullName"
    static let userEmail = "userEmail"
    static let userPhoneNumber = "userPhoneNumber"
    static let userdob = "dob"
    static let userzip = "zip"
    static let usercity = "city"
    static let userstate = "state"
    static let useraddress1 = "address1"
    static let useraddress2 = "address2"
    static let userId = "userId"
    
    static let user_type = "user_type"
    static let is_Active = "is_Active"
    static let last_login = "last_login"
    static let created_at = "created_at"
    static let update_at = "update_at"

    
    static let user_race = "race"
    static let user_gender = "gender"
    static let user_martial = "martial"
    static let user_language = "language"
    static let user_ethnicity = "ethnicity"
    static let user_insurance = "insurance"
    static let user_marital_status = "marital_status"
    static let doctorFullName = "doctorFullName"
    static let isCalendarSync = "isCalendarSync"
    static let LocalPushNotification = "LocalPushNotification"
    
    
    struct Insurance {
        static let FrontImage = "FrontImage"
        static let backImage = "backImage"
    }
    
    
    
}

struct Platform {
    static let isSimulator: Bool = {
        var isSim = false
        #if arch(i386) || arch(x86_64)
            isSim = true
        #endif
        return isSim
    }()
}

struct CustomFontColor {
    static let appointmentDate = "#8E8E93"
    static let appointmentTime = "#535353"
    static let setDefaultApptTime = "#02CCBB"
}
struct AppointmentType {
    static var past = "Past appointments"
    static var scheduled = "Scheduled appointments"
    static var checkedin_appointments = "Checkedin appointments"
}
struct CustomApp {
    static let iOSVersion = "13.4"
}

struct PayPayInfo {
    static let clientTokenOrTokenizationKey = "sandbox_rzgtgzv9_jxwyvn7gg4q9w37g"
}

func getHideAccordingMinute(Model:AppointmentModel) -> Bool
 {
     
     let dateformate = DateFormatter()
     dateformate.dateFormat = "HH:mm"
     let currentTime = dateformate.string(from: Date())
     
    let timeInterval = Utils.findDateDiffInMinute(time1Str: currentTime, time2Str: Model.appointment_time ?? "", formate: "HH:mm")
     
     return timeInterval <= 15 && timeInterval > 0  ? true:false
 }



func getFilterFlote(SelectedTag:Int,Slots:[ProviderSlots]) -> [ProviderSlots]
{
    for Date in Slots
    {
        
        for time in Date.slots
        {
        print("slote \(time.appointment_time ?? "")")
        }
        
        
    }
    var filterSlote = [ProviderSlots]()
    
    
    return filterSlote
}



func getHideAccordingHour(date:String,formate:String) -> Bool
    {
        
      let dateformate = DateFormatter()
        dateformate.dateFormat = "MM-dd-yyyy"
        let currentDate = dateformate.string(from: Date())
     
     
        return Utils.findDateDiffInHours(time1Str: currentDate, time2Str: date,formate:formate) <= 72 ? true:false
    }




let kPrimaryRegularUIColor = UIColor(red: 27/255, green: 145/255, blue: 238/255, alpha: 1)//rgba(27, 145, 238, 1)//#1B91EE
let kCustomOrangeUIColor = UIColor(red: 250/255, green: 140/255, blue: 21/255, alpha: 1)//rgba(250, 140, 21, 1)//#FA8C15

let kCustomGrayUIColor = UIColor(red: 142/255, green: 142/255, blue: 147/255, alpha: 1)//rgba(142, 142, 147, 1)//#8E8E93
let kCustomGrayTableSeparatorColor = UIColor(red: 200/255, green: 199/255, blue: 204/255, alpha: 1)//rgba(200, 199, 204, 1)//#C8C7CC



let kPrimaryRegularColor = Color(red: 39/255, green: 145/255, blue: 238/255)//rgba(27, 145, 238, 1)//#1B91EE
let kCustomGrayColor = Color(red: 142/255, green: 142/255, blue: 147/255)//rgba(142, 142, 147, 1)//#8E8E93
let kCustomLightBlueColor = Color(red: 248/255, green: 251/255, blue: 250/255)//rgba(248, 251, 250, 1)//#F8FBFA
let kCustomDarkGrayColor = Color(red: 92/255, green: 92/255, blue: 92/255)//rgba(92, 92, 92, 1)//#5C5C5C
let kCustomOrangeColor = Color(red: 250/255, green: 140/255, blue: 21/255)//rgba(250, 140, 21, 1)//#FA8C15


let kCustomPrimaryDarkColor = Color(red: 28/255, green: 105/255, blue: 167/255)//rgba(28, 105, 167, 1)//#1C69A7





let kCustomLightGrayDividerColor = Color(red: 224/255, green: 224/255, blue: 224/255)//rgba(224, 224, 224, 1)//#E0E0E0
let kCustomTextGrayColor = Color(red: 123/255, green: 122/255, blue: 122/255)//rgba(123, 122, 122, 1)//#7B7A7A

let kCustomTextBlueColor = Color(red: 28/255, green: 105/255, blue: 167/255)//rgba(28, 105, 167, 1)
let kCustomGreenColor = Color(red: 2/255, green: 204/255, blue: 187/255)//rgba(2, 204, 187, 1)//#02CCBB

let kCustomLightGrayPhotoBg = Color(red: 245/255, green: 245/255, blue: 245/255)//rgba(245, 245, 245, 1)//#F5F5F5
let kCustomLightGrayDisable = Color(red: 180/255, green: 180/255, blue: 180/255)


extension Date {
    func get(_ components: Calendar.Component..., calendar: Calendar = Calendar.current) -> DateComponents {
        return calendar.dateComponents(Set(components), from: self)
    }

    func get(_ component: Calendar.Component, calendar: Calendar = Calendar.current) -> Int {
        return calendar.component(component, from: self)
    }
}

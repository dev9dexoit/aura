//
//  AppDelegate.swift
//  MyDoktorUp
//
//  Created by Mac on 05/02/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Firebase
import IQKeyboardManager
import GoogleSignIn
import Braintree

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // for Schedule notificaton
        UNUserNotificationCenter.current().delegate = self

        requestAuthorizationNotification()
        
        FirebaseApp.configure()
        // Initialize sign-in
        GIDSignIn.sharedInstance().clientID = "819149528431-5g73rdjcsi8ss5kd1lof9so6ncl5lmbi.apps.googleusercontent.com"
        // Initialize BrainTree
        BTAppSwitch.setReturnURLScheme("com.designial.mydoktorup.debug.payments")
        
        
        
        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().keyboardDistanceFromTextField = 10

        let syncCalendar = UserDefaults.standard.value(forKey: UserDefault.isCalendarSync) as? Bool
        if syncCalendar == nil {
            UserDefaults.standard.set(true, forKey: UserDefault.isCalendarSync)
        }
        
        let localPushNotification = UserDefaults.standard.value(forKey: UserDefault.LocalPushNotification) as? Bool
        if localPushNotification == nil {
            UserDefaults.standard.set(true, forKey: UserDefault.LocalPushNotification)
        }
        
        
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    @available(iOS 9.0, *)
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        if url.scheme?.localizedCaseInsensitiveCompare("com.designial.mydoktorup.debug.payments") == .orderedSame {
            return BTAppSwitch.handleOpen(url, options: options)
        } else if url.scheme?.localizedCaseInsensitiveCompare("com.googleusercontent.apps.819149528431-5g73rdjcsi8ss5kd1lof9so6ncl5lmbi") == .orderedSame {
            GIDSignIn.sharedInstance().handle(url)
        }
        return false
    }

    // If you support iOS 8, add the following method.
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        if url.scheme?.localizedCaseInsensitiveCompare("com.designial.mydoktorup.debug.payments") == .orderedSame {
            return BTAppSwitch.handleOpen(url, sourceApplication: sourceApplication)
        } else if url.scheme?.localizedCaseInsensitiveCompare("com.googleusercontent.apps.819149528431-5g73rdjcsi8ss5kd1lof9so6ncl5lmbi") == .orderedSame {
            GIDSignIn.sharedInstance().handle(url)
        }
        return false
    }
}
extension AppDelegate: UNUserNotificationCenterDelegate
{
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        let id = notification.request.identifier
        print("Received notification with ID = \(id)")

        completionHandler([.sound, .alert])
    }
}

//
//  CustomPhoneNumberForamteTextField.swift
//  MyDoktorUp
//
//  Created by RMV on 07/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI
import UIKit

struct TextFieldWithAsInputFormate : UIViewRepresentable {

     
     var placeholder : String
    var Border : Bool
    var textalignment : NSTextAlignment
    var Fontsize:UIFont
        @Binding var text : String

     private let textField = UITextField()

    init(placeholder: String, Border:Bool,fontsize:UIFont,Alignment:NSTextAlignment, text: Binding<String>) {
                self.placeholder = placeholder
                self.Border = Border
                self.Fontsize = fontsize
                self.textalignment = Alignment
                self._text = text
          }
    
     func makeCoordinator() -> TextFieldWithAsInputFormate.Coordinator {
          Coordinator(textfield: self)
     }

     func makeUIView(context: UIViewRepresentableContext<TextFieldWithAsInputFormate>) -> UITextField {
//
//        .foregroundColor(kCustomGrayColor)
//        .accentColor(kCustomGrayColor)
//        .multilineTextAlignment(.center)
//        .font(.system(.headline))
//        .textFieldStyle(RoundedBorderTextFieldStyle())
//
//        .textContentType(.telephoneNumber)
//
        textField.textAlignment = self.textalignment
        
        if Border
        {
            textField.layer.cornerRadius = 8.0
            textField.layer.borderWidth = 1.8
            textField.layer.borderColor = UIColor(red: 142/255, green: 142/255, blue: 147/255, alpha: 0.5).cgColor

        }
        
        
        textField.text = self.text.formatePhoneNumber()
        textField.layer.masksToBounds = true
        textField.font = self.Fontsize
        textField.sizeToFit()
        textField.textColor = UIColor(red: 142/255, green: 142/255, blue: 147/255, alpha: 1)
        textField.tintColor = UIColor(red: 142/255, green: 142/255, blue: 147/255, alpha: 1)

            textField.placeholder = placeholder
            textField.keyboardType = .phonePad
            textField.delegate = context.coordinator
          return textField
     }
  

     func updateUIView(_ uiView: UITextField, context: UIViewRepresentableContext<TextFieldWithAsInputFormate>) {
        uiView.text = text
     }
     class Coordinator: NSObject , UITextFieldDelegate {

          private let parent : TextFieldWithAsInputFormate

          init(textfield : TextFieldWithAsInputFormate) {
               self.parent = textfield
          }
       
        
        func textFieldDidChangeSelection(_ textField: UITextField) {
            if !(textField.text?.contains("-"))!
            {
                textField.text = textField.text?.formatePhoneNumber()
            }
                parent.text = textField.text ?? String()
        }
        
        
        
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            

             var strText: String? = textField.text
            if strText == nil {
              strText = ""
            }
            strText = strText?.replacingOccurrences(of: "-", with:"")

          if strText!.count > 1 && strText!.count != 9 && strText!.count % 3 == 0 && string != "" {
            if textField.text?.last != "-"
            {

                textField.text! = "\(textField.text!)-\(string)"
              return false
            }

          }
            if textField.text!.count <= 11
            { 
                return true
            }
            else{
                if  string == ""
                {
                   
                    return true
                }
                else
                {
                    return false

                    
                }
            }
            
            
            
        }
          func textFieldDidEndEditing(_ textField: UITextField) {
               self.parent.textField.resignFirstResponder()
          }
    }
}

//
//  LoaderView.swift
//  MyDoktorUp
//
//  Created by Mac on 22/02/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI

struct ActivityIndicator: UIViewRepresentable {
    @Binding var isAnimating: Bool
    let style: UIActivityIndicatorView.Style
    func makeUIView(context: UIViewRepresentableContext<ActivityIndicator>) -> UIActivityIndicatorView {
        let loadingIndicator = UIActivityIndicatorView(style: style)
        loadingIndicator.color = .white
        return loadingIndicator
    }
    func updateUIView(_ uiView: UIActivityIndicatorView, context: UIViewRepresentableContext<ActivityIndicator>) {
        isAnimating ? uiView.startAnimating() : uiView.stopAnimating()
    }
}
struct LoadingView<Content>: View where Content: View {
    @Binding var isShowing: Bool
    var content: () -> Content
    var body: some View {
        GeometryReader { geometry in
            ZStack(alignment: .center) {
                self.content()
                    .disabled(self.isShowing)
                    .blur(radius: self.isShowing ? 0.8 : 0)
                VStack {
                    Text("Loading...")
                        .foregroundColor(.white)
                        .font(.headline)
                    ActivityIndicator(isAnimating: .constant(true), style: .large)
                }
                .frame(width: 150,
                       height: 150)
                .background(kPrimaryRegularColor)
                .foregroundColor(Color.white)
                .cornerRadius(20)
                    .opacity(self.isShowing ? 0.8 : 0)

            }
        }
    }
}

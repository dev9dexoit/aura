//
//  AppleSignInView.swift
//  MyDoktorUp
//
//  Created by MACBOOK PRO on 26/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import SwiftUI
import AuthenticationServices

// 1
 struct SignInWithApple: UIViewRepresentable {
    private let completionHandler: (Bool) -> Void

    
    init(completion: @escaping (Bool) -> Void) {

            self.completionHandler = completion
            
        }
    
    
    func makeCoordinator() -> Coordinator {
        return Coordinator(self, completion: completionHandler)
    }
    
    func makeUIView(context: Context) -> UIButton {
   
        let AppleBtn = UIButton()
        AppleBtn.setImage(UIImage(named: "ic_apple_logo"), for: .normal)
        AppleBtn.setTitle("Sign in with Apple", for: .normal)
        AppleBtn.addTarget(context.coordinator, action:  #selector(Coordinator.didTapButton), for: .touchUpInside)
        AppleBtn.backgroundColor = UIColor.black
        AppleBtn.layer.cornerRadius = 10
        AppleBtn.contentEdgeInsets = UIEdgeInsets(top: 12, left: 10, bottom: 12, right: 10)
        AppleBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)

        return AppleBtn
        
  }
  
  func updateUIView(_ uiView: UIButton, context: Context) {
    
  }

    class Coordinator: NSObject, ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding {
        let parent: SignInWithApple?
         private let completionHandler: (Bool) -> Void
        init(_ parent: SignInWithApple,completion: @escaping (Bool) -> Void) {
            self.parent = parent
            completionHandler = completion

            super.init()

        }
        
        @objc func didTapButton() {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
            
            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.presentationContextProvider = self
            authorizationController.delegate = self
            authorizationController.performRequests()
        }
        
        func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
            let vc = UIApplication.shared.windows.last?.rootViewController
            return (vc?.view.window!)!
        }
        
        func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
            guard let credentials = authorization.credential as? ASAuthorizationAppleIDCredential else {
                return
            }
            
            
                let firstname = credentials.fullName?.familyName ?? ""
                let lastname = credentials.fullName?.givenName ?? ""
                UserDefaultsManager().userFullName = firstname + " " + lastname
                UserDefaultsManager().userEmail = credentials.email ?? ""
            
            SignInObserver().signInViaSocialSite(Token: "\(credentials.identityToken ?? Data())", authType: LoginTypes.Apple) { (Succes) in
                                 self.completionHandler(true)

                             }
            //let defaults = UserDefaults.standard
            //defaults.set(credentials.user, forKey: "userId")
          //  parent?.name = "\(credentials.fullName?.givenName ?? "")"
        }
        
        func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        }
    }
}




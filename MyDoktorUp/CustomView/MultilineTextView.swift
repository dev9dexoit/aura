//	
//  MultilineTextView.swift
//  MyDoktorUp
//
//  Created by Mac on 29/02/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI
import UIKit

struct TextView: UIViewRepresentable {
    var placeholder: String
    @Binding var text: String
    var FontSize: CGFloat = 16
    var minHeight: CGFloat
    var isSelection = false
    var isEditable = false
    var isUserInteractionEnabled = false
    @Binding var calculatedHeight: CGFloat
    
    init(placeholder: String, text: Binding<String>,isSelection: Bool,isEditable: Bool,isUserInteractionEnabled : Bool,fontSize:CGFloat, minHeight: CGFloat, calculatedHeight: Binding<CGFloat>) {
        self.placeholder = placeholder
        self._text = text
        self.minHeight = minHeight
        self.FontSize = fontSize
        self._calculatedHeight = calculatedHeight
        self.isEditable = isEditable
        self.isUserInteractionEnabled = isUserInteractionEnabled
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }
    
    func makeUIView(context: Context) -> UITextView {
        let textView = UITextView()
        textView.delegate = context.coordinator
        
        // Decrease priority of content resistance, so content would not push external layout set in SwiftUI
        textView.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
        
        textView.isScrollEnabled = isSelection
        textView.isEditable = isEditable
        textView.isUserInteractionEnabled = isUserInteractionEnabled
        //textView.backgroundColor = UIColor(white: 0.0, alpha: 0.05)
        
        textView.contentInset = UIEdgeInsets(top: 12, left: 8, bottom: 0, right: 0)
        // Set the placeholder
        textView.font = UIFont.systemFont(ofSize: FontSize)
        textView.text = text
        textView.textColor = kCustomGrayUIColor
//        textView.textColor = self.color
        return textView
    }
    
    func updateUIView(_ textView: UITextView, context: Context) {
//        if textView.textColor != kCustomGrayUIColor {
            textView.text = self.text
//        }
        
        recalculateHeight(view: textView)
    }
    
    func recalculateHeight(view: UIView) {
        let newSize = view.sizeThatFits(CGSize(width: view.frame.size.width, height: CGFloat.greatestFiniteMagnitude))
        if minHeight < newSize.height && $calculatedHeight.wrappedValue != newSize.height {
            DispatchQueue.main.async {
                self.$calculatedHeight.wrappedValue = newSize.height // !! must be called asynchronously
            }
        } else if minHeight >= newSize.height && $calculatedHeight.wrappedValue != minHeight {
            DispatchQueue.main.async {
                self.$calculatedHeight.wrappedValue = self.minHeight // !! must be called asynchronously
            }
        }
    }
    
    class Coordinator : NSObject, UITextViewDelegate {
        
        var parent: TextView
        
        init(_ uiTextView: TextView) {
            self.parent = uiTextView
        }
        
        func textViewDidChange(_ textView: UITextView) {
            // This is needed for multistage text input (eg. Chinese, Japanese)
            if textView.markedTextRange == nil {
                parent.text = textView.text ?? String()
                parent.recalculateHeight(view: textView)
            }
        }
        
        func textViewDidBeginEditing(_ textView: UITextView) {
            if textView.textColor == kCustomGrayUIColor {
                textView.text = nil
                textView.textColor = UIColor.black
            }
        }
        
        func textViewDidEndEditing(_ textView: UITextView) {
            if textView.text.isEmpty {
                textView.text = parent.placeholder
                textView.textColor = kCustomGrayUIColor
            }
        }
    }
}




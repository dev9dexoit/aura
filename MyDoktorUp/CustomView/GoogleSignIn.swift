
//
//  GoogleSignUin.swift
//  MyDoktorUp
//
//  Created by MACBOOK PRO on 20/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import GoogleSignIn
import SwiftUI
import UIKit
import Firebase


struct GoogleSignIn : UIViewRepresentable {
    private let completionHandler: (Bool) -> Void

//    var Email:String
//    var familyName:String
//    var name:String
//    var givenName:String
//
//
    
    init(completion: @escaping (Bool) -> Void) {
//        Email = ""
//        familyName = ""
//        name = ""
//        givenName = ""
        self.completionHandler = completion
        
    }
    
    
    func makeCoordinator() -> GoogleSignIn.Coordinator {
        return GoogleSignIn.Coordinator(completion: completionHandler)
    }
        
    class Coordinator: NSObject, GIDSignInDelegate {
        
        
        private let completionHandler: (Bool) -> Void
        
        init(completion: @escaping (Bool) -> Void) {
            completionHandler = completion
        }
        
        func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
          if let error = error {
            print("error \(error.localizedDescription)")
               self.completionHandler(false )

          } else
          {
            let firstname = user.profile.familyName ?? ""
                   let lastname = user.profile.givenName ?? ""
                   UserDefaultsManager().userFullName = firstname + " " + lastname
                   
                   UserDefaultsManager().userEmail = user.profile.email ?? ""
                   
                   SignInObserver().signInViaSocialSite(Token: user.authentication.idToken, authType: LoginTypes.Google) { (Succes) in
                       self.completionHandler(true)

                   }
            
            }
            
     
//        print("id token \(user.authentication.idToken)")
            
            
        }
        
        
        
        
        func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
            
            completionHandler(false)
        }
        
    }
    
    
    func makeUIView(context: UIViewRepresentableContext<GoogleSignIn>) -> GIDSignInButton {
        let view = GIDSignInButton()
        
        
        GIDSignIn.sharedInstance().delegate = context.coordinator
        GIDSignIn.sharedInstance()?.presentingViewController = UIApplication.shared.windows.last?.rootViewController
        return view
    }
    
    func updateUIView(_ uiView: GIDSignInButton, context: UIViewRepresentableContext<GoogleSignIn>) { }
    
    
    
}

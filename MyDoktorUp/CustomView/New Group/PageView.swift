//
//  PageView.swift
//  MyDoktorUp
//
//  Created by RMV on 13/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI

struct PageViewData: Identifiable {
    let id: String = UUID().uuidString
    let imageNamed: String
}

struct PageView: View {
    let viewData: PageViewData
    var body: some View {
        Image(viewData.imageNamed)
            .resizable()
            .frame(minWidth: 0, idealWidth: 100, maxWidth: .infinity, minHeight: 0, idealHeight: 100, maxHeight: .infinity, alignment: .center)
            .scaledToFit()
        
    }
}

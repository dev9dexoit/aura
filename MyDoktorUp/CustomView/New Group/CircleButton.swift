//
//  CircleButton.swift
//  MyDoktorUp
//
//  Created by RMV on 13/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import SwiftUI

struct CircleButton: View {
    @Binding var isSelected: Bool
    let action: () -> Void
    
    var body: some View {
        Button(action: {
            self.action()
        }) { Circle()
            .frame(width: 16, height: 16)
            .foregroundColor(self.isSelected ? Color.gray : Color.gray.opacity(0.5))
        }
    }
}

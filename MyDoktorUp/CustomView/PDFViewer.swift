//
//  PDFViewer.swift
//  MyDoktorUp
//
//  Created by MACBOOK PRO on 16/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//



import SwiftUI
import PDFKit


struct PDFKitRepresentedView: UIViewRepresentable {
    let url: URL

    init(_ url: URL) {
        self.url = url
    }
    func makeUIView(context: UIViewRepresentableContext<PDFKitRepresentedView>) -> UIView {
            let pdfView = PDFView()
              pdfView.document = PDFDocument(url: self.url)
              return pdfView
    }


    func updateUIView(_ uiView: UIView, context: UIViewRepresentableContext<PDFKitRepresentedView>) {
        // Update the view.
    }
}


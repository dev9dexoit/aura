//
//  DocumentScaner.swift
//  MyDoktorUp
//
//  Created by MACBOOK PRO on 16/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import Vision
import UIKit
import VisionKit
import SwiftUI





struct ScannerView: UIViewControllerRepresentable {
    private let completionHandler: (UIImage) -> Void
    var  PageTitle:String = ""
    init(Title:String,completion: @escaping (UIImage) -> Void) {
        PageTitle = Title
        self.completionHandler = completion
    }
     
    typealias UIViewControllerType = VNDocumentCameraViewController
     
    func makeUIViewController(context: UIViewControllerRepresentableContext<ScannerView>) -> VNDocumentCameraViewController {
        let viewController = VNDocumentCameraViewController()
        viewController.title = PageTitle
        viewController.delegate = context.coordinator
        return viewController
    }
     
    func updateUIViewController(_ uiViewController: VNDocumentCameraViewController, context: UIViewControllerRepresentableContext<ScannerView>) {}
     
    func makeCoordinator() -> Coordinator {
        return Coordinator(completion: completionHandler)
    }
     
    final class Coordinator: NSObject, VNDocumentCameraViewControllerDelegate {
        private let completionHandler: (UIImage) -> Void
         
        init(completion: @escaping (UIImage) -> Void) {
            self.completionHandler = completion
        }
         
        func documentCameraViewController(_ controller: VNDocumentCameraViewController, didFinishWith scan: VNDocumentCameraScan) {
            guard scan.pageCount >= 1 else {
                // You are responsible for dismissing the VNDocumentCameraViewController.
                controller.dismiss(animated: true)
                return
            }
            
            // This is a workaround for the VisionKit bug which breaks the `UIImage` returned from `VisionKit`
            // See the `Image Loading Hack` section below for more information.
            let originalImage = scan.imageOfPage(at: 0)
            let fixedImage = reloadedImage(originalImage)
            
            // You are responsible for dismissing the VNDocumentCameraViewController.
            controller.dismiss(animated: true)
            
            // Process the image
            processImage(fixedImage)
        }
         
        func documentCameraViewControllerDidCancel(_ controller: VNDocumentCameraViewController) {
            completionHandler(UIImage(named: "ic_camera")!)
        }
         
        func documentCameraViewController(_ controller: VNDocumentCameraViewController, didFailWithError error: Error) {
            print("Document camera view controller did finish with error ", error)
            completionHandler(UIImage(named: "ic_camera")!)
            
        }

        private func processImage(_ image: UIImage) {
               completionHandler(image)
               //recognizeTextInImage(image)
               
           }
        
        
        func reloadedImage(_ originalImage: UIImage) -> UIImage {
            guard let imageData = originalImage.jpegData(compressionQuality: 1),
                let reloadedImage = UIImage(data: imageData) else {
                    return originalImage
            }
            return reloadedImage
        }
    }
}







extension UIImageView {
    /// Calculates the rect of an image displayed in a `UIImageView` with the `scaleAspectFit` `contentMode`
    var imageRect: CGRect {
        guard let image = image else { return bounds }
        guard contentMode == .scaleAspectFit else { return bounds }
        guard image.size.width > 0 && image.size.height > 0 else { return bounds }
        
        let scale: CGFloat
        if image.size.width > image.size.height {
            scale = bounds.width / image.size.width
        } else {
            scale = bounds.height / image.size.height
        }
        
        let size = CGSize(width: image.size.width * scale, height: image.size.height * scale)
        let x = (bounds.width - size.width) / 2.0
        let y = (bounds.height - size.height) / 2.0
        
        return CGRect(x: x, y: y, width: size.width, height: size.height)
    }
}


class BoundingBoxImageView: UIImageView {
    
    /// The bounding boxes currently shown
    private var boundingBoxViews = [UIView]()
    
    func load(boundingBoxes: [CGRect]) {
        // Remove all the old bounding boxes before adding the new ones
        removeExistingBoundingBoxes()
        
        // Add each bounding box
        for box in boundingBoxes {
            load(boundingBox: box)
        }
    }
    
    /// Removes all existing bounding boxes
    func removeExistingBoundingBoxes() {
        for view in boundingBoxViews {
            view.removeFromSuperview()
        }
        boundingBoxViews.removeAll()
    }
    
    private func load(boundingBox: CGRect) {
        // Cache the image rectangle to avoid unneccessary work
        let imageRect = self.imageRect
        
        // Create a mutable copy of the bounding box
        var boundingBox = boundingBox
        
        // Flip the Y axis of the bounding box because Vision uses a different coordinate system to that of UIKit
        boundingBox.origin.y = 1 - boundingBox.origin.y
        
        // Convert the bounding box rect based on the image rectangle
        var convertedBoundingBox = VNImageRectForNormalizedRect(boundingBox, Int(imageRect.width), Int(imageRect.height))
        
        // Adjust the bounding box based on the position of the image inside the UIImageView
        // Note that we only adjust the axis that is not the same in both--because we're using `scaleAspectFit`, one of the axis will always be equal
        if frame.width - imageRect.width != 0 {
            convertedBoundingBox.origin.x += imageRect.origin.x
            convertedBoundingBox.origin.y -= convertedBoundingBox.height
        } else if frame.height - imageRect.height != 0 {
            convertedBoundingBox.origin.y += imageRect.origin.y
            convertedBoundingBox.origin.y -= convertedBoundingBox.height
        }
        
        // Enlarge the bounding box to make it contain the text neatly
        let enlargementAmount = CGFloat(2.2)
        convertedBoundingBox.origin.x    -= enlargementAmount
        convertedBoundingBox.origin.y    -= enlargementAmount
        convertedBoundingBox.size.width  += enlargementAmount * 2
        convertedBoundingBox.size.height += enlargementAmount * 2
        
        // Create a view with a narrow border and transparent background as the bounding box
        let view = UIView(frame: convertedBoundingBox)
        view.layer.opacity = 1
        view.layer.borderColor = UIColor.orange.cgColor
        view.layer.borderWidth = 2
        view.layer.cornerRadius = 2
        view.layer.masksToBounds = true
        view.backgroundColor = .clear
        
        addSubview(view)
        boundingBoxViews.append(view)
    }
}

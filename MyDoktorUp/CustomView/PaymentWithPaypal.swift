//
//  PaymentWithPaypal.swift
//  MyDoktorUp
//
//  Created by MACBOOK PRO on 17/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import SwiftUI
import UIKit
import Braintree
import BraintreeDropIn

struct PaymentWithPaypal: UIViewControllerRepresentable {
    var authorization: String
    var handler: BTDropInControllerHandler

    init(authorization: String, handler: @escaping BTDropInControllerHandler) {
        self.authorization = authorization
        self.handler = handler
    }
    
    func makeUIViewController(context: Context) -> BTDropInController {
        guard let bTDropInController = BTDropInController(authorization: authorization, request: BTDropInRequest(), handler: handler) else {
            return BTDropInController()
        }
        return bTDropInController
    }
    
    func updateUIViewController(_ uiViewController: BTDropInController, context: UIViewControllerRepresentableContext<PaymentWithPaypal>) {
    }
}
